"use strict";
/**
 * Development specific configuration
 * ==================================
 */
const localMongo = {
  host: "localhost",
  port: "27017",
  user: "",
  password: "",
  db: "mom-database"
};

module.exports.hostname = "http://localhost:5000";
module.exports.connection = localMongo;
