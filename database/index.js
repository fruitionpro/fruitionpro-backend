"use strict";
const mongoose = require("mongoose");
const url = require("../common/credential");

mongoose
  .connect(url.mongodbUrl, { useNewUrlParser: true })
  .then(() => {
    console.log("connected to db");
  })
  .catch(error => {
    console.log("Error === ", error);
  });

let db = mongoose.connection;
module.exports.db = db;
