"use strict";
const jwt = require("jsonwebtoken");
const secret = "mombackend";
const errorConsole = require("./common/catch-consoles");
const User = require("./routes/user/user.model");

const isValid = async data => {
  try {
    if (data && data.email) {
      return await User.findOne({ email: data.email })
        .then(result => {
          if (result) return false;
          else return true;
        })
        .catch(() => true);
    }
    return true;
  } catch (err) {
    console.log(`${errorConsole.try_catch}isValid`, err);
  }
};

const checkToken = (req, res, next) => {
  try {
    const url =
      req.originalUrl.includes("/api/meeting/attendance") ||
      req.originalUrl.includes("/api/license/stripe-webhook");
    let token = req.headers["x-access-token"] || req.headers["authorization"]; // Express headers are auto converted to lowercase
    let operation = req.headers["operation"];
    if (token || operation || url) {
      if (operation === "public" || url) {
        next();
      } else {
        if (token && token.startsWith("Bearer ")) {
          // Remove Bearer from string
          token = token.slice(7, token.length);
        }

        if (token) {
          jwt.verify(token, secret, async (err, decoded) => {
            const isDeleted = await isValid(decoded);
            if (err || isDeleted) {
              return res.json({
                isValidToken: false,
                status: 401,
                message: "Token is not valid"
              });
            } else {
              req.decoded = decoded;
              next();
            }
          });
        } else {
          return res.json({
            status: 401,
            isValidToken: false,
            message: "Auth token is not supplied"
          });
        }
      }
    } else {
      return res.json({
        isValidToken: false,
        status: 401,
        message: "You are not authorize to access mom api's"
      });
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}checkToken`, err);
  }
};

const getJwtToken = email => {
  try {
    let token = jwt.sign({ email }, secret, {
      expiresIn: "8h" // expires in 8 hours
    });
    return token;
  } catch (err) {
    console.log(`${errorConsole.try_catch}getJwtToken`, err);
  }
};

module.exports.checkToken = checkToken;
module.exports.getJwtToken = getJwtToken;
