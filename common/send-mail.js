"use strict";
const sgMail = require("@sendgrid/mail");
const errorConsole = require("../common/catch-consoles");
const url = require("./credential");
const User = require("../routes/user/user.model");

/**
 * @method sendMail: A method to send mail
 * @param {String}  sender sender email from previous/parent
 * @param {String}  receiver receiver email from previous/parent
 * @param {String}  htmlBody mail content from previous/parent
 * @param {String}  subject mail subject from previous/parent
 * @param {Array}  docs List of files from previous/parent
 */
const sendMail = async (sender, receiver, htmlBody, subject, docs) => {
  try {
    let name = "";
    if (sender === url.email) {
      name = url.name;
    } else {
      const user = await User.findOne({ email: sender }, { name: 1, _id: 0 });
      name = (user && user.name) || "";
    }

    return new Promise(resolve => {
      sgMail.setApiKey(url.sendMailKey);
      const msg = {
        to: receiver,
        from: { name, email: sender },
        subject,
        text: url.email,
        html: htmlBody
      };
      if (docs) {
        msg.attachments = docs;
      }
      sgMail.send(msg);
      resolve(true);
    });
  } catch (err) {
    console.log(`${errorConsole.try_catch}sendMail`, err);
    return false;
  }
};

module.exports.sendMail = sendMail;
