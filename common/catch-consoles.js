/*
 catch console messages
*/
const api_catch = "api-catch-error-";
const try_catch = "try-catch-error-";

module.exports.try_catch = try_catch;
module.exports.api_catch = api_catch;
