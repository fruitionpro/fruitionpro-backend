"use strict";
const moment = require("moment");
const errorConsole = require("../common/catch-consoles");
const momentTimezone = require("moment-timezone");

/**
 * @method dateDifference: A method to calculate days, hours and mintues between two dates
 * @param {Object}  startDate Start date from previous/parent
 * @param {Object}  endDate End date from previous/parent
 */
const dateDifference = (startDate, endDate) => {
  let result = {
    days: "",
    hours: "",
    minutes: ""
  };

  try {
    const ticks = (endDate - startDate) / 1000;
    let delta = Math.abs(ticks);

    // calculate (and subtract) whole days
    result.days = Math.floor(delta / 86400);
    delta -= result.days * 86400;

    // calculate (and subtract) whole hours
    result.hours = Math.floor(delta / 3600) % 24;
    delta -= result.hours * 3600;

    // calculate (and subtract) whole minutes
    result.minutes = Math.floor(delta / 60) % 60;
    delta -= result.minutes * 60;

    if (ticks < 0) {
      result.days = result.days === 0 ? result.days : -result.days;
      result.hours = result.hours === 0 ? result.hours : -result.hours;
      result.minutes = result.minutes === 0 ? result.minutes : -result.minutes;
    }
    return result;
  } catch (err) {
    console.log(`${errorConsole.try_catch}dateDifference`, err);
    return result;
  }
};

/**
 * @method isPastDate: A method to check date is past or not
 * @param {Object}  dateTime Date from previous/parent
 */
const isPastDate = dateTime => {
  try {
    if (!dateTime) return false;
    const serverTimezone = moment.tz.guess(true);
    const currentDate = moment.tz(new Date(), serverTimezone).utc();
    const eventDateTime = moment.tz(dateTime, serverTimezone).utc();
    if (eventDateTime.isBefore(currentDate)) return true;
    else return false;
  } catch (err) {
    console.log(`${errorConsole.try_catch}isPastDate`, err);
    return false;
  }
};

module.exports.dateDifference = dateDifference;
module.exports.isPastDate = isPastDate;
