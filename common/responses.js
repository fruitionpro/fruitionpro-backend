"use strict";
/**
 * api's status
 * 200 when run api successfully
 * 304 when input request data already created or exist
 * 401 when invalid input request data
 * 400 when run un-successfully or something invalid inputs
 */
const trialLicenseExist = {
  status: 304,
  message: "Trial license already Created."
};

const InvalidLicense = { status: 400, message: "Invalid license" };

const ExpiredLicense = {
  status: 400,
  message: "Your license has been Expired."
};

const InvalidEmail = {
  status: 401,
  message: "Invalid Email."
};

const InvalidPassword = {
  status: 401,
  message: "Invalid Password."
};

const InvalidToken = {
  status: 401,
  message: "Invalid Token."
};

const InvalidID = {
  status: 401,
  message: "Invalid ID."
};

const loginSuccess = {
  status: 200,
  message: "Login Sucessfully."
};

const loginUnsuccess = {
  status: 400,
  message: "Invalid Login Details."
};

const Exist = {
  status: 304,
  message: "User already Exist."
};

const notExist = {
  status: 400,
  message: "Email doesn't Exist."
};

const resetPasswordSuccess = {
  status: 200,
  message: "Reset Password Sucessfully."
};

const verifiedSuccess = {
  status: 200,
  message: "Verified Successfully."
};

const verifyEmail = {
  status: 400,
  message: "Email not Verified."
};

const sendEmailSuccess = {
  status: 200,
  message: "Send Email Sucessfully."
};

const sendEmailUnSuccess = {
  status: 400,
  message: "Invalid invite Details."
};

const tokenExpired = {
  status: 400,
  message: "Your token has been Expired."
};

const notRegistered = {
  status: 400,
  message: "User not Registered."
};

const userType = {
  status: 200,
  message: "Update user type Sucessfully."
};

const wrongDetailError = {
  status: 400,
  message: "Invalid input details."
};

const organizationSuccess = {
  status: 200,
  message: "Create Organization Sucessfully."
};

const updateUserSuccess = {
  status: 200,
  message: "Update User Sucessfully."
};

const updateOrganizationSuccess = {
  status: 200,
  message: "Update Organization Sucessfully."
};

const deleteUserSuccess = {
  status: 200,
  message: "Delete User Sucessfully."
};

const organizationExist = {
  status: 304,
  message: "Organization already Exist."
};

const fetchSuccess = {
  status: 200,
  message: "Fetch Sucessfully."
};

const fetchUnsuccess = {
  status: 400,
  message: "Fetch Unsucessfully."
};

const importSuccuss = {
  status: 200,
  message: "Import Meetings Successfully."
};

const noMeetings = {
  status: 200,
  message: "No Meetings."
};

const noContacts = {
  status: 200,
  message: "No Contacts."
};

const importUnsuccuss = {
  status: 400,
  message: "Failed in Import meetings."
};

const alreadyInvited = { status: 400, message: "User already invited" };

const contactsSuccuss = {
  status: 200,
  message: "Import Contacts Successfully."
};

const fetchContactsUnsuccuss = {
  status: 400,
  message: "Failed in fetch Contacts."
};

const deleteContactSuccess = {
  status: 200,
  message: "Delete Contact Sucessfully."
};

const deleteMeetingSuccess = {
  status: 200,
  message: "Delete Meeting Sucessfully."
};

const somethingWrong = {
  status: 400,
  message: "Wrong delete credentails."
};

const createMeetingSuccess = {
  status: 200,
  message: "Create Meeting Sucessfully."
};

const createAgendaSuccess = {
  status: 200,
  message: "Create Agenda Sucessfully."
};

const updateAttendanceStatus = {
  status: 200,
  message: "Attendance added sucessfully."
};

const closeMeeting = {
  status: 200,
  message: "Meeting Closed sucessfully."
};

const reOpenMeeting = {
  status: 200,
  message: "Meeting Re-Open sucessfully."
};

const sameAsOldPassword = {
  status: 400,
  message: "Your new password is same as old password."
};

const changePasswordSuccess = {
  status: 200,
  message: "Change Password Sucessfully."
};

const notSetPassword = {
  status: 400,
  message: "You are social user."
};

const wrongOldPassword = {
  status: 400,
  message: "Old Password is wrong."
};

const startMeeting = {
  status: 200,
  message: "Meeting Start sucessfully."
};

const alreadyVerified = {
  status: 400,
  message: "Your account already verified."
};

const attendMeeting = {
  status: 200,
  message: "Now you are attending Meeting."
};

const getAdminSuccess = {
  status: 200,
  message: "Get user details Successfully."
};

const tasksFilterCondition = {
  status: 400,
  message: "Please Select atleast one condition."
};

const updateTask = {
  status: 200,
  message: "Update Action sucessfully."
};

const deleteTask = {
  status: 200,
  message: "Delete Action sucessfully."
};

const catchError = {
  status: 400,
  message: "Something went wrong."
};

const searchSuccess = {
  status: 200,
  message: "Fetch Sucessfully."
};

const deleteNotificationsSuccess = {
  status: 200,
  message: "Delete Notifications Sucessfully."
};

const meetingInvalid = {
  status: 400,
  message: "Meeting id is Invalid."
};

module.exports.meetingInvalid = meetingInvalid;
module.exports.deleteNotificationsSuccess = deleteNotificationsSuccess;
module.exports.getAdminSuccess = getAdminSuccess;
module.exports.attendMeeting = attendMeeting;
module.exports.alreadyVerified = alreadyVerified;
module.exports.startMeeting = startMeeting;
module.exports.notSetPassword = notSetPassword;
module.exports.wrongOldPassword = wrongOldPassword;
module.exports.changePasswordSuccess = changePasswordSuccess;
module.exports.resetPasswordSuccess = resetPasswordSuccess;
module.exports.InvalidID = InvalidID;
module.exports.closeMeeting = closeMeeting;
module.exports.reOpenMeeting = reOpenMeeting;
module.exports.InvalidLicense = InvalidLicense;
module.exports.InvalidEmail = InvalidEmail;
module.exports.InvalidPassword = InvalidPassword;
module.exports.notExist = notExist;
module.exports.loginSuccess = loginSuccess;
module.exports.InvalidToken = InvalidToken;
module.exports.loginUnsuccess = loginUnsuccess;
module.exports.Exist = Exist;
module.exports.sendEmailSuccess = sendEmailSuccess;
module.exports.verifiedSuccess = verifiedSuccess;
module.exports.tokenExpired = tokenExpired;
module.exports.verifyEmail = verifyEmail;
module.exports.notRegistered = notRegistered;
module.exports.searchSuccess = searchSuccess;
module.exports.catchError = catchError;
module.exports.updateTask = updateTask;
module.exports.deleteTask = deleteTask;
module.exports.tasksFilterCondition = tasksFilterCondition;
module.exports.updateAttendanceStatus = updateAttendanceStatus;
module.exports.updateOrganizationSuccess = updateOrganizationSuccess;
module.exports.createAgendaSuccess = createAgendaSuccess;
module.exports.createMeetingSuccess = createMeetingSuccess;
module.exports.somethingWrong = somethingWrong;
module.exports.deleteMeetingSuccess = deleteMeetingSuccess;
module.exports.deleteContactSuccess = deleteContactSuccess;
module.exports.fetchContactsUnsuccuss = fetchContactsUnsuccuss;
module.exports.noContacts = noContacts;
module.exports.contactsSuccuss = contactsSuccuss;
module.exports.importUnsuccuss = importUnsuccuss;
module.exports.noMeetings = noMeetings;
module.exports.importSuccuss = importSuccuss;
module.exports.alreadyInvited = alreadyInvited;
module.exports.fetchUnsuccess = fetchUnsuccess;
module.exports.fetchSuccess = fetchSuccess;
module.exports.organizationExist = organizationExist;
module.exports.deleteUserSuccess = deleteUserSuccess;
module.exports.updateUserSuccess = updateUserSuccess;
module.exports.organizationSuccess = organizationSuccess;
module.exports.sendEmailUnSuccess = sendEmailUnSuccess;
module.exports.wrongDetailError = wrongDetailError;
module.exports.userType = userType;
module.exports.trialLicenseExist = trialLicenseExist;
module.exports.sameAsOldPassword = sameAsOldPassword;
module.exports.ExpiredLicense = ExpiredLicense;
