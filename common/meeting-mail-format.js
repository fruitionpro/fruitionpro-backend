"use strict";
const moment = require("moment");
const errorConsole = require("../common/catch-consoles");
const dateFormat = require("../common/format");
const url = require("./credential");
const taskController = require("../routes/task/task.controller");

/**
 * @method timeZoneAbb: A method to add time-zone abbreviation to date string
 * @param {string}  str string from previous/parent
 */
const timeZoneAbb = (date, timeZone) => {
  try {
    const tzAbbrList = [{ tz: "Asia/Singapore", abbr: "SGT" }];
    const index = tzAbbrList.findIndex(i => i.tz === timeZone);
    let str = "";
    if (index > -1) {
      str = `${moment
        .tz(date, timeZone)
        .format(dateFormat.MAIL_DATE_TIME__WITHOUT_TIMEZONE_FORMAT)} ${
        tzAbbrList[index].abbr
      }`;
    } else {
      str = moment.tz(date, timeZone).format(dateFormat.MAIL_DATE_TIME_FORMAT);
    }
    return str;
  } catch (err) {
    console.log(`${errorConsole.try_catch}timeZoneAbb`, err);
    return date;
  }
};

/**
 * @method add3Dots: A method to add three dots to very large string
 * @param {string}  str string from previous/parent
 */
const add3Dots = str => {
  try {
    if (!str || str.length <= 50) return str;
    return str.slice(0, 50) + "...";
  } catch (err) {
    console.log(`${errorConsole.try_catch}add3Dots`, err);
    return str;
  }
};

/**
 * @method meetingDateStr: A method to create meeting start/end date string
 * @param {Object}  date meeting start/end date from previous/parent
 * @param {Object}  text From/To string from previous/parent
 */
const meetingDateStr = (date, text) => {
  try {
    return `<td>
      <p style="margin-top: 0;display: inline-block;margin-right: 8px;${
        text === "From" ? "margin-bottom: 0" : ""
      }"><span style="font-weight:bold; margin-right:5px">${text} - </span> ${date}
      </p>
    </td>`;
  } catch (err) {
    console.log(`${errorConsole.try_catch}meetingDateStr`, err);
    return "";
  }
};

/**
 * @method meetingDateFormat: A method to create meeting and agenda email date and participant format
 * @param {Object}  date meeting start date from previous/parent
 * @param {Object}  participants total no. of meeting partcipants from previous/parent
 */
const meetingDateFormat = (date, participants, len) => {
  try {
    const meetingStartDate = moment(date);
    return `<td style="background: #4da8e4;color: #fff;width: 120px; padding-top: 10px; padding-bottom: 10px;">
      <table style="width: 100%;height: ${len}px;text-align: center;">
        <tr>
          <td style="vertical-align: top;">
            <img style="width: 105px;object-fit: contain; margin-bottom: 10px;" src="${
              url.logo
            }"/>
            <p style="margin-top: 0;margin-bottom: 0px;font-size: 16px; line-height:1;">${meetingStartDate.format(
              "MMM"
            )}</p>
            <p style="margin-top: 0;margin-bottom: 0px;font-size: 25px;">${meetingStartDate.format(
              "DD"
            )}</p>
            <p style="margin-top: 0;margin-bottom: 0px;font-size: 16px;line-height:1;">${meetingStartDate.format(
              "ddd"
            )}</p>
          </td>
          </tr>
          <tr>
          <td style="vertical-align: bottom;">
          <p style="margin-top: 0;margin-bottom: 4px; font-size: 20px;">${
            !!participants ? participants.length : 0
          }</p>
          <p style="margin: 0;font-size: 14px;">Invited Participants</p>
          </td>
        </tr>
      </table>
    </td>`;
  } catch (err) {
    console.log(`${errorConsole.try_catch}meetingDateFormat`, err);
    return "";
  }
};

/**
 * @method verfifyMailDateFormat: A method to create verify and any response email date format
 * @param {String}  logo email sender logo from previous/parent
 */
const verfifyMailDateFormat = logo => {
  try {
    const todayDate = new Date();
    return `<td style="background: #4da8e4;color: #fff;width: 100px;">
    <table style="width: 100%;text-align: center;${
      logo != url.logo ? "height: 220px" : ""
    }">
    ${
      logo != url.logo
        ? `<tr>
    <td style="vertical-align: baseline;padding-top: 10px;"><img style="width: 105px;object-fit: contain; margin-bottom: 10px;" src="${url.logo}"/></td>
    </tr>`
        : ""
    }
      <tr>
        <td style="${logo != url.logo ? "vertical-align: baseline;" : ""}">
          <p style="margin: 0;font-size: 18px;">${moment(todayDate).format(
            "MMM"
          )}</p>
          <p style="margin: 0;font-size: 34px;font-weight: 700;">${moment(
            todayDate
          ).format("DD")}</p>
          <p style="margin: 0;font-size: 18px;">${moment(todayDate).format(
            "ddd"
          )}</p>
        </td>
        </tr>
    </table>
</td>`;
  } catch (err) {
    console.log(`${errorConsole.try_catch}verfifyMailDateFormat`, err);
    return "";
  }
};

/**
 * @method getLocalDate: A method to get start and end date of meeting in specific time-zone
 * @param {Object}  meeting_details meeting detail from previous/parent
 * @param {Object}  time_zone time zone of specific user from previous/parent
 */
const getLocalDate = (meeting_details, time_zone) => {
  let obj = { startMeetingTime: "", endMeetingTime: "" };
  try {
    const { startDateTime, endDateTime } = meeting_details;
    if (time_zone) {
      obj.startMeetingTime = timeZoneAbb(startDateTime, time_zone);
      obj.endMeetingTime = timeZoneAbb(endDateTime, time_zone);
    } else {
      obj.startMeetingTime = new Date(startDateTime).toUTCString();
      obj.endMeetingTime = new Date(endDateTime).toUTCString();
    }
    return obj;
  } catch (err) {
    console.log(`${errorConsole.try_catch}getLocalDate`, err);
    return obj;
  }
};

/**
 * @method circleCharacter: A method to create string using first character of first and last name or email
 * @param {Object}  data meeting participant detail from previous/parent
 */
const circleCharacter = data => {
  try {
    const { name, email } = data;
    let nameArr;
    if (name) {
      nameArr = name.split(" ");
      if (nameArr.length >= 2) {
        return (
          nameArr[0].charAt(0) + nameArr[nameArr.length - 1].charAt(0)
        ).toUpperCase();
      } else {
        return (nameArr[0].charAt(0) + nameArr[0].charAt(1)).toUpperCase();
      }
    } else if (email) {
      return (email.charAt(0) + email.charAt(1)).toUpperCase();
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}circleCharacter`, err);
  }
};

/**
 * @method upperCaseFirst: A method to capitalize the first character of string
 * @param {String}  string string from previous/parent
 */
const upperCaseFirst = string => {
  try {
    if (!!string) return string.charAt(0).toUpperCase() + string.slice(1);
  } catch (err) {
    console.log(`${errorConsole.try_catch}upperCaseFirst`, err);
  }
};

/**
 * @method htmlBody: A method to create html mail format for meeting invitation to participants
 * @param {Object}  param meeting detail from previous/parent
 * @param {Object}  attendanceLink meeting attendance link from previous/parent
 * @param {Object}  user user detail from previous/parent
 */
const htmlBody = async (param, attendanceLink, user) => {
  try {
    const { meeting_details, participants } = param;
    const { time_zone, preferred_calender } = user;
    let agendas = "";
    let agendaLen = 0;
    const localDate = getLocalDate(meeting_details, time_zone);

    if (meeting_details.agendas && meeting_details.agendas.length > 0) {
      agendaLen = meeting_details.agendas.length * 24 + 39;
      agendas = agendas.concat("<table>");
      await meeting_details.agendas.forEach((element, index) => {
        agendas = agendas.concat(`
          <tr>
            <th>${index + 1}.</th>
            <td><p style="display:inline-block;margin:0">${upperCaseFirst(
              element.title
            )}</p></td>
          </tr>
          `);
      });
      agendas = agendas.concat("</table>");
    }

    const date = new Date(meeting_details.startDateTime)
      .toISOString()
      .replace(/-|:|\.\d\d\d/g, "");
    const date_end = new Date(meeting_details.endDateTime)
      .toISOString()
      .replace(/-|:|\.\d\d\d/g, "");

    return `<table style="border: 1px solid #e1e1e1;border-collapse: collapse;margin-bottom: 14px">
  <tr>
   ${meetingDateFormat(
     meeting_details.startDateTime,
     participants,
     279 + agendaLen
   )}
    <td style="width: 720px;padding:15px;vertical-align: top;">
        <table  style="width: 100%;">
          <tr>
            <td>
              <table style="width: 100%;">
                <tr>
                  <td  style="width: 40px">
                    ${
                      meeting_details.logo
                        ? ` <img
                          style="width: 40px;height: 40px;border-radius: 40px;object-fit: cover;"
                          src=${meeting_details.logo}
                        />`
                        : `<div style="width: 40px;height: 40px;font-size:18px;text-align: center;border-radius: 100%;object-fit: cover;margin-right:8px; background:#4da8e4;color:#fff;padding: 10px 0;line-height: 1;box-sizing: border-box;">
                          ${circleCharacter(meeting_details)}
                        </div>`
                    }
                  </td>
                  <td style="padding-left: 15px;">
                    <p style="margin: 0;">${taskController.titleCase(
                      meeting_details.name,
                      " "
                    )}</p>
                    <p style="margin: 0;">${meeting_details.email}</p>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td>
            <h3 style="font-size: 18px;margin-top: 0;margin-bottom: 4px;">${upperCaseFirst(
              add3Dots(meeting_details.title)
            )}</h3>
            </td>
          </tr>
          <tr>
            ${meetingDateStr(localDate.startMeetingTime, "From")}
    </tr>
    <tr>
    ${meetingDateStr(localDate.endMeetingTime, "To")}
    </tr>
           ${meeting_details.location &&
             ` <tr><td><p style="display: inline-block;font-weight:bold;margin-bottom: 0;margin-top:0">Location - </p>  <p style="margin-top: 0;display: inline-block;margin-right: 8px;">${upperCaseFirst(
               meeting_details.location
             )}</p></td></tr>`}
             
             <tr>
             <td>
            <a href="${
              preferred_calender === "Outlook"
                ? `https://outlook.live.com/owa/?path=%2Fcalendar%2Fcompose&rru=addevent&startdt=${date}&enddt=${date_end}&location=${meeting_details.location}&subject=${meeting_details.title}`
                : `https://www.google.com/calendar/event?action=TEMPLATE&text=${meeting_details.title}&dates=${date}/${date_end}&location=${meeting_details.location}`
            }" style="margin-top: 0;display: inline-block;color: #4ca8e4;cursor: pointer;margin-right: 8px;"  >Add to calendar</a>
            </td>
          </tr>
          <tr>
            <td>
            <p style="margin-bottom:5px">${participants.map(
              i => " " + i.email
            )}</p>
            </td>
          </tr>
          ${
            meeting_details.recurring
              ? `<tr>
                <td>
                  <p style="display: inline-block;margin:0;font-weight:bold">Recurrence - </p>
                  <p style = "display: inline-block;margin-bottom:0">${meeting_details.recurring}</p>
                </td>
              </tr>`
              : ""
          } 
       
          ${
            meeting_details.agendas && meeting_details.agendas.length > 0
              ? ` <tr>
              <td>
            <p style="display: inline-block;margin-top:0;font-weight:bold;font-size:18px;margin-bottom:0">Agenda List</p>
           </td>
            </tr>
            <tr>
            <td>
            ${agendas}
            </td>
            </tr>`
              : ""
          }
         
        <tr>
       ${
         attendanceLink
           ? `<td >
        <a href="${attendanceLink}yes" style="font-size: 13px;padding: 4px 8px;color: #fff;background-color: #4da8e4;text-decoration: none;margin-right: 4px;border-radius: 4px;">Yes</a>
        <a href="${attendanceLink}maybe" style="font-size: 13px;padding: 4px 8px;color: #fff;background-color: #4da8e4;text-decoration: none;margin-right: 4px;border-radius: 4px;">May Be</a>
        <a href="${attendanceLink}no" style="font-size: 13px;padding: 4px 8px;color: #fff;background-color: #4da8e4;text-decoration: none;margin-right: 4px;border-radius: 4px;">No</a>
        </td>`
           : ""
       }
      </tr>
        </table>
    </td>
  </tr>
</table>
`;
  } catch (err) {
    console.log(`${errorConsole.try_catch}htmlBody`, err);
  }
};

/**
 * @method htmlBodyAgenda: A method to create html mail format for meeting tasks assigned to participants
 * @param {Object}  param meeting detail from previous/parent
 * @param {Array}  tasks meeting tasks from previous/parent
 * @param {Object}  timeZone user time-zone from previous/parent
 * @param {Object}  updateTaskData user detail who update task from previous/parent
 */
const htmlBodyAgenda = async (param, tasks, timeZone, updateTaskData) => {
  try {
    const { time_zone } = timeZone;
    const { meeting_details, participants } = param;
    let meetingTasks = "";
    let taskLen = 0;
    const localDate = getLocalDate(meeting_details, time_zone);

    if (tasks && tasks.length > 0) {
      taskLen = tasks.length * 24 + 39;
      meetingTasks = meetingTasks.concat(
        `<h3 style="margin:0;font-size:18px">Your Action</h3>`
      );

      if (tasks.length === 1) {
        const { due_date, task } = tasks[0];
        meetingTasks = meetingTasks.concat(
          `<p>Action: ${upperCaseFirst(task)}</p>
     ${
       updateTaskData
         ? `<p>
         ${
           !updateTaskData.status
             ? upperCaseFirst(updateTaskData.user) +
               " has commented on this action."
             : updateTaskData.status
             ? upperCaseFirst(updateTaskData.user) +
               " has changed action status from <strong>" +
               updateTaskData.status.oldStatus +
               "</strong> to <strong>" +
               updateTaskData.status.newStatus +
               "</strong>."
             : ""
         }
         </p>
         `
         : due_date &&
           `<p>
        Due Date: 
        ${
          time_zone
            ? timeZoneAbb(due_date, time_zone)
            : new Date(due_date).toUTCString()
        }
      </p>`
     }
                `
        );
      } else {
        meetingTasks = meetingTasks.concat(`<table>`);

        await tasks.forEach((data, index) => {
          const { due_date, task } = data;
          meetingTasks = meetingTasks.concat(`
          <tr>
            <th>${index + 1}.</th>
            <td><p style="display:inline-block;margin:0;"><b>Action:</b></p> <p style="display:inline-block;margin:0">${upperCaseFirst(
              task
            )}</p></td>
          </tr>
          <tr>
          ${due_date &&
            `<td></td>
            <td><p style="display:inline-block;margin:0"><b>Due Date:</b></p> <p style="display:inline-block;margin:0">${
              time_zone
                ? timeZoneAbb(due_date, time_zone)
                : new Date(due_date).toUTCString()
            }</p></td>
          </tr>`}
          `);
        });
        meetingTasks = meetingTasks.concat(`</table>`);
      }
    }

    return `<table style="border: 1px solid #e1e1e1;border-collapse: collapse;margin-bottom: 14px">
    <tr>
      ${meetingDateFormat(
        meeting_details.startDateTime,
        participants,
        279 + taskLen
      )}
      <td style="width: 720px;padding: 0 15px">
          <table  style="width: 100%;">
            <tr>
              <td>
                <table style="width: 100%;">
                  <tr>
                    <td  style="width: 40px">
                      ${
                        meeting_details.logo
                          ? ` <img
                            style="width: 40px;height: 40px;border-radius: 40px;object-fit: cover;"
                            src=${meeting_details.logo}
                          />`
                          : `<div style="width: 40px;height: 40px;font-size:18px;text-align: center;border-radius: 100%;object-fit: cover;margin-right:8px; background:#4da8e4;color:#fff;padding: 10px 0;line-height: 1;box-sizing: border-box;">
                            ${circleCharacter(meeting_details)}
                          </div>`
                      }
                    </td>
                    <td style="padding-left: 15px;">
                      <p style="margin: 0;">${upperCaseFirst(
                        meeting_details.name
                      )}</p>
                      <p style="margin: 0;">${meeting_details.email}</p>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td>
              <h3 style="font-size: 18px;margin-top: 0;margin-bottom: 4px;">${upperCaseFirst(
                add3Dots(meeting_details.title)
              )}</h3>
              </td>
            </tr>
            <tr>
            ${meetingDateStr(localDate.startMeetingTime, "From")}
    </tr>
    <tr>
    ${meetingDateStr(localDate.endMeetingTime, "To")}
    </tr>
           ${meeting_details.location &&
             `  <tr><td><p style="display: inline-block;font-weight:bold;margin-bottom: 0;margin-top:0">Location - </p>  <p style="margin-top: 0;display: inline-block;margin-right: 8px;">${upperCaseFirst(
               meeting_details.location
             )}</p></td></tr>`}
            <tr>
            <td>${meetingTasks}</td>
            </tr>
            <tr>
              <td>
              <a href="${
                updateTaskData
                  ? url.viewTask + "?tid=" + updateTaskData.taskId
                  : tasks && tasks.length === 1
                  ? url.viewTask + "?tid=" + tasks[0]._id
                  : url.viewTask
              }" style="font-size: 13px;padding: 4px 8px;color: #fff;background-color: #4da8e4;text-decoration: none;margin-right: 4px;border-radius: 4px;">View Action</a>
              </td>
              </tr>
          </table>
      </td>
    </tr>
  </table>
`;
  } catch (err) {
    console.log(`${errorConsole.try_catch}htmlBodyAgenda`, err);
  }
};

/**
 * @method htmlBodyVerify: A method to create html mail format for user verification, forgot password and Organization invitation
 * @param {String}  logo organiser logo from previous/parent
 * @param {String}  name organiser name from previous/parent
 * @param {String}  email organiser email from previous/parent
 * @param {String}  subject mail subject from previous/parent
 * @param {String}  link meeting link from previous/parent
 */
const htmlBodyVerify = (logo, name, email, subject, link) => {
  try {
    return `<table style="border: 1px solid #e1e1e1;border-collapse: collapse;margin-bottom: 14px">
  <tr>
   ${verfifyMailDateFormat(logo)}
    <td style="width: 720px;    padding: 20px 16px; vertical-align: text-bottom;">
        <table  style="width: 100%;">
          <tr>
            <td style="padding-bottom: 18px;">
              <table style="width: 100%;">
                <tr>
                  <td  style="width: 40px">
                  ${
                    logo
                      ? logo === url.logo
                        ? `<img
                        style="width: 120px;object-fit: contain;"
                        src=${logo}
                      />`
                        : `<img
                        style="width: 40px;height: 40px;border-radius: 40px;object-fit: cover;"
                        src=${logo}
                      />`
                      : `<div style="width: 40px;height: 40px;font-size:18px;text-align: center;border-radius: 100%;object-fit: cover;margin-right:8px; background:#4da8e4;color:#fff;padding: 10px 0;line-height: 1;box-sizing: border-box;">
                      ${circleCharacter({ name, email })}
                    </div>`
                  }
                  </td>
                  <td style="padding-left: 15px;">
                    <p style="margin: 0;color:#000;font-size: 20px;font-weight:bold;">${upperCaseFirst(
                      name
                    )}</p>
                    <a href="mailto:${email}" >${email}</a>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td>
            <p style="margin-top: 0;display: inline-block;margin-right: 8px;margin-bottom: 12px;font-size: 20px;
            margin-bottom: 6px;color:#7b7b7b;">${subject}</p>
            </td>
          </tr>
          <tr>
          <td>
          <a href="${link}" style="margin-top: 0;display: inline-block;margin-right: 8px;font-size: 14px;">${link}</a>
          </td>
          </tr>
        </table>
    </td>
  </tr>
</table>`;
  } catch (err) {
    console.log(`${errorConsole.try_catch}htmlBodyVerify`, err);
  }
};

/**
 * @method htmlBodyMailResponse: A method to create html mail format for license, attendance response by meeting participant
 * @param {String}  logo organiser logo from previous/parent
 * @param {String}  name organiser name from previous/parent
 * @param {String}  email organiser email from previous/parent
 * @param {String}  subject mail subject from previous/parent
 * @param {String}  response response text by meeting participant from previous/parent
 */
const htmlBodyMailResponse = (logo, name, email, subject, response) => {
  try {
    return `<table style="border: 1px solid #e1e1e1;border-collapse: collapse;margin-bottom: 14px">
  <tr>
    ${verfifyMailDateFormat(logo)}
    <td style="width: 720px; padding: 20px 16px; vertical-align: text-bottom;">
        <table  style="width: 100%;">
          <tr>
            <td style="padding-bottom: 18px;">
              <table style="width: 100%;">
                <tr>
                  <td  style="width: 40px">
                  ${
                    logo
                      ? logo === url.logo
                        ? `<img
                        style="width: 120px;object-fit: contain;"
                        src=${logo}
                      />`
                        : `<img
                        style="width: 40px;height: 40px;border-radius: 40px;object-fit: cover;"
                        src=${logo}
                      />`
                      : `<div style="width: 40px;height: 40px;font-size:18px;text-align: center;border-radius: 100%;object-fit: cover;margin-right:8px; background:#4da8e4;color:#fff;padding: 10px 0;line-height: 1;box-sizing: border-box;">
                      ${circleCharacter({ name, email })}
                    </div>`
                  }
                  </td>
                  <td style="padding-left: 15px;">
                  ${
                    name === url.name
                      ? ` <p style="margin: 0;color:#000;font-size: 20px;font-weight:bold;">
                        ${upperCaseFirst(name)}
                      </p>`
                      : ` <p style="margin: 0;color:#1cbae9;font-size: 20px;">
                        ${upperCaseFirst(name)}
                      </p>`
                  }
                    <a href="mailto:${email}" >${email}</a>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td>
            <p style="margin-top: 0;display: inline-block;margin-right: 8px;margin-bottom: 12px;font-size: 20px;
            margin-bottom: 6px;color:#7b7b7b;">${subject}</p>
            </td>
          </tr>
          <tr>
          <td>
          <p style="margin-right: 8px;margin-bottom: 12px;font-size: 15px;
          margin-bottom: 6px;color:#7b7b7b;">${response}</p>
          </td>
          </tr>
        </table>
    </td>
  </tr>
</table>`;
  } catch (err) {
    console.log(`${errorConsole.try_catch}htmlBodyMailResponse`, err);
  }
};

/**
 * @method licenseMail: A method to create html mail format for license keys
 * @param {Array}  data List of license keys from previous/parent
 */
const licenseMail = data => {
  try {
    if (!data) return "";
    let license = "";
    if (data.length > 0) {
      license = license.concat(
        `<table><tr><th><h3 style="text-align: left">Your License Key</h3></th></tr>`
      );
      if (data.length > 1) {
        data.forEach((element, index) => {
          license = license.concat(`
        <tr>
          <td style="margin-left:100px"><span style="width: 25px;display: inline-block" }}>${index +
            1}.</span>${element}</td>
        </tr>
        `);
        });
      } else {
        license = license.concat(`
      <tr>
        <td style="margin-left:100px">${data[0]}</td>
      </tr>
      `);
      }
      license = license.concat("</table>");
    }
    return license;
  } catch (err) {
    console.log(`${errorConsole.try_catch}licenseMail`, err);
  }
};

/**
 * @method meetingSummary: A method to create html mail format for meeting detail
 * @param {String}  file_name meeting title from previous/parent
 */
const meetingSummary = file_name => {
  try {
    return `<div><center>
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr>
          <td valign="top" align="center">
            <table width="600" cellspacing="0" cellpadding="10" border="0" style="background-color: #fafafa">
              <tbody>
                <tr>
                  <td valign="top">
                    <table width="100%" cellspacing="0" cellpadding="10" border="0">
                      <tbody>
                        <tr>
                          <td valign="top">
                            <div></div>
                          </td>
                          <td width="190" valign="top">
                            <div></div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
            <table width="600" cellspacing="0" cellpadding="0" border="0">
              <tbody>
                <tr>
                  <td valign="top" align="center">
                    <table width="600" cellspacing="0" cellpadding="0" border="0" style="border-bottom: 2px solid #28bae6; background: #fff">
                      <tbody>
                        <tr>
                          <td>
                            <img src="${url.logo}"
                              style="max-width:600px;width: 100px;padding-left: 10px;padding-top: 10px;" alt="Fruition Pro">
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td valign="top" align="center">
                    <table width="600" cellspacing="0" cellpadding="0" border="0">
                      <tbody>
                        <tr>
                          <td valign="top">
                            <table width="100%" cellspacing="0" cellpadding="20" border="0">
                              <tbody>
                                <tr>
                                  <td valign="top">
                                    <div>
                                      <div style="margin-left:95px">
                                        <p>Please find the summary of <strong>${taskController.titleCase(
                                          file_name
                                        )}</strong> attached with this email.</p>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td valign="top" align="center">
                    <table width="600" cellspacing="0" cellpadding="10" border="0" style="background-color: #ffffff">
                      <tbody>
                        <tr>
                          <td valign="top">
                            <table width="100%" cellspacing="0" cellpadding="10" border="0">
                              <tbody>
                                <tr>
                                  <td colspan="2" valign="middle" style="background-color: #fafafa">
                                    <div></div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
            <br>
          </td>
        </tr>
      </tbody>
    </table>
  </center></div>`;
  } catch (err) {
    console.log(`${errorConsole.try_catch}meetingSummary`, err);
  }
};

/**
 * @method mailAttendance: A method to create html mail format for mail meeting attendance
 * @param {String}  option Selected option for meeting(Yes, No or Maybe) from previous/parent
 * @param {String}  res response from previous/parent
 */
const mailAttendance = (option, res) => {
  try {
    return `<div style="position: fixed; top: 50%; left: 50%; transform: translate(-50%,-50%); max-width: 100%; text-align: center;">
        <img alt="emoji" src="${url.frontendUrl}/${option}.svg"
        style="width: 100px; height: 100px;">
        <h2 style="color: #25bae9;">${res}</h2>
      </div>`;
  } catch (err) {
    console.log(`${errorConsole.try_catch}mailAttendance`, err);
  }
};

module.exports.htmlBody = htmlBody;
module.exports.htmlBodyAgenda = htmlBodyAgenda;
module.exports.htmlBodyVerify = htmlBodyVerify;
module.exports.htmlBodyMailResponse = htmlBodyMailResponse;
module.exports.licenseMail = licenseMail;
module.exports.circleCharacter = circleCharacter;
module.exports.meetingSummary = meetingSummary;
module.exports.upperCaseFirst = upperCaseFirst;
module.exports.mailAttendance = mailAttendance;
