"use strict";
const cron = require("node-cron");
const errorConsole = require("../common/catch-consoles");
const Meeting = require("../routes/meeting/meeting.model");
const mailer = require("../common/send-mail");
const moment = require("moment");
const User = require("../routes/user/user.model");
const htmlFormat = require("../common/meeting-mail-format");
const taskController = require("../routes/task/task.controller");
const dateFunction = require("../common/date-functions");
const authController = require("../routes/auth/auth.controller");
const notificationController = require("../routes/notification/notification.controller");
const credentials = require("../common/credential");
const License = require("../routes/license/license.model");
const Organisation = require("../routes/organization/organization.model");
const meetingController = require("../routes/meeting/meeting.controller");
const credential = require("../common/credential");

/**
 * @method syncMeeting: A method to import meetings and contacts from user calendar at every 1 hour between user login time
 * @param {Object}  param A user detail from previous/parent
 */
const syncMeeting = param => {
  const sync = cron.schedule(`0 */1 * * *`, () => {
    try {
      authController.importData(param, sync);
    } catch (err) {
      console.log(`${errorConsole.try_catch}syncMeeting`, err);
    }
  });
};

/**
 * @method getTimeZone: A method to get time zone
 * @param {Object}  param A user detail from previous/parent
 */
const getTimeZone = async param => {
  let timeZone = { time_zone: null, preferred_calender: null };
  const fetchBits = { time_zone: 1, _id: 0, preferred_calender: 1 };
  try {
    if (param && param.email && param.admin_email) {
      await User.findOne({ email: param.email }, fetchBits).then(
        async result => {
          if (result) {
            timeZone = { ...JSON.parse(JSON.stringify(result)) };
          } else {
            await User.findOne({ email: param.admin_email }, fetchBits).then(
              res => {
                if (res) {
                  timeZone = { ...JSON.parse(JSON.stringify(res)) };
                }
              }
            );
          }
        }
      );
    }
    return timeZone;
  } catch (err) {
    console.log(`${errorConsole.try_catch}getTimeZone`, err);
    return timeZone;
  }
};

/**
 * @method sendReminderMail: A method to send task reminder mails
 * @param {Object}  meetingObj A meeting detail from previous/parent
 * @param {Object}  taskObj A task detail from previous/parent
 */
const sendReminderMail = async (meetingObj, taskObj) => {
  try {
    const { meeting_details } = meetingObj;
    const { assign_to } = taskObj;
    const senderDetail = await meetingController.getOrganisation(
      meeting_details.email
    );
    if (taskObj && assign_to && assign_to.length > 0) {
      for (let email of assign_to) {
        const timeZone = await getTimeZone({
          email,
          admin_email: meeting_details.admin_email
        });
        let mailContent = await htmlFormat.htmlBodyAgenda(
          meetingObj,
          [taskObj],
          timeZone
        );
        mailer.sendMail(
          meeting_details.email,
          email,
          mailContent,
          "Reminder Meeting Action",
          null
        );
        notificationController.create({
          receiver: email,
          sender: meeting_details.email,
          message: `${taskController.titleCase(
            senderDetail.name,
            " "
          )} sent you <strong>${htmlFormat.upperCaseFirst(
            taskObj.task
          )}</strong> action reminder.`,
          type: "task",
          task: taskObj._id
        });
      }
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}sendReminderMail`, err);
  }
};

/**
 * @method closeMeeting: A method to close meeting after 24 hours from meeting ends
 * @param {Object}  meeting A meeting detail from previous/parent
 */
const closeMeeting = async meeting => {
  try {
    if (meeting.is_closed === true) return;
    const startDate = new Date();
    const endDate = new Date(meeting.end_date_time);
    // Calculate Days after meeting
    const days = dateFunction.dateDifference(endDate, startDate).days;
    if (days >= 1) {
      await Meeting.updateOne(
        { _id: meeting._id },
        { $set: { is_closed: true } }
      );
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}closeMeeting`, err);
  }
};

/**
 * @method taskReminder: A method to fetch data for send task reminder mails at every 1 minute
 */
const taskReminder = () => {
  cron.schedule(`*/1 * * * *`, async () => {
    try {
      Meeting.find({
        is_deleted: false
      })
        .populate({
          path: "agendas",
          populate: {
            path: "tasks",
            match: {
              $and: [
                { is_deleted: false },
                { status: { $in: ["not_started", "in_progress"] } }
              ]
            }
          }
        })
        .then(async response => {
          if (response && response.length > 0) {
            for (let i = 0; i < response.length; i += 1) {
              const meeting = JSON.parse(JSON.stringify(response[i]));
              // Call Close Meeting function
              closeMeeting(meeting);
              // Get Mail Sender (Meeting organiser)
              const senderDetail = await taskController.mailSender(
                meeting.admin_email
              );
              const agendas = !!meeting ? meeting.agendas : [];
              if (agendas && agendas.length > 0) {
                for (let i = 0; i < agendas.length; i += 1) {
                  const tasks = agendas[i].tasks;
                  if (tasks && tasks.length > 0) {
                    for (let i = 0; i < tasks.length; i += 1) {
                      const taskObj = tasks[i];
                      const taskDueDate = taskObj.due_date;
                      const timeZone = taskObj.time_zone
                        ? taskObj.time_zone
                        : "";
                      const date = moment.tz(new Date(), timeZone).utc();
                      const dueDate = moment
                        .tz(taskDueDate, timeZone)
                        .diff(date);
                      const mintues = Math.ceil(
                        moment.duration(dueDate).asMinutes()
                      );
                      if (mintues > 0) {
                        switch (mintues) {
                          case 1440:
                          case 720:
                          case 120:
                            sendReminderMail(
                              {
                                meeting_details: {
                                  ...meeting,
                                  startDateTime: meeting.start_date_time,
                                  endDateTime: meeting.end_date_time,
                                  location: meeting.address,
                                  email: meeting.admin_email,
                                  ...senderDetail
                                },
                                participants: meeting.attendees
                              },
                              taskObj
                            );
                            break;
                          default:
                            break;
                        }
                      } else {
                        if (Math.abs(mintues) > 0) {
                          switch (mintues % 1440) {
                            case 0: {
                              sendReminderMail(
                                {
                                  meeting_details: {
                                    ...meeting,
                                    startDateTime: meeting.start_date_time,
                                    endDateTime: meeting.end_date_time,
                                    location: meeting.address,
                                    email: meeting.admin_email,
                                    ...senderDetail
                                  },
                                  participants: meeting.attendees
                                },
                                taskObj
                              );
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        });
    } catch (err) {
      console.log(`${errorConsole.try_catch}taskReminder`, err);
    }
  });
};

/**
 * @method licenseExpiryReminder: A method to send notification and mail for license expiry
 */
const licenseExpiryReminder = () => {
  cron.schedule(`0 */24 * * *`, async () => {
    try {
      License.find({ assign_to: { $ne: null } }).then(async result => {
        if (result && result.length > 0) {
          const licenses = JSON.parse(JSON.stringify(result));
          for (let license of licenses) {
            if (license && license.key) {
              const days = dateFunction.dateDifference(
                new Date(),
                new Date(license.expiry_date)
              ).days;
              if (days >= 0 && days <= credentials.licExpRemDays) {
                const message =
                  days === 0
                    ? "Today your license will be expired."
                    : days === 1
                    ? "Tomorrow your license will be expired."
                    : `Your license will be expired after ${days} days.`;

                let receiver = license.assign_to;
                const organisation = await Organisation.findOne({
                  email: license.assign_to
                });
                if (organisation && organisation._id) {
                  const user = await User.findOne({
                    organization_id: String(organisation._id)
                  });
                  if (user && user.email) receiver = user.email;
                }
                const userId = await User.findOne(
                  { email: receiver },
                  { _id: 1 }
                );
                const htmlBody = htmlFormat.htmlBodyMailResponse(
                  credentials.logo,
                  credentials.name,
                  credentials.email,
                  "Please see the following license details :- ",
                  `<div>
                      <p style = "margin-bottom: 10px">${message}</p>
                      <a href="${credential.viewProfile}?uid=${userId._id}" style="font-size: 13px;padding: 4px 8px;color: #fff;background-color: #4da8e4;text-decoration: none;margin-right: 4px;border-radius: 4px;">
                        Buy License
                      </a>
                    </div>`
                );
                mailer.sendMail(
                  credentials.email,
                  receiver,
                  htmlBody,
                  "License Expiration Reminder",
                  null
                );
                notificationController.create({
                  receiver,
                  sender: credentials.email,
                  message,
                  type: "profile"
                });
              }
            }
          }
        }
      });
    } catch (err) {
      console.log(`${errorConsole.try_catch}licenseExpiryReminder`, err);
    }
  });
};

module.exports.taskReminder = taskReminder;
module.exports.syncMeeting = syncMeeting;
module.exports.licenseExpiryReminder = licenseExpiryReminder;
module.exports.getTimeZone = getTimeZone;
