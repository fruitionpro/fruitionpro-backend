const config = require("../database/config");

// system config
const systemType = "live";
const backendIP = "192.168.1.46";
const frontendIP = "localhost";

// mongodb config
const dbName = "fruitionpro"; // database name
const dbUsername = "fruitionpro"; // username
const dbPassword = "fruitionPr0*"; // password
const clusterID = "0xiz0"; // database cluster id

// trial days
const freeTrialDays = 30;

// days limit for license expiration reminder
const licExpRemDays = 7;

// stripe key
const stripeKey = "sk_test_0ge8KDOvL63ovg7zwM7ndpLH009g7Fyhp1";

// send-grid scret key
const sendMailKey =
  "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";

// frontend aws url
const frontendUrl =
  systemType === "local"
    ? `http://${frontendIP}:3000`
    : "https://www.fruitionpro.com";

const viewTask = frontendUrl + "/view/tasks";
const viewProfile = frontendUrl + "/view/profile";

// backend aws url
const backendUrl =
  systemType === "local"
    ? `http://${backendIP}:5000`
    : "https://api.fruitionpro.com";

// fruition pro config
const logo = `${frontendUrl}/logo.png`;
const name = "Fruition Pro";
const email = "fruitionpro@unimasconsulting.com";

// mongoose database url
const mongodbUrl =
  systemType === "local"
    // ? `mongodb://${config.connection.user}:${config.connection.password}@${config.connection.host}:${config.connection.port}/${config.connection.db}`
    ? `mongodb://${config.connection.host}:${config.connection.port}/${config.connection.db}`
    : `mongodb+srv://${dbUsername}:${dbPassword}@cluster0-${clusterID}.mongodb.net/${dbName}?retryWrites=true&w=majority`;

module.exports.frontendUrl = frontendUrl;
module.exports.mongodbUrl = mongodbUrl;
module.exports.backendUrl = backendUrl;
module.exports.sendMailKey = sendMailKey;
module.exports.logo = logo;
module.exports.name = name;
module.exports.email = email;
module.exports.stripeKey = stripeKey;
module.exports.viewTask = viewTask;
module.exports.freeTrialDays = freeTrialDays;
module.exports.viewProfile = viewProfile;
module.exports.licExpRemDays = licExpRemDays;
