"use strict";
const axios = require("axios");
const Meeting = require("../../routes/meeting/meeting.model");
const Attendee = require("../../routes/attendee/attendee.model");
const convert = require("xml-js");
const errorConsole = require("../catch-consoles");

/**
 * @method credentials: A method to get user detail using Google api
 * @param {String}  accessToken A access token from front-end
 */
const credentials = accessToken => {
  try {
    return axios
      .get(`https://oauth2.googleapis.com/tokeninfo?id_token=${accessToken}`)
      .then(response => {
        const { data } = response;
        if (response && data) return data;
        return null;
      })
      .catch(err => {
        console.log(`${errorConsole.api_catch}google-credentials`, err);
        return null;
      });
  } catch (err) {
    console.log(`${errorConsole.try_catch}google-credentials`, err);
    return null;
  }
};

/**
 * @method events: A method to get user events using Google calendar api
 * @param {String}  accessToken A access token from front-end
 * @param {Object}  sync A cron-job object
 */
const events = (accessToken, sync) => {
  try {
    return axios
      .get(`https://www.googleapis.com/calendar/v3/calendars/primary/events`, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${accessToken}`
        }
      })
      .then(response => {
        const { data } = response;
        if (response && data) return data;
        return null;
      })
      .catch(err => {
        if (sync) {
          sync.stop();
        } else {
          console.log(`${errorConsole.api_catch}google-events`, err);
          return null;
        }
      });
  } catch (err) {
    console.log(`${errorConsole.try_catch}google-events`, err);
    return null;
  }
};

/**
 * @method contacts: A method to get user contacts using Google contacts api
 * @param {String}  accessToken A access token from front-end
 * @param {Object}  sync A cron-job object
 */
const contacts = (accessToken, sync) => {
  try {
    return axios
      .get("https://www.google.com/m8/feeds/contacts/default/full", {
        method: "GET",
        headers: {
          Authorization: `Bearer ${accessToken}`
        }
      })
      .then(response => {
        const { data } = response;
        if (response && data) return data;
        return null;
      })
      .catch(err => {
        if (sync) {
          sync.stop();
        } else {
          console.log(`${errorConsole.api_catch}google-contacts`, err);
          return null;
        }
      });
  } catch (err) {
    console.log(`${errorConsole.try_catch}google-contacts`, err);
    return null;
  }
};

/**
 * @method storeMeetings: A method to store user calendar events in database
 * @param {String}  params A user detail from previous/parent
 * @param {Object}  sync A cron-job object
 */
const storeMeetings = async (params, sync) => {
  try {
    const eventsList = await events(params.accessToken, sync);
    if (!eventsList) return;
    const { items } = eventsList;
    if (items && items.length > 0) {
      for (const data of items) {
        const { start, end } = data;
        if (!data) break;

        const oldMeeting = await Meeting.findOne({
          meeting_id: data.id,
          is_deleted: false,
          admin_email: params.email
        });

        if (!oldMeeting) {
          let attendees = [];

          if (data.attendees && data.attendees.length > 0) {
            await data.attendees.forEach(async element => {
              const { email } = element;
              const oldAttendee = await Attendee.findOne({
                email,
                is_deleted: false,
                admin_email: params.email
              });

              if (!oldAttendee) {
                await new Attendee({
                  email,
                  admin_email: params.email
                })
                  .save()
                  .then(result => {
                    attendees.push({
                      _id: result._id
                    });
                  })
                  .catch(err => {
                    console.log(
                      `${errorConsole.api_catch}google-storeMeetings`,
                      err
                    );
                  });
              } else {
                attendees.push({ _id: oldAttendee._id });
              }
            });
          }

          let start_date_time;
          let end_date_time;

          if (start) {
            start_date_time = start.dateTime
              ? start.dateTime
              : start.date
              ? start.date
              : "";
          }

          if (end) {
            end_date_time = end.dateTime
              ? end.dateTime
              : end.date
              ? end.date
              : "";
          }

          const obj = {
            meeting_id: data.id,
            title: data.summary,
            address: data.location,
            admin_email: params.email,
            start_date_time,
            end_date_time,
            attendees
          };
          await new Meeting(obj).save().catch(err => {
            console.log(`${errorConsole.api_catch}google-storeMeetings`, err);
          });
        }
      }
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}google-storeMeetings`, err);
    return null;
  }
};

/**
 * @method storeAttendees: A method to store user calendar contacts in database
 * @param {String}  params A user detail from previous/parent
 * @param {Object}  sync A cron-job object
 */
const storeAttendees = async (params, sync) => {
  try {
    const data = await contacts(params.accessToken, sync);
    if (!data) return;
    let result = convert.xml2json(data, {
      compact: true,
      ignoreComment: true,
      spaces: 4
    });
    let newData = JSON.parse(result);

    if (
      newData &&
      newData.feed &&
      newData.feed.entry &&
      newData.feed.entry.length > 0
    ) {
      for (const item of newData.feed.entry) {
        if (
          item &&
          item["gd:email"] &&
          item["gd:email"]._attributes &&
          item["gd:email"]._attributes.address
        ) {
          const oldAttendee = await Attendee.findOne({
            email: item["gd:email"]._attributes.address,
            is_deleted: false,
            admin_email: params.email
          });

          if (!oldAttendee) {
            const { title } = item;
            const obj = {
              logo: item.id && item.id._text ? item.id._text : "",
              name: title && title._text ? title._text : null,
              contact:
                item["gd:phoneNumber"] && item["gd:phoneNumber"]._text
                  ? item["gd:phoneNumber"]._text
                  : null,
              email: item["gd:email"]._attributes.address,
              admin_email: params.email
            };

            await new Attendee(obj).save().catch(err => {
              console.log(
                `${errorConsole.api_catch}google-storeAttendees`,
                err
              );
            });
          }
        }
      }
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}google-storeAttendees`, err);
    return null;
  }
};

module.exports.credentials = credentials;
module.exports.events = events;
module.exports.contacts = contacts;
module.exports.storeMeetings = storeMeetings;
module.exports.storeAttendees = storeAttendees;
