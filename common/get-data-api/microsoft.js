"use strict";
const axios = require("axios");
const Meeting = require("../../routes/meeting/meeting.model");
const Attendee = require("../../routes/attendee/attendee.model");
const errorConsole = require("../catch-consoles");

/**
 * @method credentials: A method to get user detail using Microsoft api
 * @param {String}  accessToken A access token from front-end
 */
const credentials = accessToken => {
  try {
    return axios
      .get("https://graph.microsoft.com/v1.0/me", {
        method: "GET",
        headers: {
          Authorization: `Bearer ${accessToken}`,
          Host: "graph.microsoft.com"
        }
      })
      .then(response => {
        const { data } = response;
        if (response && data) return data;
        return null;
      })
      .catch(err => {
        console.log(`${errorConsole.api_catch}microsoft-credentials`, err);
        return null;
      });
  } catch (err) {
    console.log(`${errorConsole.try_catch}microsoft-credentials`, err);
    return null;
  }
};

/**
 * @method events: A method to get user events using Microsoft calendar api
 * @param {String}  accessToken A access token from front-end
 * @param {Object}  sync A cron-job object
 */
const events = (accessToken, sync) => {
  try {
    return axios
      .get("https://graph.microsoft.com/v1.0/me/events", {
        method: "GET",
        headers: {
          Authorization: `Bearer ${accessToken}`
        }
      })
      .then(response => {
        const { data } = response;
        if (response && data) return data;
        return null;
      })
      .catch(err => {
        if (sync) {
          sync.stop();
        } else {
          console.log(`${errorConsole.api_catch}microsoft-events`, err);
          return null;
        }
      });
  } catch (err) {
    console.log(`${errorConsole.try_catch}microsoft-events`, err);
    return null;
  }
};

/**
 * @method contacts: A method to get user contacts using Microsoft contacts api
 * @param {String}  accessToken A access token from front-end
 * @param {Object}  sync A cron-job object
 */
const contacts = (accessToken, sync) => {
  try {
    return axios
      .get("https://graph.microsoft.com/v1.0/me/contacts", {
        method: "GET",
        headers: {
          Authorization: `Bearer ${accessToken}`
        }
      })
      .then(response => {
        const { data } = response;
        if (response && data) return data;
        return null;
      })
      .catch(err => {
        if (sync) {
          sync.stop();
        } else {
          console.log(`${errorConsole.api_catch}microsoft-contacts`, err);
          return null;
        }
      });
  } catch (err) {
    console.log(`${errorConsole.try_catch}microsoft-contacts`, err);
    return null;
  }
};

/**
 * @method storeMeetings: A method to store user calendar events in database
 * @param {String}  params A user detail from previous/parent
 * @param {Object}  sync A cron-job object
 */
const storeMeetings = async (params, sync) => {
  try {
    const eventsList = await events(params.accessToken, sync);
    if (!eventsList) return;
    const { value } = eventsList;
    if (value && value.length > 0) {
      for (const data of value) {
        const { start, end } = data;
        if (!data) break;

        const oldMeeting = await Meeting.findOne({
          meeting_id: data.id,
          is_deleted: false,
          admin_email: params.email
        });

        if (!oldMeeting) {
          let start_date_time;
          let end_date_time;

          if (start) {
            start_date_time = start.dateTime ? start.dateTime : "";
          }

          if (end) {
            end_date_time = end.dateTime ? end.dateTime : "";
          }

          let attendees = [];
          if (data.attendees && data.attendees.length > 0) {
            await data.attendees.forEach(async element => {
              const { emailAddress } = element;
              const oldAttendee = await Attendee.findOne({
                email: emailAddress.address,
                is_deleted: false,
                admin_email: params.email
              });

              if (!oldAttendee) {
                await new Attendee({
                  email: emailAddress.address,
                  admin_email: params.email
                })
                  .save()
                  .then(result => {
                    attendees.push({ _id: result._id });
                  })
                  .catch(err => {
                    console.log(
                      `${errorConsole.api_catch}microsoft-storeMeetings`,
                      err
                    );
                  });
              } else {
                attendees.push({ _id: oldAttendee._id });
              }
            });
          }

          const obj = {
            meeting_id: data.id,
            title: data.subject,
            address: data.location.displayName || data.location.uniqueId,
            admin_email: params.email,
            start_date_time,
            end_date_time,
            attendees
          };

          await new Meeting(obj).save().catch(err => {
            console.log(
              `${errorConsole.api_catch}microsoft-storeMeetings`,
              err
            );
          });
        }
      }
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}microsoft-storeMeetings`, err);
    return null;
  }
};

/**
 * @method storeAttendees: A method to store user calendar contacts in database
 * @param {String}  params A user detail from previous/parent
 * @param {Object}  sync A cron-job object
 */
const storeAttendees = async (params, sync) => {
  try {
    const data = await contacts(params.accessToken, sync);
    if (!data) return;
    const { value } = data;
    if (value && value.length > 0) {
      for (const item of value) {
        const { emailAddresses, displayName, mobilePhone } = item;
        if (item && emailAddresses[0] && emailAddresses[0].address) {
          const oldAttendee = await Attendee.findOne({
            email: emailAddresses[0].address,
            is_deleted: false,
            admin_email: params.email
          });

          if (!oldAttendee) {
            const obj = {
              name: displayName ? displayName : null,
              contact: mobilePhone ? mobilePhone : null,
              email: emailAddresses[0].address,
              admin_email: params.email
            };

            await new Attendee(obj).save().catch(err => {
              console.log(
                `${errorConsole.api_catch}microsoft-storeAttendees`,
                err
              );
            });
          }
        }
      }
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}microsoft-storeAttendees`, err);
    return null;
  }
};

module.exports.credentials = credentials;
module.exports.events = events;
module.exports.contacts = contacts;
module.exports.storeAttendees = storeAttendees;
module.exports.storeMeetings = storeMeetings;
