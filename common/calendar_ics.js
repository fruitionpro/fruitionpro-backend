"use strict";
const ics = require("ics");
const errorConsole = require("../common/catch-consoles");
const moment = require("moment");
const mailFormat = require("./meeting-mail-format");
const taskController = require("../routes/task/task.controller");

/**
 * @method event: A method to create ics file event object
 * @param {Object}  data A meeting detail from previous/parent
 */
const event = data => {
  try {
    const { meeting_details, participants } = data;
    const { startDateTime, endDateTime } = meeting_details;

    const attendees = participants.map(data => ({
      name: data.name ? taskController.titleCase(data.name, " ") : data.email,
      email: data.email
    }));

    const icsObj = {
      start: moment(startDateTime)
        .format("YYYY-M-D-H-m")
        .split("-"),
      end: moment(endDateTime)
        .format("YYYY-M-D-H-m")
        .split("-"),
      title: mailFormat.upperCaseFirst(meeting_details.title),
      location: mailFormat.upperCaseFirst(meeting_details.location),
      organizer: {
        name: taskController.titleCase(meeting_details.name, " "),
        email: meeting_details.email
      },
      attendees
    };
    return icsObj;
  } catch (err) {
    console.log(`${errorConsole.try_catch}event`, err);
  }
};

/**
 * @method createICSFile: A method to create ics file to add meeting/event in calendar
 * @param {Object}  data A meeting detail from previous/parent
 */
const createICSFile = data => {
  try {
    return ics.createEvent(event(data), (err, value) => {
      if (err) {
        console.log(`${errorConsole.api_catch}createICSFile`, err);
        return;
      }
      const encodedString = Buffer.from(value).toString("base64");
      const title = data.meeting_details.title.slice(0, 15);
      const attachment = [
        {
          content: encodedString,
          filename: `${title.replace(/ +/g, "_")}.ics`,
          type: "text/calendar",
          disposition: "attachment"
        }
      ];
      return attachment;
    });
  } catch (err) {
    console.log(`${errorConsole.try_catch}createICSFile`, err);
  }
};

module.exports.createICSFile = createICSFile;
