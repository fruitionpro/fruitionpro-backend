"use strict";
const express = require("express");
const controller = require("./task.controller");
const router = express.Router();

router.post("/fetch-tasks", controller.fetch_tasks);
router.post("/filter-tasks", controller.filter_tasks);
router.post("/my-organiser-all-tasks", controller.my_organiser_all_tasks);
router.post("/update-task", controller.updateTask);
router.post("/delete", controller.deleteTask);
router.post("/reminder", controller.reminder);

module.exports = router;
