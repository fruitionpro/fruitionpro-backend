"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let taskSchema = new mongoose.Schema(
  {
    task: { type: String, default: null },
    start_date: { type: Date, default: null },
    end_date: { type: Date, default: null },
    due_date: { type: Date, default: null },
    assign_to: [String],
    status: { type: String, default: "not_started" },
    is_closed: { type: Boolean, default: false },
    time_zone: { type: String, default: null },
    documents: [Object],
    comments: [{ type: Schema.Types.ObjectId, ref: "comment" }],
    is_deleted: { type: Boolean, default: false }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("task", taskSchema);
