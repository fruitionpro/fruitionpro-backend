"use strict";
const mongoose = require("mongoose");
const Task = require("./task.model");
const Meeting = require("../meeting/meeting.model");
const Organization = require("../organization/organization.model");
const User = require("../user/user.model");
const ObjectID = mongoose.Types.ObjectId;
const errorConsole = require("../../common/catch-consoles");
const responses = require("../../common/responses");
const _ = require("lodash");
const dateFunction = require("../../common/date-functions");
const commentController = require("../comment/comment.controller");
const mailer = require("../../common/send-mail");
const htmlFormat = require("../../common/meeting-mail-format");
const meetingController = require("../meeting/meeting.controller");
const notificationController = require("../notification/notification.controller");
const crobJob = require("../../common/cron-job");

/**
 * @method findUser: A method to fetch user detail
 * @param {String}  email mail address from previous/parent
 */
const findUser = async email => {
  try {
    return await User.findOne({ email }, { logo: 1, _id: 0, name: 1 }).then(
      res => {
        if (res) {
          const obj = JSON.parse(JSON.stringify(res));
          return obj;
        }
      }
    );
  } catch (err) {
    console.log(`${errorConsole.try_catch}findUser`, err);
    return "";
  }
};

/**
 * @method taskMailSender: A method to send task mails meeting participants
 * @param {Object}  meetingObj meeting detail from previous/parent
 * @param {Object}  taskObj meeting detail from previous/parent
 * @param {Object}  updateTaskData meeting detail from previous/parent
 * @param {String}  sender sender mail address from previous/parent
 * @param {String}  mailSubject mail content from previous/parent
 */
const taskMailSender = async (
  meetingObj,
  taskObj,
  updateTaskData,
  sender,
  mailSubject,
  company
) => {
  try {
    for (let email of taskObj.assign_to) {
      const timeZone = await crobJob.getTimeZone({
        email,
        admin_email: sender
      });
      let mailContent = await htmlFormat.htmlBodyAgenda(
        meetingObj,
        [taskObj],
        timeZone,
        updateTaskData
      );
      mailer.sendMail(sender, email, mailContent, mailSubject, null);
      const senderDetail = await meetingController.getOrganisation(sender);
      notificationController.create({
        receiver: email,
        sender,
        message: `${
          updateTaskData
            ? !updateTaskData.status
              ? titleCase(senderDetail.name, " ") +
                ` has commented on <strong>${htmlFormat.upperCaseFirst(
                  taskObj.task
                )}</strong> action.`
              : updateTaskData.status
              ? titleCase(senderDetail.name, " ") +
                ` has changed <strong>${htmlFormat.upperCaseFirst(
                  taskObj.task
                )}</strong> action status from <strong>` +
                updateTaskData.status.oldStatus +
                "</strong> to <strong>" +
                updateTaskData.status.newStatus +
                "</strong>."
              : ""
            : `${titleCase(
                company,
                " "
              )} sent you <strong>${htmlFormat.upperCaseFirst(
                taskObj.task
              )}</strong> action reminder.`
        }`,
        type: "task",
        task: taskObj._id
      });
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}taskMailSender`, err);
    return null;
  }
};

/**
 * @method organiserTasks: A method to fetch organiser meetings
 * @param {String}  email organiser mail address from previous/parent
 */
const organiserTasks = async email => {
  try {
    return await Meeting.find(
      {
        admin_email: email,
        agendas: { $not: { $size: 0 } },
        is_deleted: false
      },
      { title: 1, department: 1, project_name: 1, admin_email: 1 }
    ).populate({
      path: "agendas",
      select: { tasks: 1, _id: 0 },
      tasks: { $not: { $size: 0 } },
      populate: {
        path: "tasks",
        match: { is_deleted: false },
        populate: {
          path: "comments"
        }
      }
    });
  } catch (err) {
    console.log(`${errorConsole.try_catch}organiserTasks`, err);
    return [];
  }
};

/**
 * @method myTasks: A method to fetch participant meetings
 * @param {String}  email participant mail address from previous/parent
 */
const myTasks = async email => {
  try {
    return await Meeting.find(
      {
        admin_email: { $ne: email },
        agendas: { $not: { $size: 0 } },
        is_deleted: false
      },
      { title: 1, department: 1, project_name: 1, admin_email: 1 }
    ).populate({
      path: "agendas",
      select: { tasks: 1, _id: 0 },
      tasks: { $not: { $size: 0 } },
      populate: {
        path: "tasks",
        match: {
          assign_to: { $in: [email] },
          is_deleted: false
        },
        populate: {
          path: "comments"
        }
      }
    });
  } catch (err) {
    console.log(`${errorConsole.try_catch}myTasks`, err);
    return [];
  }
};

/**
 * @method titleCase: A method to capitalize the string
 * @param {String}  str string from previous/parent
 * @param {String}  splitBy split by string from previous/parent
 */
const titleCase = (str, splitBy = "_") => {
  try {
    if (!str) return "";
    const splitStr = str.toLowerCase().split(splitBy);
    for (let i = 0; i < splitStr.length; i++) {
      splitStr[i] =
        splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(" ");
  } catch (err) {
    console.log(`${errorConsole.try_catch}titleCase`, err);
    return "";
  }
};

/**
 * @method filterTaskByStatus: A method to capitalize the string
 * @param {Array}  meetings List of meetings from previous/parent
 * @param {String}  email email id of user from previous/parent
 */
const filterTaskByStatus = async (meetings, email) => {
  try {
    // My tasks
    let mtns = [], // not-started
      mtip = [], // in-progress
      mtc = [], // completed
      mtod = [], // over-due
      mta = [], // archived
      // Organiser tasks
      otns = [], // not-started
      otip = [], // in-progress
      otc = [], // completed
      otod = [], // over-due
      ota = []; // archived

    for (let meeting of meetings) {
      for (let agenda of meeting.agendas) {
        if (agenda && agenda.tasks && agenda.tasks.length > 0) {
          for (let task of agenda.tasks) {
            task.status = titleCase(task.status);
            const { status, due_date, assign_to, comments } = task;
            let found = assign_to.findIndex(value => {
              return value === email;
            });

            const assignees = [];
            for (let el of assign_to) {
              let user = await findUser(el);
              if (user) assignees.push({ ...user, email: el });
              else assignees.push({ email: el });
            }

            let commentsArr = [];
            if (comments && comments.length > 0) {
              for (let ct of comments) {
                let user = await findUser(ct.email);
                if (user) commentsArr.push({ ...user, ...ct });
                else commentsArr.push(ct);
              }
            }

            let taskDetails = {
              ...task,
              meeting_id: meeting._id,
              ..._.omit(meeting, ["agendas", "_id"]),
              assign_to: assignees,
              comments: commentsArr
            };

            if (found === -1) {
              if (status === "Cancelled" || status === "Postponed") {
                ota.push(taskDetails);
              } else if (status === "Completed") otc.push(taskDetails);
              // over due
              else if (dateFunction.isPastDate(due_date)) {
                otod.push({ ...taskDetails, isOverDue: true });
              } else if (status === "Not Started") {
                otns.push(taskDetails);
              } else otip.push(taskDetails); // in progress
            } else {
              if (status === "Cancelled" || status === "Postponed") {
                mta.push(taskDetails);
              } else if (status === "Completed") mtc.push(taskDetails);
              // over due
              else if (dateFunction.isPastDate(due_date)) {
                mtod.push({ ...taskDetails, isOverDue: true });
              } else if (status === "Not Started") {
                mtns.push(taskDetails);
              } else mtip.push(taskDetails); // in progress
            }
          }
        }
      }
    }

    // Total tasks
    const ttns = [...otns, ...mtns], // not-started
      ttip = [...otip, ...mtip], // in-progress
      ttc = [...otc, ...mtc], // completed
      ttod = [...otod, ...mtod], // over-due
      tta = [...ota, ...mta]; // archived

    const allTask = {
      not_started: {
        type: "not_started",
        tasks: ttns,
        totalTasksCount: ttns.length,
        mytasksCount: mtns.length,
        organizerTasksCount: otns.length
      },
      in_progress: {
        type: "in_progress",
        tasks: ttip,
        totalTasksCount: ttip.length,
        mytasksCount: mtip.length,
        organizerTasksCount: otip.length
      },
      completed: {
        type: "completed",
        tasks: ttc,
        totalTasksCount: ttc.length,
        mytasksCount: mtc.length,
        organizerTasksCount: otc.length
      },
      over_due: {
        type: "over_due",
        tasks: ttod,
        totalTasksCount: ttod.length,
        mytasksCount: mtod.length,
        organizerTasksCount: otod.length
      },
      archived: {
        type: "archived",
        tasks: tta,
        totalTasksCount: tta.length,
        mytasksCount: mta.length,
        organizerTasksCount: ota.length
      }
    };
    return allTask;
  } catch (err) {
    console.log(`${errorConsole.try_catch}filterTaskByStatus`, err);
    return [];
  }
};

/**
 * @method create_UUID: A method to create unique id based on current time
 */
const create_UUID = () => {
  try {
    let dt = new Date().getTime();
    let uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, c => {
      let r = (dt + Math.random() * 16) % 16 | 0;
      dt = Math.floor(dt / 16);
      return (c == "x" ? r : (r & 0x3) | 0x8).toString(16);
    });
    return uuid;
  } catch (err) {
    console.log(`${errorConsole.try_catch}create_UUID`, err);
    return "";
  }
};

/**
 * @method subGroup: A method to create groups by meeting project
 * @param {Array}  data List of records from previous/parent
 */
const subGroup = data => {
  try {
    return _.mapValues(
      _.groupBy(data, n =>
        _.trim(n.project_name) === "Other" ||
        n.project_name === null ||
        _.trim(n.project_name) === ""
          ? "Others"
          : _.trim(n.project_name)
      ),
      clist => clist.map(task => task.title)
    );
  } catch (err) {
    console.log(`${errorConsole.try_catch}subGroup`, err);
    return [];
  }
};

/**
 * @method filterDataByGroups: A method to create groups by meeting department
 * @param {Array}  tasks List of tasks from previous/parent
 */
const filterDataByGroups = tasks => {
  try {
    const grouped = _.mapValues(
      _.groupBy(tasks, task =>
        _.trim(task.department) === "Other" ||
        task.department === null ||
        _.trim(task.department) === ""
          ? "Others"
          : _.trim(task.department)
      ),
      clist => {
        const gp = clist.map(task => ({
          project_name: task.project_name,
          title: task.title
        }));
        let gpsByProjectname = subGroup(gp);
        for (let project of Object.keys(gpsByProjectname)) {
          gpsByProjectname[project] = _.uniq(gpsByProjectname[project]);
          let newTitles = [];
          for (let title of gpsByProjectname[project]) {
            newTitles.push({ title, title_id: create_UUID() });
          }
          gpsByProjectname[project] = newTitles;
        }
        return gpsByProjectname;
      }
    );

    let final = [];
    for (let item of Object.keys(grouped)) {
      let obj = {
        _id: {
          department: item,
          department_id: create_UUID()
        }
      };
      let children = [];
      let nestedGp = grouped[item];
      for (let item1 of Object.keys(nestedGp)) {
        const obj1 = {
          project_name: item1,
          project_name_id: create_UUID(),
          titles: nestedGp[item1]
        };
        children.push(obj1);
      }
      obj = {
        ...obj,
        children
      };
      final.push(obj);
    }
    return final;
  } catch (err) {
    console.log(`${errorConsole.try_catch}filterDataByGroups`, err);
    return [];
  }
};

/**
 * @method mailSender: A method to get user detail for mailing
 * @param {String}  email organisation mail address from previous/parent
 */
const mailSender = async email => {
  try {
    return await Organization.findOne(
      {
        email
      },
      { name: 1, logo: 1, _id: 0 }
    ).then(async result => {
      if (result) {
        const obj = JSON.parse(JSON.stringify(result));
        return obj;
      } else {
        let user = await findUser(email);
        return user;
      }
    });
  } catch (err) {
    console.log(`${errorConsole.try_catch}mailSender`, err);
    return res.json(responses.catchError);
  }
};

/**
 * @method upsert: A method to create/update the task detail
 * @param {Array}  param List of meeting tasks from frontend
 */
const upsert = async param => {
  try {
    const taskIds = [];
    for (const data of param) {
      const { _id, assign_to } = data;
      if (_id) {
        await Task.updateOne({ _id: ObjectID(_id) }, { $set: data })
          .then(result => {
            if (result) taskIds.push(_id);
          })
          .catch(err => {
            console.log(`${errorConsole.api_catch}upsert-task-updateOne`, err);
          });
      } else {
        if (
          data &&
          data.task &&
          assign_to &&
          assign_to.length > 0 &&
          data.due_date
        ) {
          await new Task(data)
            .save()
            .then(result => {
              if (result) taskIds.push(result._id);
            })
            .catch(err => {
              console.log(`${errorConsole.api_catch}upsert-task-new-task`, err);
            });
        }
      }
    }
    return taskIds;
  } catch (err) {
    console.log(`${errorConsole.try_catch}upsert-task`, err);
    return [];
  }
};

/**
 * @method filter_tasks: A method to filter tasks by to departments, titles, projects and due-date of specific user
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const filter_tasks = async (req, res) => {
  try {
    const { email, departments, titles, due_date } = req.body;
    let { project_names } = req.body;
    req.checkBody("email", "User email id is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    let organiserTaskCondition = [
      { assign_to: { $nin: [email] } },
      { is_deleted: false }
    ];
    let myTaskCondition = [
      { assign_to: { $elemMatch: { $eq: email } } },
      { is_deleted: false }
    ];

    if (due_date) {
      let nextDate = new Date(due_date);
      // add a day
      nextDate.setDate(nextDate.getDate() + 1);

      let cond = {
        due_date: {
          $gte: new Date(due_date).toUTCString(),
          $lt: new Date(nextDate).toUTCString()
        }
      };
      organiserTaskCondition.push(cond);
      myTaskCondition.push(cond);
    }

    let filterCondition = [
      { agendas: { $not: { $size: 0 } } },
      { is_deleted: false }
    ];
    if (departments && departments.length > 0) {
      if (departments.includes("Others")) {
        departments.push(null, "", "Other");
        let index = departments.indexOf("Others");
        if (index > -1) departments.splice(index, 1);
      }
      filterCondition.push({ department: { $in: departments } });
    }
    if (titles && titles.length > 0) {
      filterCondition.push({ title: { $in: _.uniq(titles) } });
    }
    if (project_names && project_names.length > 0) {
      project_names = _.uniq(project_names);
      if (project_names.includes("Others")) {
        project_names.push(null, "", "Other");
        let index = project_names.indexOf("Others");
        if (index > -1) project_names.splice(index, 1);
      }
      filterCondition.push({ project_name: { $in: project_names } });
    }

    Promise.all([
      Meeting.find(
        { $and: [...filterCondition, { admin_email: email }] },
        {
          title: 1,
          department: 1,
          project_name: 1,
          admin_email: 1
        }
      ).populate({
        path: "agendas",
        select: { tasks: 1, _id: 0 },
        populate: {
          path: "tasks",
          match: {
            $and: organiserTaskCondition
          },
          populate: {
            path: "comments"
          }
        }
      }),
      Meeting.find(
        { $and: filterCondition },
        { title: 1, department: 1, project_name: 1, admin_email: 1 }
      ).populate({
        path: "agendas",
        select: { tasks: 1, _id: 0 },
        populate: {
          path: "tasks",
          match: {
            $and: myTaskCondition
          },
          populate: {
            path: "comments"
          }
        }
      })
    ]).then(async result => {
      const organiserMeetings = JSON.parse(JSON.stringify(result[0]));
      const myMeetings = JSON.parse(JSON.stringify(result[1]));
      const meetings = [...organiserMeetings, ...myMeetings];
      const allTask = await filterTaskByStatus(meetings, email);

      return res.json({ ...responses.fetchSuccess, allTask });
    });
  } catch (err) {
    console.log(`${errorConsole.try_catch}filter_tasks`, err);
    return res.json(responses.fetchUnsuccess);
  }
};

/**
 * @method fetch_tasks: A method to fetch tasks of specific user
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const fetch_tasks = async (req, res) => {
  try {
    const { email } = req.body;
    req.checkBody("email", "User email id is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    Promise.all([organiserTasks(email), myTasks(email)]).then(async result => {
      const organiserMeetings = JSON.parse(JSON.stringify(result[0]));
      const myMeetings = JSON.parse(JSON.stringify(result[1]));
      const meetings = [...organiserMeetings, ...myMeetings];
      const allTask = await filterTaskByStatus(meetings, email);
      let totalTask = [];
      for (let data of Object.keys(allTask)) {
        totalTask.push(...allTask[data].tasks);
      }
      let filterData = [];
      if (totalTask && totalTask.length > 0) {
        filterData = filterDataByGroups(totalTask);
      }
      const finalTasks = { ...allTask, filterData };
      return res.json({ ...responses.fetchSuccess, allTask: finalTasks });
    });
  } catch (err) {
    console.log(`${errorConsole.try_catch}fetch_tasks`, err);
    return res.json(responses.fetchUnsuccess);
  }
};

/**
 * @method updateTask: A method to modify task and send mail for task updation to others
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const updateTask = async (req, res) => {
  try {
    const {
      id,
      status,
      start_date,
      end_date,
      documents,
      comments,
      email,
      meeting_id
    } = req.body;

    req.checkBody("id", "Task id is empty").notEmpty();
    req.checkBody("email", "User email id is empty").notEmpty();
    req.checkBody("meeting_id", "Meeting id is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    let commentIds = [];
    if (comments && comments.length > 0)
      commentIds = await commentController.create(comments);

    const task = await Task.findOne({ _id: ObjectID(id) }).then(
      async result => {
        if (result) return JSON.parse(JSON.stringify(result));
        return result;
      }
    );

    let taskObj;
    let finalTask;
    let newStatus;
    if (status) {
      if (status === "Not Started") {
        taskObj = {
          ...taskObj,
          status: "not_started",
          start_date: null,
          end_date: null
        };
        newStatus = "not_started";
      } else if (status === "In Progress") {
        taskObj = {
          ...taskObj,
          status: "in_progress"
        };
        newStatus = "in_progress";
      } else if (status === "Completed") {
        taskObj = {
          ...taskObj,
          status: "completed"
        };
        newStatus = "completed";
      } else if (status === "Cancelled") {
        taskObj = {
          ...taskObj,
          status: "cancelled"
        };
        newStatus = "cancelled";
      } else if (status === "Postponed") {
        taskObj = {
          ...taskObj,
          status: "postponed"
        };
        newStatus = "postponed";
      }
    }
    if (start_date) {
      taskObj = {
        ...taskObj,
        start_date
      };
    }
    if (end_date) {
      taskObj = {
        ...taskObj,
        end_date
      };
    }
    if (documents) {
      taskObj = {
        ...taskObj,
        documents
      };
    }
    if (commentIds && commentIds.length > 0) {
      finalTask = {
        $set: taskObj,
        $push: { comments: commentIds }
      };
    } else {
      finalTask = {
        $set: taskObj
      };
    }
    await Task.updateOne({ _id: ObjectID(id) }, finalTask).then(async () => {
      if ((commentIds && commentIds.length > 0) || newStatus !== task.status) {
        let meeting = await Meeting.findOne({
          _id: ObjectID(meeting_id)
        }).then(async result => {
          if (result) return JSON.parse(JSON.stringify(result));
          return result;
        });
        const { admin_email } = meeting;
        const user = await User.findOne({ email });
        const meetingObj = {
          meeting_details: {
            ...meeting,
            startDateTime: meeting.start_date_time,
            endDateTime: meeting.end_date_time,
            location: meeting.address,
            email,
            logo: user.logo,
            name: user.name
          },
          participants: meeting.attendees
        };

        let { assign_to } = task;
        if (!assign_to.includes(admin_email)) assign_to.push(admin_email);
        let index = assign_to.indexOf(email);
        if (index > -1) assign_to.splice(index, 1);
        if (commentIds && commentIds.length > 0) {
          let updateTaskData = {
            user: user.name,
            taskId: id
          };
          taskMailSender(
            meetingObj,
            { ...task, assign_to },
            updateTaskData,
            email,
            "Update Action Mail"
          );
        }

        if (newStatus !== task.status) {
          let updateTaskData = {
            user: user.name,
            status: { oldStatus: titleCase(task.status), newStatus: status },
            taskId: id
          };
          taskMailSender(
            meetingObj,
            { ...task, assign_to },
            updateTaskData,
            email,
            "Update Action Mail"
          );
        }
      }
      return res.json(responses.updateTask);
    });
  } catch (err) {
    console.log(`${errorConsole.try_catch}update-task`, err);
    return res.json(responses.catchError);
  }
};

/**
 * @method deleteTask: A method to soft delete task
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const deleteTask = async (req, res) => {
  try {
    const { id, admin_email } = req.body;
    req.checkBody("id", "Task id is empty").notEmpty();
    req
      .checkBody(
        "admin_email",
        "Task organiser email(Meeting admin) id is empty"
      )
      .notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }
    const task = await Task.findOne({ _id: ObjectID(id) });
    await Task.updateOne(
      { _id: ObjectID(id) },
      { $set: { is_deleted: true } }
    ).then(async result => {
      if (result) {
        if (task.assign_to.length > 0) {
          // Get organisation by creator email id
          const senderDetail = await meetingController.getOrganisation(
            admin_email
          );
          const user = await User.findOne({ email: admin_email });

          const message = `${titleCase(
            senderDetail.name,
            " "
          )} has deleted your <strong>${htmlFormat.upperCaseFirst(
            task.task
          )}</strong> task.`;

          const htmlBody = htmlFormat.htmlBodyMailResponse(
            user.logo,
            user.name,
            user.email,
            "Please see the following meeting action details:-",
            message
          );
          mailer.sendMail(
            admin_email,
            task.assign_to.filter(i => {
              if (i !== admin_email) return i;
            }),
            htmlBody,
            "Meeting Action Cancellation",
            null
          );

          for (let i of task.assign_to) {
            notificationController.create({
              receiver: i,
              sender: admin_email,
              message,
              type: "task",
              task: ObjectID(id)
            });
          }
        }
      }
    });
    return res.json(responses.deleteTask);
  } catch (err) {
    console.log(`${errorConsole.try_catch}deleteTask-task`, err);
    return res.json(responses.catchError);
  }
};

/**
 * @method reminder: A method to send task reminder mail
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const reminder = async (req, res) => {
  try {
    const { task_id, meeting_id, assignee_email } = req.body;
    req.checkBody("task_id", "Task id is empty").notEmpty();
    req.checkBody("meeting_id", "Meeting id is empty").notEmpty();
    req
      .checkBody("assignee_email", "Assignee email id is empty")
      .isArray()
      .notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    let meeting;
    let task;
    await Meeting.findOne({ _id: ObjectID(meeting_id) }).then(result => {
      const obj = JSON.parse(JSON.stringify(result));
      meeting = { ...obj };
    });
    await Task.findOne({ _id: ObjectID(task_id) }).then(result => {
      const obj = JSON.parse(JSON.stringify(result));
      task = { ...obj };
    });

    // Get Mail Sender (Meeting organiser)
    const senderDetail = await mailSender(meeting.admin_email);

    const meetingObj = {
      meeting_details: {
        ...meeting,
        startDateTime: meeting.start_date_time,
        endDateTime: meeting.end_date_time,
        location: meeting.address,
        email: meeting.admin_email,
        ...senderDetail
      },
      participants: meeting.attendees
    };

    let index = assignee_email.indexOf(meeting.admin_email);
    if (index > -1) assignee_email.splice(index, 1);

    const admin = await meetingController.getOrganisation(meeting.admin_email);
    if (assignee_email && assignee_email.length > 0) {
      taskMailSender(
        meetingObj,
        { ...task, assign_to: assignee_email },
        null,
        meeting.admin_email,
        "Task Reminder",
        admin.name
      );
    } else return res.json({ status: 400, message: "This is your task" });

    res.json(responses.sendEmailSuccess);
  } catch (err) {
    console.log(`${errorConsole.try_catch}reminder-task`, err);
    return res.json(responses.catchError);
  }
};

/**
 * @method my_organiser_all_tasks: A method to fetch Organize/Assigned tasks of specific user
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const my_organiser_all_tasks = async (req, res) => {
  try {
    const { email, label } = req.body;
    req.checkBody("email", "User email id is empty").notEmpty();
    req
      .checkBody("label", "User label(My, Organiser or Total Actions) is empty")
      .notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    Promise.all([
      meetingController.patternMatch(label, "Total")
        ? organiserTasks(email)
        : meetingController.patternMatch(label, "Organiser")
        ? organiserTasks(email)
        : [],
      meetingController.patternMatch(label, "Total")
        ? myTasks(email)
        : meetingController.patternMatch(label, "My")
        ? myTasks(email)
        : []
    ]).then(async result => {
      const organiserMeetings = JSON.parse(JSON.stringify(result[0]));
      const myMeetings = JSON.parse(JSON.stringify(result[1]));
      const meetings = [...organiserMeetings, ...myMeetings];
      const allTask = await filterTaskByStatus(meetings, email);
      return res.json({ ...responses.fetchSuccess, allTask });
    });
  } catch (err) {
    console.log(`${errorConsole.try_catch}my_organiser_tasks`, err);
    return res.json(responses.catchError);
  }
};

module.exports.titleCase = titleCase;
module.exports.mailSender = mailSender;
module.exports.create_UUID = create_UUID;
module.exports.upsert = upsert;
module.exports.filter_tasks = filter_tasks;
module.exports.my_organiser_all_tasks = my_organiser_all_tasks;
module.exports.reminder = reminder;
module.exports.deleteTask = deleteTask;
module.exports.fetch_tasks = fetch_tasks;
module.exports.updateTask = updateTask;
module.exports.findUser = findUser;
