"use strict";
const mongoose = require("mongoose");
const responses = require("../../common/responses");
const microsoftMethods = require("../../common/get-data-api/microsoft");
const googleMethods = require("../../common/get-data-api/google");
const Meeting = require("../meeting/meeting.model");
const Attendee = require("../attendee/attendee.model");
const User = require("../user/user.model");
const ObjectID = mongoose.Types.ObjectId;
const mailer = require("../../common/send-mail");
const _ = require("lodash");
const meetingRecurrence = require("./recurrence/meeting");
const url = require("../../common/credential");
const crypto = require("crypto");
const agendaController = require("../agenda/agenda.controller");
const errorConsole = require("../../common/catch-consoles");
const htmlPdf = require("html-pdf");
const middleware = require("../../middleware");
const htmlFormat = require("../../common/meeting-mail-format");
const icsFile = require("../../common/calendar_ics");
const notificationController = require("../notification/notification.controller");
const taskController = require("../task/task.controller");
const Organisation = require("../organization/organization.model");
const crobJob = require("../../common/cron-job");
const moment = require('moment-timezone');

/**
 * @method organizerInvitation: A method to send invite to organizer
 * @param {Object}  param meeting detail from previous/parent
 * @param {String}  icsContent meeting summary from previous/parent
 * @param {String}  inviteLink application link from previous/parent
 */
const organizerInvitation = async (param, icsContent, inviteLink) => {
  try {
    const { meeting_details, participants } = param;
    const index = participants.findIndex(
      i => i.email === meeting_details.email
    );
    if (index === -1) {
      const user = await crobJob.getTimeZone({
        email: meeting_details.email,
        admin_email: meeting_details.email
      });

      let mailContent = await htmlFormat.htmlBody(param, "", user);

      mailer.sendMail(
        meeting_details.email,
        meeting_details.email,
        mailContent.concat(
          `<div><a href="${inviteLink}" style="font-size: 15px;
        padding: 6px 11px;
        color: #fff;
        background-color: #4da8e4;
        text-decoration: none;
        border-radius: 5px;
        margin: 0 0 10px 122px;" >Join Meeting</a></div>`
        ),
        "Meeting Invitation",
        icsContent
      );
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}organizerInvitation`, err);
    return [];
  }
};

/**
 * @method createAgendas: A method to create agendas
 * @param {Array}  data list of agendas from previous/parent
 */
const createAgendas = async data => {
  try {
    let ids = [];
    if (data && data.length > 0) {
      const agendas = await agendaController.create(data);
      if (agendas && agendas.length > 0) {
        for (const agenda of agendas) {
          ids.push(agenda._id);
        }
      }
    }
    return ids;
  } catch (err) {
    console.log(`${errorConsole.try_catch}createAgendas`, err);
    return [];
  }
};

/**
 * @method attendeeCondition: A method to add meeting participant conditions
 * @param {String}  email attendee mail id from previous/parent
 */
const attendeeCondition = async email => {
  try {
    const attendeeCond = [{ attendees: { $not: { $size: 0 } } }];
    await Attendee.find(
      { $and: [{ email }, { is_deleted: false }] },
      { _id: 1 }
    ).then(result => {
      if (result && result.length > 0) {
        const attendees = result.map(i => ObjectID(i._id));
        attendeeCond.push({
          attendees: {
            $elemMatch: {
              _id: { $in: attendees }
            }
          }
        });
      }
    });
    return attendeeCond;
  } catch (err) {
    console.log(`${errorConsole.try_catch}attendeeCondition`, err);
    return [];
  }
};

/**
 * @method getOrganisation: A method to get organisation detail for notification
 * @param {String}  email user mail address from previous/parent
 */
const getOrganisation = async email => {
  try {
    return await User.findOne(
      { email },
      { organization_id: 1, name: 1, _id: 0, type: 1 }
    ).then(async result => {
      if (result && result.organization_id && result.type !== "invitedUser") {
        const organisation = await Organisation.findOne(
          {
            _id: ObjectID(result.organization_id)
          },
          { _id: 0, name: 1 }
        );
        return JSON.parse(JSON.stringify(organisation));
      } else return JSON.parse(JSON.stringify(result));
    });
  } catch (err) {
    console.log(`${errorConsole.try_catch}getOrganisation`, err);
    return null;
  }
};

/**
 * @method add3Dots: A method to add three dots when string is very large
 * @param {String}  str string from previous/parent
 * @param {String}  keyword searched text from frontend
 */
const add3Dots = (str, keyword) => {
  try {
    const lenStr = str.length; // length of str
    const lenKwd = keyword.length; // length of keyword
    if (Math.abs(lenStr - lenKwd) <= 30) return str;
    const index = str.indexOf(keyword);
    let leftStr, rgtStr; // left and right sub-strings from keyword in str
    if (index > 15) leftStr = str.substring(Math.abs(index - 15), index);
    else leftStr = str.substring(0, index);
    const rgtInd = index + lenKwd; // right sub-string starting index
    if (str.substring(rgtInd).length > 15) {
      rgtStr = str.substring(rgtInd, rgtInd + 15) + "...";
    } else rgtStr = str.substring(rgtInd);
    return leftStr + keyword + rgtStr;
  } catch (err) {
    console.log(`${errorConsole.try_catch}add3Dots`, err);
  }
};

/**
 * @method findInNotes: A method to find keyword in notes
 * @param {String}  notes string from previous/parent
 * @param {String}  keyword searched text from frontend
 */
const findInNotes = (notes, keyword) => {
  try {
    if (notes) {
      const lines = notes.split("\n");
      for (let line of lines) {
        if (patternMatch(line, keyword)) return line;
      }
      return "";
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}findInNotes`, err);
  }
};

/**
 * @method findItem: A method to find keyword in records
 * @param {Array}  data List of records from previous/parent
 * @param {String}  matched meeting bit from previous/parent
 * @param {String}  keyword searched text from frontend
 */
const findItem = (data, matched, keyword) => {
  try {
    if (data && data.length > 0) {
      for (let item of data) {
        let att = item[matched];
        if (matched === "notes") {
          const notesLine = findInNotes(att, keyword);
          if (notesLine) return notesLine;
        } else if (patternMatch(att, keyword)) return att;
      }
      return null;
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}findItem`, err);
  }
};

/**
 * @method patternMatch: A method to match sub in str
 * @param {String}  str string from previous/parent
 * @param {String}  sub sub-string from previous/parent
 */
const patternMatch = (str, sub) => {
  try {
    let result = false;
    if (str && sub) {
      const newStr = str.toLowerCase();
      const newSub = sub.toLowerCase();
      result = newStr.includes(newSub);
    }
    return result;
  } catch (err) {
    console.log(`${errorConsole.try_catch}patternMatch`, err);
  }
};

/**
 * @method filterByKeyword: A method to search keyword in meeting records
 * @param {Object}  meeting meeting detail from previous/parent
 * @param {Object}  filter Selected fields from front-end
 * @param {Boolean}  matchBit matching bit
 */
const filterByKeyword = (meeting, filter, matchBit) => {
  try {
    const { keyword, title, project, notes, task, agenda, email } = filter;
    const { agendas } = meeting;
    let matchedIn = {};

    if (title === matchBit) {
      if (patternMatch(meeting.title, keyword)) {
        matchedIn.Title = meeting.title;
      }
    }

    if (project === matchBit) {
      if (patternMatch(meeting.project_name, keyword)) {
        matchedIn.Project = add3Dots(meeting.project_name, keyword);
      }
    }

    if (notes === matchBit) {
      const notesLine = findInNotes(meeting.notes, keyword);
      if (notesLine) {
        matchedIn["Meeting Comments"] = add3Dots(notesLine, keyword);
      }
      if (meeting.private_notes && meeting.private_notes.length > 0) {
        for (let data of meeting.private_notes) {
          if (data.email === email) {
            const privateNotesLine = findInNotes(data.notes, keyword);
            if (privateNotesLine)
              matchedIn["Private Notes"] = add3Dots(privateNotesLine, keyword);
            break;
          }
        }
      }
      let item = findItem(agendas, "notes", keyword);
      if (item) matchedIn["Agenda Notes"] = add3Dots(item, keyword);
    }

    if (agenda === matchBit) {
      let item = findItem(agendas, "title", keyword);
      if (item) matchedIn["Agenda"] = add3Dots(item, keyword);
    }

    if (task === matchBit) {
      if (agendas && agendas.length > 0) {
        for (let agenda of agendas) {
          let item = findItem(agenda.tasks, "task", keyword);
          if (item) matchedIn.Action = add3Dots(item, keyword);
        }
      }
    }
    return matchedIn;
  } catch (err) {
    console.log(`${errorConsole.try_catch}filterByKeyword`, err);
  }
};

/**
 * @method combineMeetings: A method to filter meetings
 * @param {Array}  orgm List of organiser meetings from previous/parent
 * @param {Array}  ptm List of participant meetings from previous/parent
 * @param {String}  email email of user from frontend
 */
const combineMeetings = (orgm, ptm, email) => {
  try {
    let othm = []; // other meetings
    if (ptm && ptm.length > 0) {
      ptm.forEach(item => {
        if (item.attendees && item.attendees.length > 0) {
          const index = item.attendees.findIndex(prt => {
            if (prt && prt._id && prt._id.email) return prt._id.email === email;
          });
          if (index > -1) othm.push(item);
        }
      });
    }

    // final meetings
    const meetings = _.unionWith(
      orgm,
      othm,
      (a, b) => String(a._id) === String(b._id)
    );
    return meetings;
  } catch (err) {
    console.log(`${errorConsole.try_catch}combineMeetings`, err);
  }
};

/**
 * @method checkAttendeeDetails: A method to filter meetings
 * @param {String}  id ID of meeting participant from previous/parent
 * @param {Array}  arr List of participants of meeting from previous/parent
 */
const checkAttendeeDetails = (id, arr) => {
  try {
    if (arr && arr.length > 0) {
      for (let i of arr) {
        if (i && i._id && i._id._id && i._id._id === id)
          return {
            is_invitation_sent: i.is_invitation_sent
              ? i.is_invitation_sent
              : false,
            is_present: i.is_present ? i.is_present : false
          };
      }
    }
    return {
      is_invitation_sent: false,
      is_present: false
    };
  } catch (err) {
    console.log(`${errorConsole.try_catch}checkAttendeeDetails`, err);
  }
};

/**
 * Object to create pdf file of meeting summary
 */
const pdfOptions = {
  format: "A4",
  height: "10.5in", // allowed units: mm, cm, in, px
  width: "8in",
  paginationOffset: 1,
  footer: {
    height: "28mm",
    contents: {
      default: `<table style="width: 100%; text-align: right; margin-top: 20px;">
       <tr><td style="vertical-align: middle;">
       <table style="display: inline-table;"><tr><td><img src="file://${require.resolve(
        "../../public/images/fruitionPro.png"
      )}" style="width: 75px;">
       </td><td><span style="margin-left: 20px; font-weight: 500;"> {{page}} of {{pages}}</span></td></tr>
       </table></td></tr></table>`
    }
  }
};

/**
 * @method inviteAgendaTask: A method to send task invitations to meeting participants
 * @param {String}  id ID of meeting participant from previous/parent
 * @param {Array}  attendees List of participants of meeting from previous/parent
 */
const inviteAgendaTask = async param => {
  try {
    const { meeting_details } = param;
    const agendas = meeting_details.agendas
      ? JSON.parse(JSON.stringify(meeting_details.agendas))
      : [];
    let taskDetails = [];
    await agendas.forEach(async agendaItem => {
      await agendaItem.tasks.forEach(async element => {
        if (element.assign_to.length > 0) {
          await element.assign_to.forEach(item => {
            let index = taskDetails.findIndex(ele => item === ele.assignee);
            if (index < 0) {
              taskDetails.push({
                assignee: item,
                tasks: [element]
              });
            } else taskDetails[index].tasks.push(element);
          });
        }
      });
    });
    if (taskDetails.length > 0) {
      for (let data of taskDetails) {
        const timeZone = await crobJob.getTimeZone({
          email: data.assignee,
          admin_email: meeting_details.email
        });

        let mailContent = await htmlFormat.htmlBodyAgenda(
          param,
          data.tasks,
          timeZone,
          null
        );

        let taskStr = `${taskController.titleCase(
          meeting_details.company
            ? meeting_details.company
            : meeting_details.name,
          " "
        )} has assigned you `;
        if (data.tasks.length === 1) {
          taskStr = taskStr.concat(
            `a <strong>${htmlFormat.upperCaseFirst(
              data.tasks[0].task
            )}</strong> action.`
          );
        } else {
          taskStr = taskStr.concat("multiple actions.");
          for (let m = 1; m <= data.tasks.length; m++) {
            taskStr = taskStr.concat(
              "<br>" +
              m +
              ". " +
              htmlFormat.upperCaseFirst(data.tasks[m - 1].task)
            );
          }
        }

        notificationController.create({
          receiver: data.assignee,
          sender: meeting_details.email,
          message: taskStr,
          type: "task",
          task: data.tasks.length > 0 ? data.tasks[0]._id : ""
        });
        mailer.sendMail(
          meeting_details.email,
          data.assignee,
          mailContent,
          "Meeting Task",
          null
        );
      }
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}inviteAgendaTask`, err);
  }
};

/**
 * @method isMailSent: A method to check meeting participant invited or not
 * @param {String}  id ID of meeting participant from previous/parent
 * @param {Object}  meeting meeting detail from previous/parent
 */
const isMailSent = (id, meeting) => {
  try {
    if (meeting && meeting.attendees.length > 0) {
      for (let el of meeting.attendees) {
        if (
          el &&
          el._id &&
          el._id._id &&
          meeting.admin_email !== el._id.email &&
          String(el._id._id) === String(id) &&
          el.is_invitation_sent === true
        ) {
          return false;
        }
      }
    }
    return true;
  } catch (err) {
    console.log(`${errorConsole.try_catch}isMailSent`, err);
    return true;
  }
};

/**
 * @method invite: A method to send invitations to meeting participants
 * @param {Object}  param meeting detail from previous/parent
 */
const invite = async param => {
  try {
    let mailContent;
    const { meeting_details, participants } = param;
    const inviteLink = `${url.frontendUrl}/register`;
    const icsContent = icsFile.createICSFile(param);
    organizerInvitation(param, icsContent, inviteLink);
    if (participants && participants.length > 0) {
      for (const data of participants) {
        let isSent = isMailSent(data._id, meeting_details.oldMeeting);
        if (isSent === true) {
          notificationController.create({
            receiver: data.email,
            sender: meeting_details.email,
            message: `${taskController.titleCase(
              meeting_details.company
                ? meeting_details.company
                : meeting_details.name,
              " "
            )} sent you a <strong>${htmlFormat.upperCaseFirst(
              meeting_details.title
            )}</strong> meeting invitation.`,
            type: "meeting",
            meeting: meeting_details.meeting_id
          });
          const token = await crypto.randomBytes(16).toString("hex");
          const attendanceLink = `${url.backendUrl}/api/meeting/attendance?mid=${meeting_details.meeting_id}&ai=${data._id}&st=`;
          const user = await crobJob.getTimeZone({
            email: data.email,
            admin_email: meeting_details.email
          });

          mailContent = await htmlFormat.htmlBody(param, attendanceLink, user);

          mailer.sendMail(
            meeting_details.email,
            data.email,
            mailContent.concat(
              `<div><a href="${inviteLink}?token=${token}&id=${data._id}&type=meeting" style="font-size: 15px;
              padding: 6px 11px;
              color: #fff;
              background-color: #4da8e4;
              text-decoration: none;
              border-radius: 5px;
              margin: 0 0 10px 122px;" >Join Meeting</a></div>`
            ),
            "Meeting Invitation",
            icsContent
          );
          await Attendee.updateOne(
            { _id: ObjectID(data._id) },
            { $set: { token } }
          );
          await Meeting.updateOne(
            {
              _id: ObjectID(meeting_details.meeting_id),
              attendees: { $elemMatch: { _id: ObjectID(data._id) } }
            },
            { $set: { "attendees.$.is_invitation_sent": true } }
          );
        }
      }
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}invite-meeting`, err);
  }
};

/**
 * @method create: A method to create meeting
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const create = async (req, res) => {
  try {
    const {
      title,
      project_name,
      topic,
      department,
      startDateTime,
      endDateTime,
      location,
      recurrenceData,
      email,
      agendas,
      share,
      participants,
      company,
      owner,
      documents,
      notes,
      private_notes,
      logo,
      createFirstMeeting
    } = req.body;
    let recurring = "off";
    req.checkBody("email", "Email is empty").notEmpty();
    req.checkBody("email", "Email is invalid").isEmail();
    req.checkBody("title", "Meeting title is empty").notEmpty();
    req.checkBody("topic", "Meeting topic is empty").notEmpty();
    req.checkBody("startDateTime", "Start date and time is empty").notEmpty();
    req.checkBody("endDateTime", "End date and time is empty").notEmpty();
    req.checkBody("location", "Meeting location is empty").notEmpty();

    if (share && participants && participants.length > 0) {
      req.checkBody("owner", "Login user name is empty").notEmpty();
      req
        .checkBody("share", "Meeting share is empty(true or false)")
        .notEmpty();
    }

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    const attendees = [];
    const promise = [];
    if (participants && participants.length > 0) {
      for (let index = 0; index < participants.length; index++) {
        let item = participants[index];
        let newAttendee;
        const user = await User.findOne(
          { email: item.email },
          { logo: 1, _id: 0 }
        );
        if (!item._id) {
          promise.push(
            Attendee.findOne({
              admin_email: email,
              is_deleted: false,
              email: item.email
            }).then(async result => {
              if (!result) {
                newAttendee = await new Attendee({
                  email: item.email,
                  admin_email: email,
                  logo: user && user.logo ? user.logo : ""
                }).save();
                attendees.push({
                  _id: newAttendee._id,
                  can_edit:
                    item.canEdit && item.canEdit === true ? true : false,
                  can_attend: item.can_attend ? item.can_attend : ""
                });
                participants.splice(index, 1, newAttendee);
              } else {
                if (user && user.logo) {
                  await Attendee.updateOne(
                    { _id: ObjectID(result._id) },
                    { set: { logo: user.logo } }
                  );
                }
                attendees.push({
                  _id: result._id,
                  can_edit:
                    item.canEdit && item.canEdit === true ? true : false,
                  can_attend: item.can_attend ? item.can_attend : ""
                });
                participants.splice(index, 1, { ...item, _id: result._id });
              }
            })
          );
        } else {
          if (user && user.logo) {
            await Attendee.updateOne(
              { _id: ObjectID(item._id) },
              { set: { logo: user.logo } }
            );
          }
          attendees.push({
            _id: item._id,
            can_edit: item.canEdit && item.canEdit === true ? true : false,
            can_attend: item.can_attend ? item.can_attend : ""
          });
        }
      }
    }

    Promise.all(promise).then(async () => {
      const obj = {
        title,
        project_name,
        topic,
        department,
        start_date_time: startDateTime,
        end_date_time: endDateTime,
        address: location,
        admin_email: email,
        attendees,
        documents,
        notes,
        private_notes
      };
      let meetingId = "";

      if (createFirstMeeting) {
        const agendasIds = await createAgendas(agendas);
        await new Meeting({ ...obj, agendas: agendasIds })
          .save()
          .then(result => {
            if (result) {
              meetingId = ObjectID(result._id);
            }
          });
      }

      if (recurrenceData && recurrenceData.length > 0) {
        recurring = "on";
        for (let i = 0; i < recurrenceData.length; i++) {
          const agendasIds = await createAgendas(agendas);
          const meetingObj = {
            ...obj,
            start_date_time: recurrenceData[i].start_date,
            end_date_time: recurrenceData[i].end_date,
            agendas: agendasIds
          };
          if (i === 0 && !meetingId) {
            await new Meeting(meetingObj).save().then(async result => {
              if (result) {
                meetingId = ObjectID(result._id);
                await Meeting.updateOne({ _id: meetingId }, { $set: { parent_id: meetingId } });
              }
            });
          } else {
            await new Meeting({ ...meetingObj, parent_id: meetingId }).save();
          }
        }
      }
      // if (recurrenceData && Object.keys(recurrenceData).length > 0) {
      //   const compareDate = new Date(startDateTime);
      //   compareDate.setHours(0, 0, 0, 0);

      //   if (
      //     compareDate.getUTCDate() ===
      //     new Date(recurrenceData.Range.reccurenceStartDate).getUTCDate()
      //   ) {
      //     createFirstMeeting = false;
      //   }
      //   const date1 = new Date(startDateTime);
      //   const date2 = new Date(endDateTime);
      //   const diffDays = Math.abs(date2.getDate() - date1.getDate());
      //   meetingDates = await meetingRecurrence({
      //     meetingData: { ...obj, diffDays, createdAgendas, participants },
      //     recurrenceData
      //   });

      //   recurring = "on";
      // }

      const meeting = await Meeting.findOne(
        {
          _id: meetingId
        },
        { agendas: 1, _id: 0 }
      ).populate("agendas");

      if (share) {
        const invitationParams = {
          participants,
          meeting_details: {
            name: owner,
            email,
            title,
            company,
            location,
            startDateTime,
            endDateTime,
            agendas: meeting.agendas,
            recurring,
            logo,
            meeting_id: meetingId
          }
        };
        invite(invitationParams);
      }
      return res.json(responses.createMeetingSuccess);
    });
  } catch (err) {
    console.log(`${errorConsole.try_catch}create-meeting`, err);
    return res.json(responses.wrongDetailError);
  }
};

/**
 * @method deleteMeeting: A method to soft delete meeting
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const deleteMeeting = async (req, res) => {
  try {
    const { id, isDeleteAll } = req.body;

    req.checkBody("id", "Meeting id is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    const meeting = await Meeting.findOne({ _id: ObjectID(id) }).populate(
      "attendees._id"
    );
    if (!meeting) return res.json(responses.meetingInvalid);

    if (meeting.attendees.length > 0) {
      const user = await User.findOne({ email: meeting.admin_email });

      const message = `Your ${
        meeting.title
          ? `<strong>${htmlFormat.upperCaseFirst(meeting.title)}</strong>`
          : ""
        } meeting has been cancelled.`;

      const htmlBody = await htmlFormat.htmlBodyMailResponse(
        user.logo,
        user.name,
        user.email,
        "Meeting Information",
        message
      );

      const participants = meeting.attendees.map(i => {
        if (i._id && i._id.email) return i._id.email;
      });
      let index = participants.indexOf(user.email);
      if (index > -1) participants.splice(index, 1);

      mailer.sendMail(
        user.email,
        participants,
        htmlBody,
        "Meeting Cancellation",
        null
      );

      for (let receiver of participants) {
        notificationController.create({
          receiver,
          sender: user.email,
          message,
          meeting: meeting._id
        });
      }
    }
    let idList = [];
    if (isDeleteAll === true && meeting.parent_id) {
      let time = new Date().toUTCString();
      const recMeetings = await Meeting.find(
        {
          $and: [
            { parent_id: ObjectID(meeting.parent_id) },
            { end_date_time: { $gte: time } },
            { is_deleted: false }
          ]
        }
      );
      const idArr = [];
      recMeetings.forEach(ele => {
        idArr.push(ObjectID(ele._id));
        idList.push(ele._id);
      });
      if (idArr && idArr.length > 0) {
        await Meeting.updateMany(
          { _id: { $in: idArr } },
          { $set: { is_deleted: true } }
        );
      }
    } else {
      idList = [id];
      await Meeting.updateOne(
        { _id: ObjectID(id) },
        { $set: { is_deleted: true } }
      );
    }
    return res.json({ ...responses.deleteMeetingSuccess, id: idList });
  } catch (err) {
    console.log(`${errorConsole.try_catch}delete-meeting`, err);
    return res.json(responses.somethingWrong);
  }
};

/**
 * @method import_meetings: A method to import meetings/events from specific calendar
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const import_meetings = async (req, res) => {
  try {
    const { accessToken, type, email } = req.body;
    req.checkBody("accessToken", "Token is empty").notEmpty();
    req.checkBody("type", "Type is empty(microsoft or google)").notEmpty();
    req.checkBody("email", "Email id is empty").notEmpty();
    req.checkBody("email", "Email id is invalid").isEmail();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    const meetingParams = {
      accessToken,
      email
    };
    if (type === "microsoft") {
      await microsoftMethods.storeMeetings(meetingParams);
    } else if (type === "google") {
      await googleMethods.storeMeetings(meetingParams);
    }
    const time = new Date().toUTCString();
    const meetings = await Meeting.find({
      admin_email: email,
      end_date_time: { $gte: time },
      is_deleted: false
    })
      .populate("attendees._id")
      .populate({
        path: "agendas",
        populate: { path: "tasks", match: { is_deleted: false } }
      })
      .sort({ start_date_time: 1 });

    if (meetings) {
      return res.json({ ...responses.importSuccuss, data: meetings });
    } else return res.json({ ...responses.noMeetings, data: [] });
  } catch (err) {
    console.log(`${errorConsole.try_catch}import-meetings`, err);
    return res.json(responses.importUnsuccuss);
  }
};

/**
 * @method import_contacts: A method to import contacts from specific calendar
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const import_contacts = async (req, res) => {
  try {
    const { accessToken, type, email } = req.body;
    req.checkBody("accessToken", "Token is empty").notEmpty();
    req.checkBody("type", "Type is empty(microsoft or google)").notEmpty();
    req.checkBody("email", "Email id is empty").notEmpty();
    req.checkBody("email", "Email id is invalid").isEmail();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    const attendeeParams = {
      accessToken,
      email
    };
    if (type === "microsoft") {
      await microsoftMethods.storeAttendees(attendeeParams);
    } else if (type === "google") {
      await googleMethods.storeAttendees(attendeeParams);
    }

    const attendees = await Attendee.find({
      admin_email: email,
      is_deleted: false
    });
    if (attendees)
      return res.json({ ...responses.contactsSuccuss, data: attendees });
    else return res.json(responses.noContacts);
  } catch (err) {
    console.log(`${errorConsole.try_catch}import-contacts`, err);
    return res.json(responses.fetchContactsUnsuccuss);
  }
};

/**
 * @method fetch_meetings: A method to fetch meetings of specific user
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const fetch_meetings = async (req, res) => {
  try {
    const { type, email } = req.body;
    req.checkBody("type", "Type id is empty(upcoming or past)").notEmpty();
    req.checkBody("email", "Email id is empty").notEmpty();
    req.checkBody("email", "Email id is invalid").isEmail();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    let time = new Date().toUTCString();

    let pastOrUpcoming;
    let sort;
    if (type === "upcoming") {
      pastOrUpcoming = {
        $gte: time
      };
      sort = { start_date_time: 1 };
    } else if (type === "past") {
      pastOrUpcoming = {
        $lt: time
      };
      sort = { start_date_time: -1 };
    }

    let conditions = [{ end_date_time: pastOrUpcoming }, { is_deleted: false }];

    const attendeeCond = await attendeeCondition(email);
    Promise.all([
      Meeting.find({ $and: [...conditions, { admin_email: email }] })
        .populate("attendees._id")
        .populate({
          path: "agendas",
          populate: { path: "tasks", match: { is_deleted: false } }
        })
        .sort(sort),
      attendeeCond.length > 1 &&
      Meeting.find({
        $and: [
          ...conditions,
          { admin_email: { $ne: email } },
          ...attendeeCond
        ]
      })
        .populate({
          path: "attendees._id"
        })
        .populate({
          path: "agendas",
          populate: { path: "tasks", match: { is_deleted: false } }
        })
        .sort(sort)
    ]).then(result => {
      const orgm = JSON.parse(JSON.stringify(result[0])); // organize meetings
      let mym = JSON.parse(JSON.stringify(result[1])) || []; // my meetings as participant
      // const meetings = combineMeetings(result[0], result[1], email);
      const meetings = [...orgm, ...mym];
      if (meetings.length > 0) {
        return res.json({
          ...responses.fetchSuccess,
          data: meetings
        });
      } else return res.json({ ...responses.noMeetings, data: [] });
    });
  } catch (err) {
    console.log(`${errorConsole.try_catch}fetch-meetings`, err);
    return res.json(responses.catchError);
  }
};

/**
 * @method invite_meeting_token: A method to verify the user for meeting invitation
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const invite_meeting_token = async (req, res) => {
  try {
    const { id, token } = req.body;
    req.checkBody("id", "User id is empty").notEmpty();
    req.checkBody("token", "Token is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    const attendee = await Attendee.findOne({ _id: ObjectID(id) });

    if (!attendee) return res.json(responses.InvalidID);

    if (attendee.token !== token) return res.json(responses.InvalidToken);

    const user = await User.findOne({
      email: attendee.email
    });
    const jwtToken = middleware.getJwtToken(attendee.email);
    if (user) {
      return res.json({
        ...responses.loginSuccess,
        data: user,
        token: jwtToken
      });
    } else
      return res.json({ ...responses.notRegistered, email: attendee.email });
  } catch (err) {
    console.log(
      `${errorConsole.try_catch}after-send-invite-meeting-token`,
      err
    );
    return res.json(responses.loginUnsuccess);
  }
};

/**
 * @method modify: A method to update the meeting
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const modify = async (req, res) => {
  try {
    const {
      _id,
      title,
      startDateTime,
      endDateTime,
      location,
      agendas,
      participants,
      email,
      owner,
      share,
      company,
      private_notes,
      logo,
      isEditAll,
      documents,
      notes,
      project_name,
      topic,
      department
    } = req.body;
    req.checkBody("_id", "Meeting id is empty").notEmpty();
    req.checkBody("email", "Admin email is empty").notEmpty();
    req.checkBody("email", "Admin email is invalid").isEmail();

    if (share && participants && participants.length > 0) {
      req.checkBody("owner", "Login user name is empty").notEmpty();
      req
        .checkBody(
          "share",
          "Share Meeting invitation permission is empty(true or false)"
        )
        .notEmpty();
    }

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    let oldMeeting;
    await Meeting.findOne({
      _id: ObjectID(_id)
    })
      .populate("attendees._id")
      .then(result => {
        oldMeeting = JSON.parse(JSON.stringify(result));
      });

    if (!oldMeeting) return res.json(responses.meetingInvalid);

    let upsertAgendas = [];
    if (agendas && agendas.length > 0) {
      upsertAgendas = await agendaController.upsert(agendas);
    }
    const attendees = [];
    const promise = [];
    for (let index = 0; index < participants.length; index++) {
      let item = participants[index];
      let newAttendee;
      const user = await User.findOne(
        { email: item.email },
        { logo: 1, _id: 0 }
      );
      if (!item._id) {
        promise.push(
          Attendee.findOne({
            admin_email: email,
            is_deleted: false,
            email: item.email
          }).then(async result => {
            if (!result) {
              newAttendee = await new Attendee({
                email: item.email,
                admin_email: email,
                logo: user && user.logo ? user.logo : ""
              }).save();

              attendees.push({
                _id: newAttendee._id,
                can_edit: item.canEdit && item.canEdit === true ? true : false,
                can_attend: item.can_attend ? item.can_attend : ""
              });
              participants.splice(index, 1, newAttendee);
            } else {
              if (user && user.logo) {
                await Attendee.updateOne(
                  { _id: ObjectID(result._id) },
                  { set: { logo: user.logo } }
                );
              }
              const attendeeDetails = checkAttendeeDetails(
                result._id,
                oldMeeting.attendees
              );
              const obj = {
                _id: result._id,
                can_edit: item.canEdit && item.canEdit === true ? true : false,
                can_attend: item.can_attend ? item.can_attend : "",
                is_present: item.is_present || attendeeDetails.is_present,
                is_invitation_sent: attendeeDetails.is_invitation_sent
              };
              attendees.push(obj);
              participants.splice(index, 1, { ...item, _id: result._id });
            }
          })
        );
      } else {
        if (user && user.logo) {
          await Attendee.updateOne(
            { _id: ObjectID(item._id) },
            { set: { logo: user.logo } }
          );
        }
        const attendeeDetails = checkAttendeeDetails(
          item._id,
          oldMeeting.attendees
        );
        attendees.push({
          _id: item._id,
          can_edit: item.canEdit && item.canEdit === true ? true : false,
          can_attend: item.can_attend ? item.can_attend : "",
          is_present: item.is_present || attendeeDetails.is_present,
          is_invitation_sent: attendeeDetails.is_invitation_sent
        });
      }
    }

    Promise.all(promise).then(async () => {
      const meetingData = {
        ...req.body,
        agendas: upsertAgendas,
        attendees,
        start_date_time: startDateTime,
        end_date_time: endDateTime,
        address: location,
        private_notes
      };
      let isModified = false;
      if (isEditAll === true && oldMeeting.parent_id) {
        let obj = {
          attendees,
          address: location,
          private_notes,
          documents,
          notes,
          title,
          project_name,
          topic,
          department
        }
        let time = new Date().toUTCString();
        const recMeetings = await Meeting.find(
          {
            $and: [
              { parent_id: ObjectID(oldMeeting.parent_id) },
              { end_date_time: { $gte: time } },
              { is_deleted: false }
            ]
          }
        );
        for (let meeting of recMeetings) {
          obj = {
            ...obj,
            start_date_time: new Date(`${moment((meeting.start_date_time)).format().slice(0, 11)}${moment(startDateTime).format().slice(11)}`),
            end_date_time: new Date(`${moment(meeting.end_date_time).format().slice(0, 11)}${moment(endDateTime).format().slice(11)}`)
          }
          await Meeting.updateOne(
            { _id: ObjectID(meeting._id) },
            { $set: obj }
          ).then(result => {
            if (result && result.nModified) {
              isModified = true;
            }
          })
        }
      } else {
        await Meeting.updateOne(
          { _id: ObjectID(_id) },
          { $set: meetingData }
        ).then(async result => {
          if (result && result.nModified) {
            isModified = true;
          }
        });
      }

      if (isModified === true && participants && participants.length > 0) {
        const arr = participants.map(i => i.email);
        let index = arr.indexOf(oldMeeting.admin_email);
        if (index > -1) arr.push(oldMeeting.admin_email);
        const admin = await getOrganisation(email);
        for (let i of arr) {
          notificationController.create({
            receiver: i,
            sender: email,
            message: `${taskController.titleCase(
              admin.name,
              " "
            )} has updated <strong>${htmlFormat.upperCaseFirst(
              oldMeeting.title
            )}</strong> meeting.`,
            type: "meeting",
            meeting: _id
          });
        }
      }

      const meeting = await Meeting.findOne({
        _id: ObjectID(_id)
      })
        .populate("attendees._id")
        .populate({
          path: "agendas",
          populate: { path: "tasks", match: { is_deleted: false } }
        });

      const meeting_details = {
        name: owner,
        email,
        title,
        company,
        location,
        startDateTime,
        endDateTime,
        agendas: meeting.agendas,
        logo,
        meeting_id: _id,
        oldMeeting
      };

      const invitationParams = {
        participants,
        meeting_details
      };

      if (share) invite(invitationParams);

      if (meeting && meeting.agendas && meeting.agendas.length > 0) {
        inviteAgendaTask(invitationParams);
      }

      return res.json({ ...responses.updateUserSuccess, data: meeting });
    });
  } catch (err) {
    console.log(`${errorConsole.try_catch}modify-meeting`, err);
    return res.json(responses.wrongDetailError);
  }
};

/**
 * @method meeting_attendance: A method to store meeting attendance by participant
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const meeting_attendance = async (req, res) => {
  res.writeHeader(200, { "Content-Type": "text/html" });
  try {
    const { mid, ai, st } = req.query;
    const arr = ["yes", "no", "maybe"];
    const attendee = await Attendee.findOne({
      _id: ObjectID(ai)
    });

    const meeting = await Meeting.findOne({
      _id: ObjectID(mid)
    });

    let user;
    if (attendee) {
      user = await User.findOne({
        email: attendee.email
      }).then(result => {
        if (result) {
          return JSON.parse(JSON.stringify(result));
        }
      });
    }

    if (!attendee || !meeting || !arr.some(i => i === st)) {
      res.write(htmlFormat.mailAttendance("no", "Invalid Link"));
    } else if (!user) {
      res.write(
        htmlFormat.mailAttendance(
          "yes",
          `You are not Registered.<a href="${url.frontendUrl}/register" style = "color: #ff3300">click here to register</a>`
        )
      );
    } else {
      await Meeting.updateOne(
        {
          _id: ObjectID(mid),
          attendees: { $elemMatch: { _id: ObjectID(ai) } }
        },
        { $set: { "attendees.$.can_attend": st } }
      ).then(async result => {
        if (result.nModified) {
          const message = `${taskController.titleCase(
            user.name,
            " "
          )} said <strong>${htmlFormat.upperCaseFirst(
            st
          )}</strong> for <strong>${htmlFormat.upperCaseFirst(
            meeting.title
          )}</strong> meeting.`;
          const htmlBody = await htmlFormat.htmlBodyMailResponse(
            user.logo,
            user.name,
            user.email,
            "Reply of your meeting participant",
            message
          );

          mailer.sendMail(
            attendee.email,
            meeting.admin_email,
            htmlBody,
            "Meeting Participant Reply",
            null
          );

          notificationController.create({
            receiver: meeting.admin_email,
            sender: attendee.email,
            message: "Your participant " + message,
            type: "meeting",
            meeting: meeting._id
          });

          res.write(
            htmlFormat.mailAttendance(st, "Your response added successfully.")
          );
        } else {
          res.write(
            htmlFormat.mailAttendance(st, "Your response already added.")
          );
        }
      });
    }
  } catch (err) {
    res.write(htmlFormat.mailAttendance("no", "Invalid Link"));
  }
  res.end();
};

/**
 * @method createPdf: A method to create meeting summary/detail pdf file
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const createPdf = (req, res) => {
  try {
    const { html, file_name } = req.body;
    req.checkBody("html", "Meeting html file is empty").notEmpty();
    req.checkBody("file_name", "Meeting file name is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    htmlPdf
      .create(
        `${html}<img alt="Fruition Pro" src="file://${require.resolve(
          "../../public/images/fruitionPro.png"
        )}" style="width:0px;">`,
        pdfOptions
      )
      .toStream((err, result) => {
        res.setHeader("Content-Type", "application/pdf");
        res.header(
          "Content-Disposition",
          `attachment; filename="${file_name}.pdf"`
        );
        result.pipe(res);
      });
  } catch (err) {
    console.log(`${errorConsole.try_catch}createpdf`, err);
    return res.json(responses.wrongDetailError);
  }
};

/**
 * @method shareSummary: A method for send meeting summary to specific users
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const shareSummary = async (req, res) => {
  try {
    const { emails, attachment, sender_email, meeting_id } = req.body;
    req.checkBody("meeting_id", "Meeting id is empty").notEmpty();
    req
      .checkBody("emails", "Receiver emails array is empty")
      .isArray()
      .notEmpty();
    req.checkBody("attachment", "Meeting summary file is missing").notEmpty();
    req.checkBody("sender_email", "Sender email id is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    const meeting = await Meeting.findOne({ _id: ObjectID(meeting_id) });

    const file_name = meeting.title.replace(/[^A-Z0-9]+/gi, "_");

    const htmlBody = htmlFormat.meetingSummary(file_name);
    htmlPdf
      .create(
        `${attachment}<img alt="Fruition Pro" src="file://${require.resolve(
          "../../public/images/fruitionPro.png"
        )}" style="width:0px;">`,
        pdfOptions
      )
      .toBuffer((err, result) => {
        const attachmentData = [
          {
            content: result.toString("base64"),
            filename: `${file_name}.pdf`,
            type: "application/pdf",
            disposition: "attachment"
          }
        ];
        mailer
          .sendMail(
            sender_email,
            emails,
            htmlBody,
            "Meeting summary",
            attachmentData
          )
          .then(async result => {
            // Get organisation by creator email id
            const senderDetail = await getOrganisation(sender_email);
            for (let email of emails) {
              notificationController.create({
                receiver: email,
                sender: sender_email,
                message: `${taskController.titleCase(
                  senderDetail.name,
                  " "
                )} sent you <strong>${htmlFormat.upperCaseFirst(
                  meeting.title
                )}</strong> meeting summary.`,
                type: "meeting",
                meeting: meeting._id
              });
            }
            if (result) {
              res.status(200).json({ message: "Share Meeting Sucessfully" });
            }
          });
      });
  } catch (err) {
    console.log(`${errorConsole.try_catch}shareSummary`, err);
    return res.json(responses.wrongDetailError);
  }
};

/**
 * @method reopen_close_meeting: A method to close/open the meeting
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const reopen_close_meeting = (req, res) => {
  try {
    const { id, meetingStatus } = req.body;
    req.checkBody("id", "Meeting id is empty").notEmpty();
    req.checkBody("meetingStatus", "Meeting status is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    Meeting.updateOne(
      { _id: ObjectID(id) },
      { $set: { is_closed: meetingStatus === "close" ? true : false } }
    )
      .then(() => {
        if (meetingStatus === "close") {
          res.json({ meetingStatus, ...responses.closeMeeting });
        } else res.json({ meetingStatus, ...responses.reOpenMeeting });
      })
      .catch(err => {
        console.log(`${errorConsole.api_catch}reopen_close_meeting`, err);
      });
  } catch (err) {
    console.log(`${errorConsole.try_catch}reopen_close_meeting`, err);
    return res.json(responses.wrongDetailError);
  }
};

/**
 * @method updateMeetingStatus: A method to update the meeting status
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const updateMeetingStatus = async (req, res) => {
  try {
    const { id, status } = req.body;
    req.checkBody("id", "Meeting id is empty").notEmpty();
    req.checkBody("status", "Meeting status is empty").notEmpty();
    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    const meeting = await Meeting.findOne(
      { _id: ObjectID(id) },
      { admin_email: 1, attendees: 1, title: 1 }
    );

    if (!meeting) res.json(responses.wrongDetailError);

    Meeting.updateOne({ _id: ObjectID(id) }, { $set: { status: status } })
      .then(async result => {
        if (result && result.nModified && meeting.attendees.length > 0) {
          // Get organisation by creator email id
          const senderDetail = await getOrganisation(meeting.admin_email);
          for (let i of meeting.attendees) {
            let attendee = await Attendee.findOne(
              { _id: ObjectID(i._id) },
              { email: 1, _id: 0 }
            );
            notificationController.create({
              receiver: attendee.email,
              sender: meeting.admin_email,
              message: `${taskController.titleCase(
                senderDetail.name,
                " "
              )} is ready to start <strong>${htmlFormat.upperCaseFirst(
                meeting.title
              )}</strong> meeting.`,
              type: "meeting",
              meeting: meeting._id
            });
          }
        }
        return res.json({ meetingStatus: status, ...responses.startMeeting });
      })
      .catch(err => {
        console.log(`${errorConsole.api_catch}updateMeetingStatus`, err);
      });
  } catch (err) {
    console.log(`${errorConsole.try_catch}updateMeetingStatus`, err);
    return res.json(responses.wrongDetailError);
  }
};

/**
 * @method search: A method to fetch meetings contain particular text of specific user
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const search = async (req, res) => {
  try {
    let { email, keyword, title, project, notes, task, agenda } = req.body;
    req.checkBody("keyword", "Search word is empty").notEmpty();
    req.checkBody("email", "Email id is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    keyword = keyword.trim();

    if (
      title === false &&
      project === false &&
      notes === false &&
      agenda === false &&
      task === false
    ) {
      title = project = notes = agenda = task = true;
    }

    const attendeeCond = await attendeeCondition(email);

    Promise.all([
      Meeting.find({
        $and: [{ admin_email: email }, { is_deleted: false }]
      })
        .populate("attendees._id")
        .populate({
          path: "agendas",
          populate: { path: "tasks", match: { is_deleted: false } }
        })
        .sort({ start_date_time: 1 }),
      Meeting.find({
        $and: [
          { is_deleted: false },
          { admin_email: { $ne: email } },
          ...attendeeCond
        ]
      })
        .populate({
          path: "attendees._id"
        })
        .populate({
          path: "agendas",
          populate: { path: "tasks", match: { is_deleted: false } }
        })
        .sort({ start_date_time: 1 })
    ]).then(res1 => {
      const orgm = JSON.parse(JSON.stringify(res1[0])); // organize meetings
      let mym = JSON.parse(JSON.stringify(res1[1])); // my meetings as participant
      // const meetings = combineMeetings(orgm, mym, email);
      const meetings = [...orgm, ...mym];
      const result = [];
      // const otherMeetings = [];
      if (meetings.length > 0) {
        const obj = { title, project, notes, agenda, task, keyword, email };
        for (let meeting of meetings) {
          let matchedIn = filterByKeyword(meeting, obj, true);
          if (Object.keys(matchedIn).length > 0) {
            result.push({
              ...meeting,
              matchedIn: _.omit(matchedIn, ["Title"])
            });
          }
          //  else {
          //   let otherMatchedIn = filterByKeyword(meeting, obj, false);
          //   if (Object.keys(otherMatchedIn).length > 0)
          //     otherMeetings.push({
          //       ...meeting,
          //       otherMatchedIn: _.omit(otherMatchedIn, ["Title"])
          //     });
          // }
        }
        return res.json({
          ...responses.fetchSuccess,
          data: {
            search: result
            // Other: otherMeetings
          }
        });
      } else {
        return res.json({
          ...responses.noMeetings,
          data: {
            search: result
            // Other: otherMeetings
          }
        });
      }
    });
  } catch (err) {
    console.log(`${errorConsole.try_catch}search`, err);
    return res.json(responses.catchError);
  }
};

module.exports.patternMatch = patternMatch;
module.exports.create = create;
module.exports.modify = modify;
module.exports.import_meetings = import_meetings;
module.exports.import_contacts = import_contacts;
module.exports.fetch_meetings = fetch_meetings;
module.exports.deleteMeeting = deleteMeeting;
module.exports.invite_meeting_token = invite_meeting_token;
module.exports.meeting_attendance = meeting_attendance;
module.exports.createPdf = createPdf;
module.exports.shareSummary = shareSummary;
module.exports.reopen_close_meeting = reopen_close_meeting;
module.exports.updateMeetingStatus = updateMeetingStatus;
module.exports.search = search;
module.exports.getOrganisation = getOrganisation;
