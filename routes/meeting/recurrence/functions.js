"use strict";
const moment = require("moment");
const dateFormat = require("../../../common/format");
const errorConsole = require("../../../common/catch-consoles");

/**
 * @method getWeekDay: A method to capitalize the string
 * @param {Object}  dateString date from previous/parent
 * @param {BigInt}  dayOfWeek email id of user from previous/parent
 */
const getWeekDay = (dateString, dayOfWeek) => {
  try {
    let date = moment(dateString, dateFormat.MEETING_DATE_FORMAT);

    let day = date.day();
    let diffDays = 0;

    if (day > dayOfWeek) {
      diffDays = 7 - (day - dayOfWeek);
    } else {
      diffDays = dayOfWeek - day;
    }
    return date.add(diffDays, "day").format(dateFormat.MEETING_DATE_FORMAT);
  } catch (err) {
    console.log(`${errorConsole.try_catch}getWeekDay`, err);
    return null;
  }
};

/**
 * @method recurrenceMeetingObj: A method to create meeting detail obj with recurrence dates
 * @param {Object}  date date from previous/parent
 * @param {Object}  meetingData meeting data from frontend
 */
const recurrenceMeetingObj = (date, meetingData) => {
  try {
    const endDateTime = new Date(date);

    const meetingStartTime = new Date(meetingData.start_date_time);
    date.setHours(
      meetingStartTime.getHours(),
      meetingStartTime.getMinutes(),
      meetingStartTime.getSeconds(),
      meetingStartTime.getMilliseconds()
    );

    const meetingEndTime = new Date(meetingData.end_date_time);
    endDateTime.setHours(
      meetingEndTime.getHours(),
      meetingEndTime.getMinutes(),
      meetingEndTime.getSeconds(),
      meetingEndTime.getMilliseconds()
    );

    endDateTime.setDate(date.getDate() + meetingData.diffDays);

    const obj = {
      ...meetingData,
      start_date_time: date,
      end_date_time: endDateTime
    };
    return obj;
  } catch (err) {
    console.log(`${errorConsole.try_catch}recurrenceMeetingObj`, err);
    return null;
  }
};

module.exports = { getWeekDay, recurrenceMeetingObj };
