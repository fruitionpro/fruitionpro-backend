"use strict";
const moment = require("moment");
const Meeting = require("../meeting.model");
const recurrenceMeetingObj = require("../recurrence/functions")
  .recurrenceMeetingObj;
const errorConsole = require("../../../common/catch-consoles");

/**
 * @method weeklyMeetingRecurrence: A method to create monthly meetings
 * @param {Object}  meetingData meeting detail from frontend
 * @param {Object}  startDate start date from previous/parent
 * @param {Object}  endDate end date from previous/parent
 * @param {BigInt}  gapBetweenDates no. of days between start and end date of meeting from previous/parent
 * @param {BigInt}  weekDays list of week-days from frontend
 * @param {BigInt}  occurences total no. of meetings to be created, from frontend
 */
const weeklyMeetingRecurrence = async (
  meetingData,
  startDate,
  endDate,
  gapBetweenDates,
  weekDays,
  occurences
) => {
  try {
    let meetingDates = []; // meetings array
    let obj;
    const { createdAgendas, participants } = meetingData;

    if (occurences > 0) {
      let dayOfWeek = moment(startDate).day() + 1;
      let weekStartDate = new Date(startDate);
      weekStartDate.setDate(weekStartDate.getDate() - dayOfWeek);

      for (
        let i = weekStartDate;
        meetingDates.length < occurences;
        i.setDate(i.getDate() + gapBetweenDates)
      ) {
        let checkDate = new Date(i);

        for (
          let t = dayOfWeek;
          t <= 7;
          t++, checkDate.setDate(checkDate.getDate() + 1)
        ) {
          if (
            weekDays.indexOf(moment(checkDate).format("dddd")) > -1 &&
            checkDate >= startDate
          ) {
            obj = recurrenceMeetingObj(new Date(checkDate), meetingData);
            let meeting = await new Meeting(obj).save().catch(err => {
              console.log(
                `${errorConsole.api_catch}weeklyMeetingRecurrence-occurences`,
                err
              );
            });

            let meetingParsedData = JSON.parse(JSON.stringify(meeting));

            if (meeting && meetingDates.length < occurences) {
              meetingDates.push({
                ...meetingParsedData,
                agendas: createdAgendas,
                attendees: participants
              });
            }
          }
          dayOfWeek = 1;
        }
      }
    }
    // for end date
    else {
      let dayOfWeek = moment(startDate).day() + 1;
      let weekStartDate = new Date(startDate);
      weekStartDate.setDate(weekStartDate.getDate() - dayOfWeek);

      for (
        let i = weekStartDate;
        i <= endDate;
        i.setDate(i.getDate() + gapBetweenDates)
      ) {
        let checkDate = new Date(i);

        for (
          let t = dayOfWeek;
          t <= 7;
          t++, checkDate.setDate(checkDate.getDate() + 1)
        ) {
          if (
            weekDays.indexOf(moment(checkDate).format("dddd")) > -1 &&
            checkDate <= endDate &&
            checkDate >= startDate
          ) {
            obj = recurrenceMeetingObj(new Date(checkDate), meetingData);
            let meeting = await new Meeting(obj).save().catch(err => {
              console.log(
                `${errorConsole.api_catch}weeklyMeetingRecurrence-end-date`,
                err
              );
            });

            let meetingParsedData = JSON.parse(JSON.stringify(meeting));

            if (meeting) {
              meetingDates.push({
                ...meetingParsedData,
                agendas: createdAgendas,
                attendees: participants
              });
            }
          }
          dayOfWeek = 1;
        }
      }
    }

    return meetingDates;
  } catch (err) {
    console.log(`${errorConsole.try_catch}weeklyMeetingRecurrence`, err);
    return res.json(responses.wrongDetailError);
  }
};

module.exports = weeklyMeetingRecurrence;
