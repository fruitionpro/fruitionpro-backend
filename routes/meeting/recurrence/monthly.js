"use strict";
const getWeekDay = require("./functions").getWeekDay;
const Meeting = require("../meeting.model");
const recurrenceMeetingObj = require("../recurrence/functions")
  .recurrenceMeetingObj;
const errorConsole = require("../../../common/catch-consoles");

/**
 * @method monthlyMeetingDaysRecurrence: A method to create monthly meetings
 * @param {Object}  meetingData meeting detail from frontend
 * @param {Object}  startDate start date from previous/parent
 * @param {Object}  endDate end date from previous/parent
 * @param {BigInt}  gapBetweenMonths no. of months between start and end date of meeting from previous/parent
 * @param {BigInt}  dayOfMonth day of month from frontend
 * @param {BigInt}  occurences total no. of meetings to be created, from frontend
 */
const monthlyMeetingDaysRecurrence = async (
  meetingData,
  startDate,
  endDate,
  gapBetweenMonths,
  dayOfMonth,
  occurences
) => {
  try {
    let meetingDates = []; // meetings array
    const date = new Date(startDate);
    let obj;
    const { createdAgendas, participants } = meetingData;

    if (occurences > 0) {
      for (
        let i = startDate, temp = 1;
        temp <= occurences;
        i.setMonth(i.getMonth() + gapBetweenMonths)
      ) {
        i.setDate(dayOfMonth);
        if (date <= i) {
          obj = recurrenceMeetingObj(new Date(i), meetingData);
          let meeting = await new Meeting(obj).save().catch(err => {
            console.log(
              `${errorConsole.api_catch}monthlyMeetingDaysRecurrence-occurences`,
              err
            );
          });

          let meetingParsedData = JSON.parse(JSON.stringify(meeting));

          if (meeting) {
            meetingDates.push({
              ...meetingParsedData,
              agendas: createdAgendas,
              attendees: participants
            });
          }
          temp++;
        }
      }
    }
    // for end date
    else {
      for (
        let i = startDate;
        i <= endDate;
        i.setMonth(i.getMonth() + gapBetweenMonths)
      ) {
        i.setDate(dayOfMonth);
        if (date <= i) {
          obj = recurrenceMeetingObj(new Date(i), meetingData);
          let meeting = await new Meeting(obj).save().catch(err => {
            console.log(
              `${errorConsole.api_catch}monthlyMeetingDaysRecurrence-occurences`,
              err
            );
          });

          let meetingParsedData = JSON.parse(JSON.stringify(meeting));

          if (meeting) {
            meetingDates.push({
              ...meetingParsedData,
              agendas: createdAgendas,
              attendees: participants
            });
          }
        }
      }
    }

    return meetingDates;
  } catch (err) {
    console.log(`${errorConsole.try_catch}monthlyMeetingDaysRecurrence`, err);
    return res.json(responses.wrongDetailError);
  }
};

/**
 * @method monthlyMeetingWeekDaysRecurrence: A method to create monthly on particular week days meetings
 * @param {Object}  meetingData meeting detail from frontend
 * @param {Object}  startDate start date from previous/parent
 * @param {Object}  endDate end date from previous/parent
 * @param {BigInt}  gapBetweenMonths no. of months between start and end date of meeting from previous/parent
 * @param {BigInt}  day day of week from frontend
 * @param {BigInt}  week week of month from frontend
 * @param {BigInt}  occurences total no. of meetings to be created, from frontend
 */
const monthlyMeetingWeekDaysRecurrence = async (
  meetingData,
  startDate,
  endDate,
  gapBetweenMonths,
  day,
  week,
  occurences
) => {
  try {
    let meetingDates = [];
    let obj;
    const { createdAgendas, participants } = meetingData;

    const getStartDate = startDate.getDate();
    startDate.setDate(startDate.getDate() - getStartDate + 1);

    if (occurences > 0) {
      for (
        let i = startDate, temp = 1;
        temp <= occurences;
        i.setMonth(i.getMonth() + gapBetweenMonths), temp++
      ) {
        let t = new Date(getWeekDay(i, day));
        obj = recurrenceMeetingObj(
          new Date(t.setDate(t.getDate() + week * 7)),
          meetingData
        );
        let meeting = await new Meeting(obj).save().catch(err => {
          console.log(
            `${errorConsole.api_catch}monthlyMeetingWeekDaysRecurrence-occurences`,
            err
          );
        });

        let meetingParsedData = JSON.parse(JSON.stringify(meeting));

        if (meeting) {
          meetingDates.push({
            ...meetingParsedData,
            agendas: createdAgendas,
            attendees: participants
          });
        }
      }
    }
    // for end date
    else {
      for (
        let i = startDate;
        i <= endDate;
        i.setMonth(i.getMonth() + gapBetweenMonths)
      ) {
        if (i >= startDate) {
          let t = new Date(getWeekDay(i, day));
          let meetingDate = new Date(t.setDate(t.getDate() + week * 7));
          if (meetingDate <= endDate) {
            obj = recurrenceMeetingObj(new Date(meetingDate), meetingData);
            let meeting = await new Meeting(obj).save().catch(err => {
              console.log(
                `${errorConsole.api_catch}monthlyMeetingWeekDaysRecurrence-end-date`,
                err
              );
            });

            let meetingParsedData = JSON.parse(JSON.stringify(meeting));

            if (meeting) {
              meetingDates.push({
                ...meetingParsedData,
                agendas: createdAgendas,
                attendees: participants
              });
            }
          }
        }
      }
    }

    return meetingDates;
  } catch (err) {
    console.log(
      `${errorConsole.try_catch}monthlyMeetingWeekDaysRecurrence`,
      err
    );
    return res.json(responses.wrongDetailError);
  }
};

module.exports.monthlyMeetingDaysRecurrence = monthlyMeetingDaysRecurrence;
module.exports.monthlyMeetingWeekDaysRecurrence = monthlyMeetingWeekDaysRecurrence;
