"use strict";
const Meeting = require("../meeting.model");
const recurrenceMeetingObj = require("../recurrence/functions")
  .recurrenceMeetingObj;
const errorConsole = require("../../../common/catch-consoles");

/**
 * @method dailyEveryMeetingRecurrence: A method to create regular meetings
 * @param {Object}  meetingData meeting detail from frontend
 * @param {Object}  startDate start date from previous/parent
 * @param {Object}  endDate end date from previous/parent
 * @param {BigInt}  gapBetweenDates no. of days between start and end date of meeting from previous/parent
 * @param {BigInt}  occurences total no. of meetings to be created, from frontend
 */
const dailyEveryMeetingRecurrence = async (
  meetingData,
  startDate,
  endDate,
  gapBetweenDates,
  occurences
) => {
  try {
    let meetingDates = []; // meetings array
    let obj;
    const { createdAgendas, participants } = meetingData;

    if (occurences > 0) {
      for (
        let i = startDate, temp = 1;
        temp <= occurences;
        i.setDate(i.getDate() + gapBetweenDates), temp++
      ) {
        obj = recurrenceMeetingObj(new Date(i), meetingData);
        let meeting = await new Meeting(obj).save().catch(err => {
          console.log(
            `${errorConsole.api_catch}dailyEveryMeetingRecurrence-occurences`,
            err
          );
        });

        let meetingParsedData = JSON.parse(JSON.stringify(meeting));

        if (meeting) {
          meetingDates.push({
            ...meetingParsedData,
            agendas: createdAgendas,
            attendees: participants
          });
        }
      }
    } // for end date
    else {
      for (
        let i = startDate;
        i <= endDate;
        i.setDate(i.getDate() + gapBetweenDates)
      ) {
        obj = recurrenceMeetingObj(new Date(i), meetingData);
        let meeting = await new Meeting(obj).save().catch(err => {
          console.log(
            `${errorConsole.api_catch}dailyEveryMeetingRecurrence-end-date`,
            err
          );
        });

        let meetingParsedData = JSON.parse(JSON.stringify(meeting));

        if (meeting) {
          meetingDates.push({
            ...meetingParsedData,
            agendas: createdAgendas,
            attendees: participants
          });
        }
      }
    }

    return meetingDates;
  } catch (err) {
    console.log(`${errorConsole.try_catch}dailyEveryMeetingRecurrence`, err);
    return res.json(responses.wrongDetailError);
  }
};

/**
 * @method dailyEveryWeekendMeetingRecurrence: A method to create weekend meetings
 * @param {Object}  meetingData meeting detail from frontend
 * @param {Object}  startDate start date from previous/parent
 * @param {Object}  endDate end date from previous/parent
 * @param {BigInt}  occurences total no. of meetings to be created, from frontend
 */
const dailyEveryWeekendMeetingRecurrence = async (
  meetingData,
  startDate,
  endDate,
  occurences
) => {
  try {
    let meetingDates = [];
    let obj;
    const { createdAgendas, participants } = meetingData;

    if (occurences > 0) {
      const upcomingMeetingDates = [];
      let i = startDate;
      for (let temp = 1; temp <= occurences; i.setDate(i.getDate() + 1)) {
        let dayValue = i.getDay();
        if (dayValue >= 1 && dayValue < 6) {
          upcomingMeetingDates.push(new Date(i));
          temp++;
        }
      }

      await upcomingMeetingDates.forEach(async date => {
        obj = recurrenceMeetingObj(date, meetingData);
        let meeting = await new Meeting(obj).save().catch(err => {
          console.log(
            `${errorConsole.api_catch}dailyEveryWeekendMeetingRecurrence-occurences`,
            err
          );
        });
        let meetingParsedData = JSON.parse(JSON.stringify(meeting));
        if (meeting) {
          meetingDates.push({
            ...meetingParsedData,
            agendas: createdAgendas,
            attendees: participants
          });
        }
      });
    } // for end date
    else {
      for (let j = startDate; j <= endDate; j.setDate(j.getDate() + 1)) {
        let dayValue = j.getDay();
        if (dayValue >= 1 && dayValue < 6) {
          obj = recurrenceMeetingObj(new Date(j), meetingData);
          let meeting = await new Meeting(obj).save().catch(err => {
            console.log(
              `${errorConsole.api_catch}dailyEveryWeekendMeetingRecurrence-end-date`,
              err
            );
          });

          let meetingParsedData = JSON.parse(JSON.stringify(meeting));

          if (meeting) {
            meetingDates.push({
              ...meetingParsedData,
              agendas: createdAgendas,
              attendees: participants
            });
          }
        }
      }
    }

    return meetingDates;
  } catch (err) {
    console.log(
      `${errorConsole.try_catch}dailyEveryWeekendMeetingRecurrence`,
      err
    );
    return res.json(responses.wrongDetailError);
  }
};

module.exports.dailyEveryMeetingRecurrence = dailyEveryMeetingRecurrence;
module.exports.dailyEveryWeekendMeetingRecurrence = dailyEveryWeekendMeetingRecurrence;
