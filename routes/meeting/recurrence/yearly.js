"use strict";
const getWeekDay = require("./functions").getWeekDay;
const Meeting = require("../meeting.model");
const recurrenceMeetingObj = require("../recurrence/functions")
  .recurrenceMeetingObj;
const errorConsole = require("../../../common/catch-consoles");

/**
 * @method yearlyMeetingDaysRecurrence: A method to create monthly meetings
 * @param {Object}  meetingData meeting detail from frontend
 * @param {Object}  startDate start date from previous/parent
 * @param {Object}  endDate end date from previous/parent
 * @param {BigInt}  gapBetweenYears no. of years between start and end date of meeting from previous/parent
 * @param {BigInt}  month month of year from frontend
 * @param {BigInt}  dayOfMonth day of month from frontend
 * @param {BigInt}  occurences total no. of meetings to be created, from frontend
 */
const yearlyMeetingDaysRecurrence = async (
  meetingData,
  startDate,
  endDate,
  gapBetweenYears,
  month,
  dayOfMonth,
  occurences
) => {
  try {
    let meetingDates = []; // meetings array
    const date = new Date(startDate);
    let obj;
    const { createdAgendas, participants } = meetingData;

    if (occurences > 0) {
      for (
        let i = startDate, temp = 1;
        temp <= occurences;
        i.setFullYear(i.getFullYear() + gapBetweenYears)
      ) {
        i.setMonth(Math.abs(month - 1));
        i.setDate(dayOfMonth);
        if (date <= i) {
          obj = recurrenceMeetingObj(new Date(i), meetingData);
          let meeting = await new Meeting(obj).save().catch(err => {
            console.log(
              `${errorConsole.api_catch}yearlyMeetingDaysRecurrence-occurences`,
              err
            );
          });

          let meetingParsedData = JSON.parse(JSON.stringify(meeting));

          if (meeting) {
            meetingDates.push({
              ...meetingParsedData,
              agendas: createdAgendas,
              attendees: participants
            });
          }
          temp++;
        }
      }
    }
    // for end date
    else {
      for (
        let i = startDate;
        i <= endDate;
        i.setFullYear(i.getFullYear() + gapBetweenYears)
      ) {
        i.setMonth(Math.abs(month - 1));
        i.setDate(dayOfMonth);
        if (date <= i) {
          obj = recurrenceMeetingObj(new Date(i), meetingData);
          let meeting = await new Meeting(obj).save().catch(err => {
            console.log(
              `${errorConsole.api_catch}yearlyMeetingDaysRecurrence-end-date`,
              err
            );
          });

          let meetingParsedData = JSON.parse(JSON.stringify(meeting));

          if (meeting) {
            meetingDates.push({
              ...meetingParsedData,
              agendas: createdAgendas,
              attendees: participants
            });
          }
        }
      }
    }

    return meetingDates;
  } catch (err) {
    console.log(`${errorConsole.try_catch}yearlyMeetingDaysRecurrence`, err);
    return res.json(responses.wrongDetailError);
  }
};

/**
 * @method yearlyMeetingWeekDaysRecurrence: A method to create monthly meetings
 * @param {Object}  meetingData meeting detail from frontend
 * @param {Object}  startDate start date from previous/parent
 * @param {Object}  endDate end date from previous/parent
 * @param {BigInt}  gapBetweenYears no. of years between start and end date of meeting from previous/parent
 * @param {BigInt}  month month of year from frontend
 * @param {BigInt}  day day of month from frontend
 * @param {BigInt}  week week of month from frontend
 * @param {BigInt}  occurences total no. of meetings to be created, from frontend
 */
const yearlyMeetingWeekDaysRecurrence = async (
  meetingData,
  startDate,
  endDate,
  gapBetweenYears,
  month,
  day,
  week,
  occurences
) => {
  try {
    let meetingDates = []; // meetings array
    const date = new Date(startDate);
    const { createdAgendas, participants } = meetingData;

    const getStartDate = startDate.getDate();
    startDate.setDate(startDate.getDate() - getStartDate + 1);

    if (occurences > 0) {
      for (
        let i = startDate, temp = 1;
        temp <= occurences;
        i.setFullYear(i.getFullYear() + gapBetweenYears)
      ) {
        i.setMonth(Math.abs(month - 1));
        let t = new Date(getWeekDay(i, day));

        if (date <= i) {
          obj = recurrenceMeetingObj(
            new Date(t.setDate(t.getDate() + week * 7)),
            meetingData
          );
          let meeting = await new Meeting(obj).save().catch(err => {
            console.log(
              `${errorConsole.api_catch}yearlyMeetingWeekDaysRecurrence-occurences`,
              err
            );
          });

          let meetingParsedData = JSON.parse(JSON.stringify(meeting));

          if (meeting) {
            meetingDates.push({
              ...meetingParsedData,
              agendas: createdAgendas,
              attendees: participants
            });
          }
          temp++;
        }
      }
    }
    // for end date
    else {
      for (
        let i = startDate;
        i <= endDate;
        i.setFullYear(i.getFullYear() + gapBetweenYears)
      ) {
        i.setMonth(Math.abs(month - 1));
        let t = new Date(getWeekDay(i, day));
        if (date <= i) {
          obj = recurrenceMeetingObj(
            new Date(t.setDate(t.getDate() + week * 7)),
            meetingData
          );
          let meeting = await new Meeting(obj).save().catch(err => {
            console.log(
              `${errorConsole.api_catch}yearlyMeetingWeekDaysRecurrence-end-date`,
              err
            );
          });

          let meetingParsedData = JSON.parse(JSON.stringify(meeting));

          if (meeting) {
            meetingDates.push({
              ...meetingParsedData,
              agendas: createdAgendas,
              attendees: participants
            });
          }
        }
      }
    }

    return meetingDates;
  } catch (err) {
    console.log(
      `${errorConsole.try_catch}yearlyMeetingWeekDaysRecurrence`,
      err
    );
    return res.json(responses.wrongDetailError);
  }
};

module.exports.yearlyMeetingDaysRecurrence = yearlyMeetingDaysRecurrence;
module.exports.yearlyMeetingWeekDaysRecurrence = yearlyMeetingWeekDaysRecurrence;
