"use strict";
const dailyMeetingRecurrence = require("./daily");
const weeklyMeetingRecurrence = require("./weekly");
const monthlyMeetingRecurrence = require("./monthly");
const yearlyMeetingRecurrence = require("./yearly");
const errorConsole = require("../../../common/catch-consoles");

/**
 * @method meetingRecurrence: A method used to call different recurrence methods
 * @param {Object}  params meeting and recurrence detail from frontend
 */
const meetingRecurrence = async params => {
  try {
    const { recurrenceData, meetingData } = params;
    let meetingDates = []; // meetings array

    const type = recurrenceData.type; // meetings recurrence type (Daily, Weekly, Monthly, Yearly)
    const startDate = new Date(recurrenceData.Range.reccurenceStartDate); // start date of recurrence
    const noRange = new Date(recurrenceData.Range.reccurenceStartDate); // if no end date

    const endDate =
      recurrenceData.Range.rangeRadio === "NoEndRadio" // if no end date
        ? type === "Yearly"
          ? new Date(noRange.setFullYear(noRange.getFullYear() + 10))
          : new Date(noRange.setFullYear(noRange.getFullYear() + 1))
        : new Date(recurrenceData.Range.reccurenceEndDate); // end date of recurrence
    const occurences = recurrenceData.Range.EndAfterText
      ? parseInt(recurrenceData.Range.EndAfterText)
      : 0;
    if (type === "Daily") {
      const { Daily } = recurrenceData;

      if (Daily.DailyRadio === "dailyEveryWeekend") {
        meetingDates = await dailyMeetingRecurrence.dailyEveryWeekendMeetingRecurrence(
          meetingData,
          startDate,
          endDate,
          occurences
        );
      } else {
        const gapBetweenDates = parseInt(Daily.dailyDays);
        meetingDates = await dailyMeetingRecurrence.dailyEveryMeetingRecurrence(
          meetingData,
          startDate,
          endDate,
          gapBetweenDates,
          occurences
        );
      }
    } else if (type === "Weekly") {
      const { Weekly } = recurrenceData;
      const gapBetweenDates = parseInt(Weekly.weekendDays) * 7;
      let weekDays = [];
      if (Weekly && Object.keys(Weekly).length > 0) {
        Object.keys(Weekly).forEach(element => {
          if (Weekly[element] === true) weekDays.push(element);
        });
      }

      meetingDates = await weeklyMeetingRecurrence(
        meetingData,
        startDate,
        endDate,
        gapBetweenDates,
        weekDays,
        occurences
      );
    } else if (type === "Monthly") {
      const { Monthly } = recurrenceData;
      const dayOfMonth = Monthly.monthlyDay ? parseInt(Monthly.monthlyDay) : 0;
      const day = Monthly.selectDay;
      const week = Monthly.selectDate - 1;
      let gapBetweenMonths;
      if (Monthly.monthlyMonth)
        gapBetweenMonths = parseInt(Monthly.monthlyMonth);
      else if (Monthly.monthlySelectMonthField)
        gapBetweenMonths = parseInt(Monthly.monthlySelectMonthField);
      if (dayOfMonth > 0) {
        meetingDates = await monthlyMeetingRecurrence.monthlyMeetingDaysRecurrence(
          meetingData,
          startDate,
          endDate,
          gapBetweenMonths,
          dayOfMonth,
          occurences
        );
      } else {
        meetingDates = await monthlyMeetingRecurrence.monthlyMeetingWeekDaysRecurrence(
          meetingData,
          startDate,
          endDate,
          gapBetweenMonths,
          day,
          week,
          occurences
        );
      }
    }
    // for "Yearly" type
    else {
      const { Yearly } = recurrenceData;
      const dayOfMonth = Yearly.yearlySelectMonthField
        ? parseInt(Yearly.yearlySelectMonthField)
        : 0;
      let month;
      if (Yearly.yearlySelectMonth) month = Yearly.yearlySelectMonth;
      else if (Yearly.yearlyMonth) month = Yearly.yearlyMonth;
      const day = Yearly.yearlyWeek;
      const week = Yearly.yearlyNumber - 1;
      const gapBetweenYears = parseInt(Yearly.yearlySelectYear);
      if (dayOfMonth > 0) {
        meetingDates = await yearlyMeetingRecurrence.yearlyMeetingDaysRecurrence(
          meetingData,
          startDate,
          endDate,
          gapBetweenYears,
          month,
          dayOfMonth,
          occurences
        );
      } else {
        meetingDates = await yearlyMeetingRecurrence.yearlyMeetingWeekDaysRecurrence(
          meetingData,
          startDate,
          endDate,
          gapBetweenYears,
          month,
          day,
          week,
          occurences
        );
      }
    }

    return meetingDates;
  } catch (err) {
    console.log(`${errorConsole.try_catch}meetingRecurrence`, err);
    return [];
  }
};

module.exports = meetingRecurrence;
