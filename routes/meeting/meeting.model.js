"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let meetingSchema = new mongoose.Schema({
  parent_id: { type: Schema.Types.ObjectId, ref: "meeting" },
  meeting_id: { type: String, default: null },
  title: { type: String, default: null, trim: true },
  topic: { type: String, default: null },
  department: { type: String, default: null, trim: true },
  project_name: { type: String, default: null, trim: true },
  start_date_time: { type: Date, default: null },
  end_date_time: { type: Date, default: null },
  address: { type: String, default: null, trim: true },
  attendees: [
    {
      _id: { type: Schema.Types.ObjectId, ref: "attendee" },
      can_edit: { type: Boolean, default: false },
      can_attend: { type: String, default: null },
      is_present: { type: Boolean, default: false },
      is_invitation_sent: { type: Boolean, default: false }
    }
  ],
  admin_email: { type: String, required: true, default: null },
  agendas: [{ type: Schema.Types.ObjectId, ref: "agenda" }],
  is_deleted: { type: Boolean, default: false },
  documents: [Object],
  notes: { type: String, default: null },
  private_notes: [{ email: String, notes: String }],
  is_closed: { type: Boolean, default: false },
  status: { type: String, default: "created" }
});

module.exports = mongoose.model("meeting", meetingSchema);
