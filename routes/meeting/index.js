"use strict";
const express = require("express");
const controller = require("./meeting.controller");
const router = express.Router();

router.post("/create", controller.create);
router.post("/modify", controller.modify);
router.post("/meetings", controller.import_meetings);
router.post("/contacts", controller.import_contacts);
router.post("/fetch-meetings", controller.fetch_meetings);
router.post("/delete", controller.deleteMeeting);
router.post("/invite-meeting-token", controller.invite_meeting_token);
router.get("/attendance", controller.meeting_attendance);
router.post("/pdf", controller.createPdf);
router.post("/share", controller.shareSummary);
router.post("/reopen-close-meeting", controller.reopen_close_meeting);
router.post("/update-status", controller.updateMeetingStatus);
router.post("/search", controller.search);

module.exports = router;
