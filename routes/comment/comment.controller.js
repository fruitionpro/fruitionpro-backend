"use strict";
const Comment = require("./comment.model");
const errorConsole = require("../../common/catch-consoles");

/**
 * @method create: A method to check the password is right or wrong
 * @param {Array}  param List of comments from front-end
 */
const create = async param => {
  try {
    const commentIds = [];
    for (const data of param) {
      if (!data._id) {
        await new Comment(data)
          .save()
          .then(result => {
            if (result) commentIds.push(result._id);
          })
          .catch(err => {
            console.log(`${errorConsole.api_catch}create-comment`, err);
          });
      }
    }
    return commentIds;
  } catch (err) {
    console.log(`${errorConsole.try_catch}create-comment`, err);
    return [];
  }
};

module.exports.create = create;
