"use strict";
const mongoose = require("mongoose");

let commentSchema = new mongoose.Schema({
  email: { type: String, required: true, default: null },
  message: { type: String, required: true, default: null },
  created_at: { type: Date, required: true, default: Date.now }
});

module.exports = mongoose.model("comment", commentSchema);
