"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let organizationSchema = new mongoose.Schema({
  logo: { type: String, default: null },
  name: { type: String, default: null },
  email: { type: String, required: true, unique: true, default: null },
  license_key: { type: Schema.Types.ObjectId, ref: "license" },
  department: [
    {
      label: { type: String, trim: true },
      value: { type: String, trim: true }
    }
  ],
  designation: [
    {
      label: { type: String, trim: true },
      value: { type: String, trim: true }
    }
  ],
  project: [
    {
      label: { type: String, trim: true },
      value: { type: String, trim: true }
    }
  ],
  location: [
    {
      label: { type: String, trim: true },
      value: { type: String, trim: true }
    }
  ]
});

module.exports = mongoose.model("organization", organizationSchema);
