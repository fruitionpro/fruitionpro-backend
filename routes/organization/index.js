"use strict";
const express = require("express");
const controller = require("./organization.controller");
const router = express.Router();

router.post("/create", controller.create);
router.post("/modify", controller.modify);
router.post("/delete-user", controller.deleteOrgUser);
router.post("/users", controller.organization_users);
router.post("/update-license", controller.updateLicense);

module.exports = router;
