"use strict";
const mongoose = require("mongoose");
const Organization = require("./organization.model");
const OrganizationUser = require("../organisation_users/organisation_users.model");
const User = require("../user/user.model");
const Attendee = require("../attendee/attendee.model");
const responses = require("../../common/responses");
const ObjectID = mongoose.Types.ObjectId;
const errorConsole = require("../../common/catch-consoles");
const _ = require("lodash");
const { getJwtToken } = require("../../middleware");
const taskController = require("../task/task.controller");
const licenseController = require("../license/license.controller");
const authController = require("../auth/auth.controller");
const License = require("../license/license.model");
const notificationController = require("../notification/notification.controller");
const meetingController = require("../meeting/meeting.controller");

/**
 * @method trimArrString: A method use to remove spaces from starting and ending of strings in array
 * @param {Array}  data list of values from previous/parent
 */
const trimArrOfString = data => {
  let arr = [];
  try {
    if (data && data.length > 0) {
      for (let i of data) {
        if (
          i &&
          i.label &&
          i.label.trim() !== "" &&
          i.value &&
          i.value.trim() !== ""
        ) {
          arr.push(i);
        }
      }
    }
    return arr;
  } catch (err) {
    console.log(`${errorConsole.try_catch}trimArrString`, err);
    return arr;
  }
};

/**
 * @method updateUser: A method to update user
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const updateUser = async (email, data) => {
  try {
    await User.updateOne({ email }, { $set: data }).then(result => {
      if (result && result.nModified) return true;
      else return false;
    });
  } catch (err) {
    console.log(`${errorConsole.try_catch}updateUser`, err);
    return false;
  }
};

/**
 * @method create: A method to create Organization
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const create = async (req, res) => {
  try {
    const { id, name, email, logo, license } = req.body;
    req.checkBody("id", "User id is empty").notEmpty();
    req.checkBody("name", "Organization name is empty").notEmpty();
    req.checkBody("email", "Organization email id is empty").notEmpty();
    req.checkBody("email", "Organization email id is invalid").isEmail();
    req.checkBody("license", "License key is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    let organizationFound = await Organization.findOne({ email });
    if (organizationFound) return res.json(responses.organizationExist);

    let user = await User.findOne({ _id: ObjectID(id) }, { _id: 0, email: 1 });

    const licenseDetail = await licenseController.check_license({
      license,
      email,
      admin_email: user.email
    });
    let licenseId;
    if (licenseDetail.status === 200) licenseId = licenseDetail.id;
    else return res.json(licenseDetail);

    const params = {
      name,
      email,
      logo,
      license_key: licenseId
    };

    return new Organization(params)
      .save()
      .then(async org => {
        if (org) {
          let organization;
          await Organization.findOne({ _id: ObjectID(org._id) })
            .populate("license_key")
            .then(result => {
              organization = JSON.parse(JSON.stringify(result));
            });

          await User.updateOne(
            { _id: ObjectID(id) },
            {
              $set: {
                organization_id: organization._id,
                type: "organization"
              }
            }
          );
          let user;
          await User.findOne({ _id: ObjectID(id) })
            .populate("license_key")
            .then(result => {
              user = JSON.parse(JSON.stringify(result));
            });

          return res.json({
            ...responses.organizationSuccess,
            data: {
              organization: {
                ...organization,
                ...authController.expiryDate(organization)
              },
              user: {
                ...user,
                ...authController.expiryDate(user),
                token: getJwtToken(params.email)
              }
            }
          });
        } else return res.json(responses.wrongDetailError);
      })
      .catch(err => {
        console.log(`${errorConsole.api_catch}create-organization`, err);
        return res.json(responses.wrongDetailError);
      });
  } catch (err) {
    console.log(`${errorConsole.try_catch}create-organization`, err);
    return res.json(responses.wrongDetailError);
  }
};

/**
 * @method deleteOrgUser: A method to permanent delete Organization employee
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const deleteOrgUser = async (req, res) => {
  try {
    const { user_id, admin_email } = req.body;
    req.checkBody("user_id", "User id is empty").notEmpty();
    req.checkBody("admin_email", "Admin email id is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    const organisationUser = await OrganizationUser.findOne(
      { _id: ObjectID(user_id) },
      { email: 1, _id: 0, license_key: 1 }
    );
    await OrganizationUser.deleteOne({ _id: ObjectID(user_id) }).then(
      async result => {
        if (result) {
          // Get organisation by creator email id
          const senderDetail = await meetingController.getOrganisation(
            admin_email
          );
          notificationController.create({
            receiver: organisationUser.email,
            sender: admin_email,
            message: `${taskController.titleCase(
              senderDetail.name,
              " "
            )} has deleted your account.`,
            type: "profile"
          });
        }
      }
    );
    await Attendee.updateOne(
      { admin_email, email: organisationUser.email, is_deleted: false },
      { $set: { is_deleted: true } }
    );
    if (organisationUser && organisationUser.license_key) {
      await License.updateOne(
        { _id: organisationUser.license_key },
        { $set: { assign_to: null } }
      );
      const user = await User.findOne({ email: organisationUser.email });
      const obj = {
        type: user && user.organization_id ? "organization" : "individual",
        department: null,
        designation: null,
        projects: [],
        locations: [],
        departments: []
      };
      await updateUser(organisationUser.email, obj);
      await License.findOne({ previous: organisationUser.email }).then(
        async result => {
          if (result && result._id) {
            await updateUser(organisationUser.email, {
              license_key: result._id
            });
          }
        }
      );
    }
    return res.json(responses.deleteUserSuccess);
  } catch (err) {
    console.log(`${errorConsole.try_catch}delete-organization`, err);
    return res.json(responses.somethingWrong);
  }
};

/**
 * @method modify: A method to update the Organization and user
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const modify = async (req, res) => {
  try {
    const {
      id,
      license,
      departmentOptions,
      designationOptions,
      projectOptions,
      locationOptions,
      email,
      userId,
      userName,
      userEmail,
      userDepartment,
      userDesignation,
      preferred_calender
    } = req.body;
    
    req.checkBody("id", "The organization id is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    let licenseObj = { license, email, admin_email: userEmail };
    const licenseDetail = await licenseController.check_license(licenseObj);
    let licenseId;
    if (licenseDetail.status === 200) licenseId = licenseDetail.id;
    else return res.json(licenseDetail);

    let organizationObj = {
      ..._.omit(req.body, [
        "departmentOptions",
        "designationOptions",
        "projectOptions",
        "locationOptions"
      ]),
      department: trimArrOfString(departmentOptions),
      designation: trimArrOfString(designationOptions),
      project: trimArrOfString(projectOptions),
      location: trimArrOfString(locationOptions),
      license_key: licenseId
    };

    let userObj = {
      name: userName,
      department: userDepartment.value ? userDepartment.value : "",
      designation: userDesignation.value ? userDesignation.value : "",
      preferred_calender: taskController.titleCase(preferred_calender)
    };

    const updatedOrganization = await Organization.updateOne(
      { _id: ObjectID(id) },
      { $set: organizationObj }
    );

    const updatedUser = await User.updateOne(
      { _id: ObjectID(userId) },
      { $set: userObj }
    );

    if (updatedOrganization && updatedUser) {
      let organization;
      await Organization.findOne({ _id: ObjectID(id) })
        .populate("license_key")
        .then(result => {
          organization = JSON.parse(JSON.stringify(result));
        });

      let user;
      await User.findOne({ _id: ObjectID(userId) })
        .populate("license_key")
        .then(result => {
          user = JSON.parse(JSON.stringify(result));
        });

      return res.json({
        ...responses.updateOrganizationSuccess,
        data: {
          ...organization,
          ...authController.expiryDate(organization),
          personalDetails: {
            ...user,
            license:
              user.license_key && user.license_key.key
                ? user.license_key.key
                : ""
          }
        }
      });
    } else {
      return res.json(responses.wrongDetailError);
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}modify-organization`, err);
    return res.json(responses.wrongDetailError);
  }
};

/**
 * @method organization_users: A method to fetch employees of specific Organization
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const organization_users = async (req, res) => {
  try {
    req.checkBody("email", "Email is empty").notEmpty();
    req.checkBody("email", "Email is invalid").isEmail();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    const users = await OrganizationUser.find({
      org_email: req.body.email
    }).populate("license_key");

    return res.json({ ...responses.fetchSuccess, users: users });
  } catch (err) {
    console.log(`${errorConsole.try_catch}fetch-organization-users`, err);
    return res.json(responses.fetchUnsuccess);
  }
};

/**
 * @method updateLicense: A method to change license
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const updateLicense = async (req, res) => {
  try {
    const { organization_id, user_id } = req.body;

    req.checkBody("organization_id", "Organization id is empty").notEmpty();
    req.checkBody("user_id", "User id is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    let user = await User.findOne(
      { _id: ObjectID(user_id) },
      { _id: 0, email: 1 }
    );

    const licenseDetail = await licenseController.check_license({
      license,
      email,
      admin_email: user.email
    });
    let licenseId;
    if (licenseDetail.status === 200) licenseId = licenseDetail.id;
    else return res.json(licenseDetail);

    await Organization.updateOne(
      { _id: ObjectID(organization_id) },
      { $set: { license_key: licenseId } }
    ).then(async result => {
      if (result) {
        let organization;
        await Organization.findOne({ _id: ObjectID(organization_id) })
          .populate("license_key")
          .then(result => {
            organization = JSON.parse(JSON.stringify(result));
          });

        let user;
        await User.findOne({ _id: ObjectID(user_id) })
          .populate("license_key")
          .then(result => {
            user = JSON.parse(JSON.stringify(result));
          });

        return res.json({
          ...responses.organizationSuccess,
          data: {
            organization: {
              ...organization,
              ...authController.expiryDate(organization)
            },
            user: {
              ...user,
              ...authController.expiryDate(user),
              token: getJwtToken(organization.email)
            }
          }
        });
      } else return res.json(responses.wrongDetailError);
    });

    return res.json(responses.updateOrganizationSuccess);
  } catch (err) {
    console.log(`${errorConsole.try_catch}updateLicense`, err);
    return res.json(responses.catchError);
  }
};

module.exports.create = create;
module.exports.modify = modify;
module.exports.organization_users = organization_users;
module.exports.deleteOrgUser = deleteOrgUser;
module.exports.updateLicense = updateLicense;
