"use strict";
const bcrypt = require("bcrypt-nodejs");
const errorConsole = require("../../common/catch-consoles");

/**
 * @method comparePasswords: A method to check the password is right or wrong
 * @param {String}  passwordInHand password from front-end
 * @param {String}  passwordInDB password from user account
 */
const comparePasswords = (passwordInHand, passwordInDB) => {
  try {
    return new Promise((resolve, reject) => {
      bcrypt.compare(passwordInHand, passwordInDB, (err, result) => {
        if (err) reject(err);
        if (result) resolve(result);
        resolve(false);
      });
    });
  } catch (err) {
    console.log(`${errorConsole.try_catch}comparePasswords`, err);
    return res.json(responses.loginUnsuccess);
  }
};

/**
 * @method encryptPassword: A method to convert the user account password into encrypted form
 * @param {String}  pwd password from front-end
 */
const encryptPassword = pwd => {
  try {
    if (!pwd) throw new Error("Password is Required");
    let salt = bcrypt.genSaltSync(10);
    let hash = bcrypt.hashSync(pwd, salt);
    return { pwd: hash, salt };
  } catch (err) {
    console.log(`${errorConsole.try_catch}encryptPassword`, err);
    return res.json(responses.loginUnsuccess);
  }
};

module.exports.comparePasswords = comparePasswords;
module.exports.encryptPassword = encryptPassword;
