"use strict";
const mongoose = require("mongoose");
const User = require("../user/user.model");
const helper = require("./helper");
const responses = require("../../common/responses");
const microsoftCredentials = require("../../common/get-data-api/microsoft");
const middleware = require("../../middleware");
const googleCredentials = require("../../common/get-data-api/google");
const Organization = require("../organization/organization.model");
const _ = require("lodash");
const microsoftMethods = require("../../common/get-data-api/microsoft");
const googleMethods = require("../../common/get-data-api/google");
const errorConsole = require("../../common/catch-consoles");
const moment = require("moment");
const dateFormat = require("../../common/format");
const organisationUser = require("../../routes/organisation_users/organisation_users.model");
const ObjectID = mongoose.Types.ObjectId;
const dateFunction = require("../../common/date-functions");
const cronJob = require("../../common/cron-job");

/**
 * @method expiryDate: A method to check license is expired or not
 * @param {Object}  data user detail from previous/parent
 */
const expiryDate = data => {
  try {
    let obj = {
      license: "",
      license_expire_date: "",
      leftTimeToExpireLicense: "",
      isLicenseExpired: false
    };

    if (!data) return obj;

    const { license_key } = data;

    if (license_key && license_key.expiry_date) {
      const { expiry_date } = license_key;
      obj.license_expire_date = moment(expiry_date).format(
        dateFormat.EXPIRY_DATE_FORMAT
      );
      obj.leftTimeToExpireLicense = dateFunction.dateDifference(
        new Date(),
        new Date(expiry_date)
      );
      obj.license = data.key;
      if (
        obj.leftTimeToExpireLicense.days <= 0 &&
        obj.leftTimeToExpireLicense.hours <= 0 &&
        obj.leftTimeToExpireLicense.minutes <= 0
      ) {
        obj.isLicenseExpired = true;
      }
    }

    return obj;
  } catch (err) {
    console.log(`${errorConsole.try_catch}expiryDate`, err);
    return true;
  }
};

/**
 * @method importData: A method to call import meetings and contacts function from user calendar
 * @param {Object}  params user access token, calendar type from frontend
 * @param {Object}  sync A cron-job object
 */
const importData = (params, sync) => {
  try {
    if (params && params.type) {
      const dataParams = _.omit(params, ["type"]);
      if (params.type === "microsoft") {
        microsoftMethods.storeMeetings(dataParams, sync);
        microsoftMethods.storeAttendees(dataParams, sync);
      } else if (params.type === "google") {
        googleMethods.storeMeetings(dataParams, sync);
        googleMethods.storeAttendees(dataParams, sync);
      }
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}social-login-importData`, err);
    return res.json(responses.loginUnsuccess);
  }
};

/**
 * @method login: A method to login using email and passowrd of user
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const login = async (req, res) => {
  try {
    const { id, password, email, time_zone } = req.body;
    req.checkBody("password", "User password is empty").notEmpty();
    req.checkBody("email", "User email id is empty").notEmpty();
    req.checkBody("email", "User email id is invalid").isEmail();
    req.checkBody("time_zone", "User time-zone is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    let user;
    await User.findOne({ email })
      .populate("license_key")
      .then(result => {
        user = JSON.parse(JSON.stringify(result));
      });
    if (!user) return res.json(responses.notRegistered);

    if (!user.password) return res.json(responses.wrongDetailError);

    await User.updateOne({ email }, { $set: { time_zone } });

    if (!user.is_verified) {
      return res.json(responses.verifyEmail);
    }

    let flag = await helper.comparePasswords(password, user.password);
    if (!flag) return res.json(responses.InvalidPassword);

    let organization;
    if (user.organization_id) {
      await Organization.findOne({
        _id: ObjectID(user.organization_id)
      })
        .populate("license_key")
        .then(result => {
          organization = JSON.parse(JSON.stringify(result));
        });
    }

    await organisationUser.updateOne(
      { email },
      { $set: { name: user.name, logo: user.logo } }
    );

    let InvitedUser;
    if (id !== "") {
      const invitedOrganization = await Organization.findOne({
        _id: ObjectID(id)
      });

      await organisationUser
        .findOne({
          email: email,
          org_email: invitedOrganization.email
        })
        .populate("license_key")
        .then(result => {
          InvitedUser = JSON.parse(JSON.stringify(result));
        });

      const obj = {
        department: (InvitedUser && InvitedUser.department) || "",
        designation: (InvitedUser && InvitedUser.designation) || "",
        projects: (InvitedUser && InvitedUser.projects) || "",
        locations: (InvitedUser && InvitedUser.locations) || "",
        license_key:
          InvitedUser && InvitedUser.license_key && InvitedUser.license_key._id
            ? InvitedUser.license_key._id
            : "",
        departments: (InvitedUser && InvitedUser.departments) || ""
      };

      if (InvitedUser && InvitedUser.email) {
        await User.updateOne({ email: InvitedUser.email }, { $set: obj });
      }
    }

    const finalData = {
      ...responses.loginSuccess,
      data: {
        ..._.omit(user, ["license", "organization_id"]),
        individualUser: {
          license:
            user.license_key && user.license_key.key ? user.license_key.key : ""
        },
        ...expiryDate(user),
        organization: {
          ...organization,
          ...expiryDate(organization)
        },
        InvitedUser: {
          ...InvitedUser,
          ...expiryDate(InvitedUser)
        }
      },
      token: middleware.getJwtToken(email)
    };

    return res.json(finalData);
  } catch (err) {
    console.log(`${errorConsole.try_catch}login`, err);
    return res.json(responses.loginUnsuccess);
  }
};

/**
 * @method socialLogin: A method to social-login using user access token
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const socialLogin = async (req, res) => {
  try {
    const { loginAccessToken, type, time_zone } = req.body;
    req.checkBody("loginAccessToken", "Token is empty").notEmpty();
    req.checkBody("type", "Type is empty(microsoft or google)").notEmpty();
    req.checkBody("time_zone", "User time-zone is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    let data;
    let params;
    if (type === "microsoft") {
      data = await microsoftCredentials.credentials(loginAccessToken);
      if (!data) return res.json(responses.InvalidToken);

      if (!data.userPrincipalName) res.json(responses.notExist);

      params = {
        name: data.displayName ? data.displayName : null,
        email: data.userPrincipalName,
        is_verified: true,
        time_zone
      };
    } else if (type === "google") {
      data = await googleCredentials.credentials(loginAccessToken);
      if (!data) return res.json(responses.InvalidToken);

      if (!data.email) res.json(responses.notExist);

      params = {
        logo: data.picture ? data.picture : null,
        name: data.name ? data.name : null,
        email: data.email,
        is_verified: true,
        time_zone
      };
    }

    const token =
      type === "google" ? req.body.eventAccessToken : req.body.loginAccessToken;

    const importParams = {
      accessToken: token,
      type,
      email: params.email
    };
    cronJob.syncMeeting(importParams);

    let user;
    await User.findOne({ email: params.email })
      .populate("license_key")
      .then(result => {
        user = JSON.parse(JSON.stringify(result));
      });
    if (user) {
      await User.updateOne({ email: params.email }, { $set: { time_zone } });

      if (user.logo) params.logo = user.logo;

      let organization;
      if (user.organization_id) {
        await Organization.findOne({
          _id: user.organization_id
        })
          .populate("license_key")
          .then(result => {
            organization = JSON.parse(JSON.stringify(result));
          });
      }

      return res.json({
        ...responses.loginSuccess,
        exist: true,
        data: {
          user: {
            ...user,
            socialLoginType: type,
            ...expiryDate(user)
          },
          organization: {
            ...organization,
            ...expiryDate(organization)
          }
        },
        token: middleware.getJwtToken(params.email)
      });
    } else {
      const newUser = await new User(params).save();

      importData(importParams);

      return res.json({
        ...responses.loginSuccess,
        exist: false,
        data: {
          ...JSON.parse(JSON.stringify(newUser)),
          socialLoginType: type
        },
        token: middleware.getJwtToken(newUser.email)
      });
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}social-login`, err);
    return res.json(responses.loginUnsuccess);
  }
};

module.exports.expiryDate = expiryDate;
module.exports.login = login;
module.exports.socialLogin = socialLogin;
module.exports.importData = importData;
