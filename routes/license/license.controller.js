"use strict";
const responses = require("../../common/responses");
const credential = require("../../common/credential");
const errorConsole = require("../../common/catch-consoles");
const License = require("../license/license.model");
const taskController = require("../task/task.controller");
const mailer = require("../../common/send-mail");
const htmlFormat = require("../../common/meeting-mail-format");
const stripe = require("stripe")(credential.stripeKey);
const dateFunction = require("../../common/date-functions");

/**
 * @method paymentHandler: A method to send payment information by mail
 * @param {String}  id transaction id from previous/parent
 * @param {String}  status transaction status from previous/parent
 * @param {String}  header mail heading from previous/parent
 * @param {String}  message mail content/message from previous/parent
 * @param {String}  receiver receiver mail id from previous/parent
 */
const paymentHandler = async (id, status, message, receiver) => {
  try {
    await License.updateOne({ transaction_id: id }, { $set: { status } });
    const htmlBody = htmlFormat.htmlBodyMailResponse(
      credential.logo,
      credential.name,
      credential.email,
      "Please see the following license payment details :- ",
      message
    );
    mailer.sendMail(
      credential.email,
      receiver,
      htmlBody,
      "Payment Information",
      null
    );
  } catch (err) {
    console.log(`${errorConsole.try_catch}paymentHandler`, err);
    return null;
  }
};

/**
 * @method licenseSender: A method to send licenses to buyer through mail
 * @param {Array}  licenses List of licenses from front-end
 * @param {String}  receiver receiver mail from front-end
 */
const licenseSender = (licenses, receiver) => {
  try {
    let mailContent = htmlFormat.licenseMail(licenses);
    const htmlBody = htmlFormat.htmlBodyMailResponse(
      credential.logo,
      credential.name,
      credential.email,
      "Please see the following license details :- ",
      mailContent
    );
    mailer.sendMail(
      credential.email,
      receiver,
      htmlBody,
      "License Information",
      null
    );
  } catch (err) {
    console.log(`${errorConsole.try_catch}licenseSender`, err);
    return null;
  }
};

/**
 * @method createLicense: A method to create licenses
 * @param {Object}  data buyer detail from previous/parent
 * @param {String}  licenseType license type(group or single) from front-end
 * @param {String}  plan license plan(monthly, yearly and free trial) from front-end
 * @param {String}  quantity Total no. of licenses from front-end
 */
const createLicense = (data, licenseType, plan, quantity) => {
  try {
    const { status, billing_details, id } = data;
    let obj = {
      email: billing_details.email,
      status
    };
    let licensesArr = [];
    let result = {
      message: status,
      id
    };
    if (status === "succeeded") {
      const todayDate = new Date();
      plan = plan.toLowerCase();
      if (plan === "month") todayDate.setMonth(todayDate.getMonth() + 1);
      else if (plan === "year") {
        todayDate.setFullYear(todayDate.getFullYear() + 1);
      } else if (plan === "trial") {
        obj.is_used_trial = true;
        todayDate.setDate(todayDate.getDate() + credential.freeTrialDays);
      }
      let expiry_date = new Date(todayDate);
      obj = {
        ...obj,
        expiry_date,
        type: licenseType
      };
      for (let i = 1; i <= quantity; i++) {
        let licenseKey = taskController.create_UUID();
        licensesArr.push(licenseKey);
        new License({ ...obj, key: licenseKey }).save();
      }
      licenseSender(licensesArr, billing_details.email);
      return {
        status: 200,
        ...result
      };
    } else {
      obj = {
        ...obj,
        transaction_id: id,
        transaction_info: { licenseType, plan, quantity }
      };
      new License(obj).save();
      return {
        status: 400,
        ...result
      };
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}createLicense`, err);
    return null;
  }
};

/**
 * @method payment: A method to purchase licenses
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-ends
 */
const payment = async (req, res) => {
  try {
    const {
      amount,
      description,
      currency,
      source,
      type,
      plan,
      quantity
    } = req.body;

    req.checkBody("amount", "Amount is empty").notEmpty();
    req.checkBody("currency", "Currency is empty").notEmpty();
    req.checkBody("source", "Source (Transaction data) is empty").notEmpty();
    req.checkBody("plan", "License plan is empty").notEmpty();
    req.checkBody("quantity", "License quantity is empty").notEmpty();
    req.checkBody("type", "License type (Group or Single) is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    const licenseType = type.toLowerCase();

    if (licenseType !== "single" && licenseType !== "group") {
      return res.json({
        status: 400,
        message: "License type (Group or Single) is incorrect"
      });
    }

    await stripe.charges
      .create({
        amount,
        currency,
        description,
        source
      })
      .then(async result => {
        const obj = createLicense(result, licenseType, plan, quantity);
        return res.json(obj);
      })
      .catch(err => {
        console.log(`${errorConsole.api_catch}payment`, err);
        return res.json({ status: 400, message: "Your card was declined." });
      });
  } catch (err) {
    console.log(`${errorConsole.try_catch}payment`, err);
    return res.json(responses.catchError);
  }
};

/**
 * @method check_license: A method to check license is expired, used and valid
 * @param {Object}  param license detail from previous/parent
 */
const check_license = async (param, previous = true) => {
  try {
    const { license, email, admin_email } = param;
    let conditions = [{ key: license }];
    if (admin_email) conditions.push({ email: admin_email });

    const data = await License.findOne(
      { $and: conditions },
      { key: 1, assign_to: 1, expiry_date: 1 }
    );
    if (!data) return responses.InvalidLicense;
    else if (dateFunction.isPastDate(data.expiry_date)) {
      return responses.ExpiredLicense;
    } else if (!data.assign_to) {
      let obj = {
        assign_to: email
      };
      if (previous === true) {
        obj = {
          ...obj,
          previous: email
        };
      }
      return await License.updateOne({ key: license }, { $set: obj }).then(
        async res => {
          if (res) {
            await License.updateMany(
              { $and: [{ assign_to: email }, { key: { $ne: license } }] },
              { $set: { assign_to: null } }
            );
            await License.updateMany(
              { $and: [{ previous: email }, { key: { $ne: license } }] },
              { $set: { previous: null } }
            );
            return {
              status: 200,
              message: "License Successfully assigned to User",
              id: data._id
            };
          }
        }
      );
    } else if (data.assign_to !== email) {
      return {
        status: 400,
        message: "License already assigned to another user"
      };
    } else if (data.assign_to === email) {
      return {
        status: 200,
        id: data._id
      };
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}check_license`, err);
    return res.json(responses.catchError);
  }
};

/**
 * @method stripeWebhook: A method for license payment transactions
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const stripeWebhook = async (req, res) => {
  try {
    const { type, data } = req.body;
    req.checkBody("type", "Transaction type is empty.").notEmpty();
    req.checkBody("data.object.id", "Transaction id is empty.").notEmpty();
    req
      .checkBody("data.object.status", "Transaction status is empty.")
      .notEmpty();
    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    let apiRes = {};
    const transaction_id = data.object.id;
    const newStatus = data.object.status; // new updated transaction status from webhooks
    await License.findOne({ transaction_id }).then(async res => {
      if (res) {
        const obj = JSON.parse(JSON.stringify(res));
        const { transaction_info } = obj;
        const dbStatus = obj.status; // old stored status from database
        if (dbStatus === "succeeded") {
          apiRes = {
            status: 304,
            message: "Already Succeeded"
          };
        } else {
          switch (type.slice(7)) {
            case "succeeded":
              if (newStatus === "succeeded" && dbStatus === "pending") {
                apiRes = createLicense(
                  {
                    status: newStatus,
                    billing_details: { email: obj.email },
                    id: transaction_id
                  },
                  transaction_info.licenseType,
                  transaction_info.plan,
                  transaction_info.quantity
                );
                await License.updateOne(
                  { transaction_id },
                  { $set: { status: newStatus } }
                );
              } else {
                apiRes = {
                  status: 304,
                  message: `Already ${taskController.titleCase(dbStatus)}`
                };
              }
              break;
            case "pending":
              if (newStatus === "pending" && dbStatus === "pending") {
                apiRes = {
                  status: 400,
                  message: "pending"
                };
                paymentHandler(
                  transaction_id,
                  newStatus,
                  "Your transaction is in progress.",
                  obj.email
                );
              } else {
                apiRes = {
                  status: 304,
                  message: `Already ${taskController.titleCase(dbStatus)}`
                };
              }
              break;
            case "failed":
              if (newStatus === "failed" && dbStatus === "pending") {
                apiRes = {
                  status: 400,
                  message: "failed"
                };
                paymentHandler(
                  transaction_id,
                  newStatus,
                  "Your transaction has been failed.",
                  obj.email
                );
              } else {
                apiRes = {
                  status: 304,
                  message: `Already ${taskController.titleCase(dbStatus)}`
                };
              }
              break;
            case "refunded":
              if (newStatus === "succeeded") {
                apiRes = {
                  status: 200,
                  message: "Refunded"
                };
                paymentHandler(
                  transaction_id,
                  "refunded",
                  "Your transaction amount has been refunded successfully.",
                  obj.email
                );
              }
              break;
            default:
              apiRes = {
                status: 400,
                message: "Other transaction type"
              };
          }
        }
      } else {
        apiRes = {
          status: 400,
          message: "No Record"
        };
      }
    });
    return res.json(apiRes);
  } catch (err) {
    console.log(`${errorConsole.try_catch}stripeWebhook`, err);
    return res.json(responses.catchError);
  }
};

/**
 * @method freeTrial: A method to create free trial license
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const freeTrial = async (req, res) => {
  try {
    const { email } = req.body;
    req.checkBody("email", "Email is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    const license = await License.findOne({ email, is_used_trial: true });
    if (license) {
      licenseSender([license.key], email);
      return res.json(responses.trialLicenseExist);
    }

    const obj = {
      id: "",
      status: "succeeded",
      billing_details: { email }
    };
    createLicense(obj, "single", "trial", 1);
    return res.json({
      status: 200,
      message: obj.status
    });
  } catch (err) {
    console.log(`${errorConsole.try_catch}freeTrial`, err);
    return res.json(responses.catchError);
  }
};

module.exports.payment = payment;
module.exports.stripeWebhook = stripeWebhook;
module.exports.check_license = check_license;
module.exports.freeTrial = freeTrial;
