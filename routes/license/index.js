"use strict";
const express = require("express");
const controller = require("./license.controller");
const router = express.Router();

router.post("/payment", controller.payment);
router.post("/stripe-webhook", controller.stripeWebhook);
router.post("/trial", controller.freeTrial);

module.exports = router;
