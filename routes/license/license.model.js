"use strict";
const mongoose = require("mongoose");

let licenseSchema = new mongoose.Schema({
  transaction_id: { type: String, default: null },
  transaction_info: { type: Object, default: null },
  email: { type: String, required: true, default: null },
  key: { type: String, default: null },
  created_at: { type: Date, default: Date.now },
  type: { type: String, required: true, default: null },
  expiry_date: { type: Date, default: null },
  assign_to: { type: String, default: null },
  status: { type: String, required: true, default: null },
  is_used_trial: { type: Boolean, default: false },
  previous: { type: String, default: null }
});

module.exports = mongoose.model("license", licenseSchema);
