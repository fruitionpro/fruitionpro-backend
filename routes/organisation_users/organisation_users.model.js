"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let organizationUserSchema = new mongoose.Schema({
  logo: { type: String, default: null },
  name: { type: String, default: null },
  is_accepted: { type: Boolean, default: null },
  email: { type: String, default: null },
  org_email: { type: String, required: true, default: null },
  department: { type: String, default: null, trim: true },
  designation: { type: String, default: null, trim: true },
  projects: [
    {
      label: { type: String, trim: true },
      value: { type: String, trim: true }
    }
  ],
  locations: [
    {
      label: { type: String, trim: true },
      value: { type: String, trim: true }
    }
  ],
  departments: [
    {
      label: { type: String, trim: true },
      value: { type: String, trim: true }
    }
  ],
  license_key: { type: Schema.Types.ObjectId, ref: "license" },
  token: { type: String, default: null }
});

module.exports = mongoose.model("organization_users", organizationUserSchema);
