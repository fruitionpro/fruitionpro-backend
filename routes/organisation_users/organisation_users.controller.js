"use strict";
const mongoose = require("mongoose");
const OrganizationUser = require("../organisation_users/organisation_users.model");
const User = require("../user/user.model");
const responses = require("../../common/responses");
const errorConsole = require("../../common/catch-consoles");
const ObjectID = mongoose.Types.ObjectId;
const License = require("../license/license.model");
const notificationController = require("../notification/notification.controller");
const taskController = require("../task/task.controller");
const Organisation = require("../organization/organization.model");

/**
 * @method orgAdmin: A method to fetch organisation creator
 * @param {Object}  email organisation email id from previous/parent
 */
const orgAdmin = async email => {
  try {
    const organisation = await Organisation.findOne({ email }, { _id: 1 });
    return await User.findOne(
      {
        organization_id: String(organisation._id)
      },
      { email: 1 }
    ).then(result => {
      if (result) {
        let obj = JSON.parse(JSON.stringify(result));
        return obj;
      } else return null;
    });
  } catch (err) {
    console.log(`${errorConsole.try_catch}orgAdmin`, err);
    return null;
  }
};

/**
 * @method verify_token: A method to verify the Organization employee
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const verify_token = async (req, res) => {
  try {
    const { token } = req.body;
    req.checkBody("token", "Token is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    const orgUser = await OrganizationUser.findOne({ token });
    if (orgUser) {
      let userAlreadyRegistered = false;
      const isUserFound = await User.findOne({
        email: orgUser.email
      });
      if (isUserFound) {
        userAlreadyRegistered = true;
        await OrganizationUser.updateOne(
          { email: orgUser.email },
          { $set: { is_accepted: true, logo: isUserFound.logo || "" } }
        ).then(async result => {
          if (result && result.nModified) {
            const admin = await orgAdmin(orgUser.org_email);
            notificationController.create({
              receiver: admin.email,
              sender: orgUser.email,
              message: `${taskController.titleCase(
                orgUser.name,
                " "
              )} has accepted your invitation.`,
              type: "invitation"
            });
          }
        });
        await User.updateOne(
          { email: orgUser.email },
          {
            $set: {
              license_key: orgUser.license_key, type: "invitedUser",
              department: orgUser.department, designation: orgUser.designation
            }
          }
        ).then(async res => {
          if (res) {
            if (isUserFound.license_key) {
              await License.updateOne(
                { _id: ObjectID(isUserFound.license_key) },
                { $set: { assign_to: null, previous: orgUser.email } }
              );
            }
          }
        });
      }
      return res.json({
        verified_status: true,
        email: orgUser.email,
        userAlreadyRegistered,
        ...responses.verifiedSuccess
      });
    } else {
      return res.json({
        status: 400,
        verified_status: false,
        message: "Invalid token not verified"
      });
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}verify-token-organization-user`, err);
    return res.json({ status: 400, message: "Invalid token" });
  }
};

module.exports.verify_token = verify_token;
module.exports.orgAdmin = orgAdmin;
