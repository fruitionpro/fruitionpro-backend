"use strict";
const express = require("express");
const controller = require("./organisation_users.controller");
const router = express.Router();

router.post("/verify-token", controller.verify_token);

module.exports = router;
