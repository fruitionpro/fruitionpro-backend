"use strict";
const mongoose = require("mongoose");

let attendeeSchema = new mongoose.Schema({
  logo: { type: String, default: null },
  name: { type: String, default: null },
  contact: { type: String, default: null },
  email: { type: String, default: null },
  admin_email: { type: String, required: true, default: null },
  is_deleted: { type: Boolean, default: false },
  token: { type: String, default: null }
});

module.exports = mongoose.model("attendee", attendeeSchema);
