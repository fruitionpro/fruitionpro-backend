"use strict";
const mongoose = require("mongoose");
const Attendee = require("../attendee/attendee.model");
const responses = require("../../common/responses");
const ObjectID = mongoose.Types.ObjectId;
const errorConsole = require("../../common/catch-consoles");

/**
 * @method modify: A method to modify Organization attendee
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const modify = async (req, res) => {
  try {
    const { id } = req.body;
    req.checkBody("id", "Contact id is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    await Attendee.updateOne({ _id: ObjectID(id) }, { $set: req.body });
    return res.json({ ...responses.updateUserSuccess, id });
  } catch (err) {
    console.log(`${errorConsole.try_catch}modify-attendee`, err);
    return res.json(responses.wrongDetailError);
  }
};

/**
 * @method deleteAttendee: A method to soft delete Organization attendee
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const deleteAttendee = async (req, res) => {
  try {
    const { id } = req.body;

    req.checkBody("id", "Contact id is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    await Attendee.updateOne(
      { _id: ObjectID(id) },
      { $set: { is_deleted: true } }
    );
    return res.json({ ...responses.deleteContactSuccess, id });
  } catch (err) {
    console.log(`${errorConsole.try_catch}delete-attendee`, err);
    return res.json(responses.somethingWrong);
  }
};

/**
 * @method fetch_contacts: A method to fetch contacts of specific user
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const fetch_contacts = async (req, res) => {
  try {
    const { email } = req.body;
    req.checkBody("email", "Email id is empty").notEmpty();
    req.checkBody("email", "Email id is invalid").isEmail();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    const attendees = await Attendee.find({
      $and: [{ admin_email: email }, { is_deleted: false }]
    });
    if (attendees) {
      return res.json({ ...responses.fetchSuccess, data: attendees });
    } else return res.json(responses.noContacts);
  } catch (err) {
    console.log(`${errorConsole.try_catch}fetch-contacts-attendee`, err);
    return res.json(responses.fetchContactsUnsuccuss);
  }
};

module.exports.fetch_contacts = fetch_contacts;
module.exports.deleteAttendee = deleteAttendee;
module.exports.modify = modify;
