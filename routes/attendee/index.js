"use strict";
const express = require("express");
const controller = require("./attendee.controller");
const router = express.Router();

router.post("/delete", controller.deleteAttendee);
router.post("/fetch-contacts", controller.fetch_contacts);

module.exports = router;
