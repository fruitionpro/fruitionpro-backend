"use strict";
const mongoose = require("mongoose");
const User = require("./user.model");
const OrganisationUser = require("../../routes/organisation_users/organisation_users.model");
const helper = require("../auth/helper");
const crypto = require("crypto");
const mailer = require("../../common/send-mail");
const responses = require("../../common/responses");
const url = require("../../common/credential");
const ObjectID = mongoose.Types.ObjectId;
const errorConsole = require("../../common/catch-consoles");
const htmlFormat = require("../../common/meeting-mail-format");
const Attendee = require("../attendee/attendee.model");
const Meeting = require("../meeting/meeting.model");
const Organization = require("../organization/organization.model");
const middleware = require("../../middleware");
const taskController = require("../task/task.controller");
const licenseController = require("../license/license.controller");
const authController = require("../auth/auth.controller");
const notificationController = require("../notification/notification.controller");
const orgUserController = require("../organisation_users/organisation_users.controller");
const meetingController = require("../meeting/meeting.controller");

/**
 * @method diff_hours: A method to calculate no. of hours between the two dates
 * @param {Object}  dt1 initial date from previous/parent
 * @param {Object}  dt2 final date from previous/parent
 */
const diff_hours = (dt1, dt2) => {
  try {
    let diff = (dt1.getTime() - dt2.getTime()) / 1000;
    diff /= 60 * 60;
    return Math.abs(Math.round(diff));
  } catch (err) {
    console.log(`${errorConsole.try_catch}diff_hours`, err);
    return "";
  }
};

/**
 * @method create: A method for sign-up
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const create = async (req, res) => {
  try {
    const {
      name,
      email,
      password,
      is_verified,
      time_zone,
      preferred_calender
    } = req.body;
    req.checkBody("name", "User name is empty").notEmpty();
    req.checkBody("email", "User email id is empty").notEmpty();
    req.checkBody("email", "User email id is invalid").isEmail();
    req.checkBody("password", "User password is empty").notEmpty();
    req.checkBody("time_zone", "User time-zone is empty").notEmpty();
    req
      .checkBody("preferred_calender", "Please Choose One Calender")
      .notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    const { pwd } = await helper.encryptPassword(password);

    let userFound = await User.findOne({ email });
    if (userFound) return res.json(responses.Exist);

    let token = await crypto.randomBytes(16).toString("hex");
    const params = {
      name,
      email,
      password: pwd,
      is_verified: is_verified || false,
      email_token: token,
      time_zone,
      preferred_calender: taskController.titleCase(preferred_calender)
    };

    let user = await new User(params).save().catch(err => {
      console.log(`${errorConsole.api_catch}new-user-sign-up-user`, err);
    });
    const obj = JSON.parse(JSON.stringify(user));
    let verifyEmailLink = `${url.frontendUrl}/verify-email?token=${token}&id=${obj._id}`;

    const htmlBody = await htmlFormat.htmlBodyVerify(
      url.logo,
      url.name,
      url.email,
      "Please visit the following to verify your email",
      verifyEmailLink
    );

    if (!req.body.is_verified) {
      mailer
        .sendMail(url.email, email, htmlBody, "Email verification", null)
        .then(() => {
          return res.json({
            ...responses.sendEmailSuccess,
            data: { ...obj, is_redirect: true },
            token: middleware.getJwtToken(email)
          });
        })
        .catch(err => {
          console.log(`${errorConsole.api_catch}send-mail-sign-up-user`, err);
          return res.json(responses.sendEmailUnSuccess);
        });
    } else {
      await OrganisationUser.updateOne(
        { email },
        { $set: { is_accepted: true, name } }
      ).then(async result => {
        if (result && result.nModified) {
          const orgUser = await OrganisationUser.findOne(
            { email },
            { _id: 0, org_email: 1 }
          );
          const admin = await orgUserController.orgAdmin(orgUser.org_email);
          notificationController.create({
            receiver: admin.email,
            sender: email,
            message: `${taskController.titleCase(
              name,
              " "
            )} has accepted your invitation.`,
            type: "invitation"
          });
        }
      });
      return res.json({
        ...responses.sendEmailSuccess,
        data: { ...obj, is_redirect: false },
        token: middleware.getJwtToken(email)
      });
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}sign-up-user`, err);
    return res.json(responses.sendEmailUnSuccess);
  }
};

/**
 * @method user_modify: A method to update the user detail
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const user_modify = async (req, res) => {
  try {
    const { id, email, license, preferred_calender, licenseType } = req.body;
    req.checkBody("id", "The user id is empty").notEmpty();
    req.checkBody("licenseType", "The user type is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    let obj = {
      ...req.body,
      preferred_calender: taskController.titleCase(preferred_calender)
    };

    let licenseObj = { license, email };
    if (licenseType !== "invitedUser") licenseObj.admin_email = email;
    const licenseDetail = await licenseController.check_license(licenseObj);
    let licenseId;
    if (licenseDetail.status === 200) licenseId = licenseDetail.id;
    else return res.json(licenseDetail);

    obj = {
      ...req.body,
      license_key: licenseId
    };

    const updatedUser = await User.updateOne(
      { _id: ObjectID(id) },
      { $set: obj }
    );

    const newObj = { name: obj.name, logo: obj.logo };

    await OrganisationUser.updateMany({ email }, { $set: newObj });
    await Attendee.updateMany({ email }, { $set: newObj });

    if (updatedUser) {
      let user;
      await User.findOne({ _id: ObjectID(id) })
        .populate("license_key")
        .then(result => {
          user = JSON.parse(JSON.stringify(result));
        });

      return res.json({
        ...responses.updateUserSuccess,
        data: {
          ...user,
          ...authController.expiryDate(user)
        }
      });
    } else return res.json(responses.wrongDetailError);
  } catch (err) {
    console.log(`${errorConsole.try_catch}modify-user`, err);
    return res.json(responses.wrongDetailError);
  }
};

/**
 * @method verifyEmailToken: A method to verify the new user
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const verifyEmailToken = async (req, res) => {
  try {
    const { id, token } = req.body;
    req.checkBody("token", "Token empty").notEmpty();
    req.checkBody("id", "User id is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    let user = await User.findOne({ _id: ObjectID(id) });
    if (!user) return res.json(responses.notExist);

    const authToken = middleware.getJwtToken(user.email);

    const obj = {
      ...responses.verifiedSuccess,
      token: authToken,
      name: user.name,
      email: user.email
    };
    if (user.is_verified === true) return res.json(obj);

    if (user.email_token === token) {
      await User.updateOne(
        { _id: ObjectID(id) },
        { $set: { is_verified: true } }
      ).then(result => {
        if (result) return res.json(obj);
      });
    } else return res.json(responses.InvalidToken);
  } catch (err) {
    console.log(`${errorConsole.try_catch}verifyEmailToken`, err);
    return res.json({ status: 400, message: "Invalid Token" });
  }
};

/**
 * @method verifyForgotPassword: A method to verify the user for change password
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const verifyForgotPassword = async (req, res) => {
  try {
    const { id, token } = req.body;
    req.checkBody("id", "User id is empty").notEmpty();
    req.checkBody("token", "Token is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    let user = await User.findOne({ _id: ObjectID(id) });
    const tokenCreatedTime = diff_hours(new Date(), user.tokenDate);
    if (user) {
      if (tokenCreatedTime <= 1) {
        if (user.token === token) {
          return res.json(responses.verifiedSuccess);
        } else {
          return res.json(responses.InvalidToken);
        }
      } else {
        return res.json(responses.tokenExpired);
      }
    } else {
      return res.json(responses.notExist);
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}verifyForgotPassword`, err);
    return res.json({ status: 400, message: "Invalid Token" });
  }
};

/**
 * @method forgotPassword: A method to send mail for change password
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const forgotPassword = async (req, res) => {
  try {
    req.checkBody("email", "User email id is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    let user = await User.findOne({ email: req.body.email });
    if (!user) return res.json(responses.notRegistered);

    if (!user.is_verified) return res.json(responses.verifyEmail);

    let token = crypto.randomBytes(16).toString("hex");

    const result = await User.updateOne(
      { email: req.body.email },
      { $set: { token, tokenDate: new Date() } }
    );
    if (result) {
      let forgotEmailLink = `${url.frontendUrl}/change-password?token=${token}&id=${user._id}`;

      const htmlBody = await htmlFormat.htmlBodyVerify(
        url.logo,
        url.name,
        url.email,
        "Please visit the following link to reset your password",
        forgotEmailLink
      );

      mailer
        .sendMail(url.email, req.body.email, htmlBody, "Forgot Password", null)
        .then(() => {
          return res.json({
            ...responses.sendEmailSuccess,
            data: forgotEmailLink
          });
        })
        .catch(err => {
          console.log(`${errorConsole.api_catch}forgotPassword-send-mail`, err);
          return res.json(responses.sendEmailUnSuccess);
        });
    }
  } catch (err) {
    console.log(`${errorConsole.try_catch}forgotPassword`, err);
    return res.json(responses.sendEmailUnSuccess);
  }
};

/**
 * @method resetPassword: A method to change password
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const resetPassword = async (req, res) => {
  try {
    const { id, password } = req.body;
    req.checkBody("password", "User password is empty").notEmpty();
    req.checkBody("id", "User id is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    let user = await User.findOne({ _id: ObjectID(id) });
    if (!user) return res.json(responses.notExist);

    const { pwd } = await helper.encryptPassword(password);
    await User.updateOne({ _id: ObjectID(id) }, { $set: { password: pwd } });
    return res.json(responses.resetPasswordSuccess);
  } catch (err) {
    console.log(`${errorConsole.try_catch}resetPassword`, err);
    return res.json({ status: 400, message: "Invalid Token" });
  }
};

/**
 * @method userType: A method to create user individual account
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const userType = async (req, res) => {
  try {
    const { id, license, email } = req.body;
    req.checkBody("id", "User id is empty").notEmpty();
    req.checkBody("license", "License key is empty").notEmpty();
    req.checkBody("email", "User email is empty").notEmpty();
    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    let oldUser = await User.findOne(
      { _id: ObjectID(id) },
      { _id: 0, email: 1 }
    );
    if (oldUser) {
      const licenseDetail = await licenseController.check_license({
        license,
        email,
        admin_email: oldUser.email
      });
      let licenseId;
      if (licenseDetail.status === 200) licenseId = licenseDetail.id;
      else return res.json(licenseDetail);

      await User.updateOne(
        { _id: ObjectID(id) },
        {
          $set: {
            ...req.body,
            license_key: licenseId
          }
        }
      );

      let updatedUser;
      await User.findOne({ _id: ObjectID(id) })
        .populate("license_key")
        .then(result => {
          updatedUser = JSON.parse(JSON.stringify(result));
        });

      return res.json({
        ...responses.userType,
        data: {
          ...updatedUser,
          ...authController.expiryDate(updatedUser)
        }
      });
    } else return res.json(responses.wrongDetailError);
  } catch (err) {
    console.log(`${errorConsole.try_catch}userType`, err);
    return res.json(responses.wrongDetailError);
  }
};

/**
 * @method organization_user_modify: A method to update Organization employee detail
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const organization_user_modify = async (req, res) => {
  try {
    const { email, department, designation, license, admin_email } = req.body;
    req.checkBody("email", "email is empty").notEmpty();
    req.checkBody("admin_email", "Admin email is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    let obj = {
      department,
      designation
    };

    const licenseDetail = await licenseController.check_license(
      { license, email, admin_email },
      false
    );
    let licenseId;
    if (licenseDetail.status === 200) licenseId = licenseDetail.id;
    else return res.json(licenseDetail);

    obj = {
      ...obj,
      license_key: licenseId
    };

    await OrganisationUser.updateOne({ email }, { $set: obj }).then(
      async result => {
        if (result && result.nModified) {
          // Get organisation by creator email id
          const senderDetail = await meetingController.getOrganisation(
            admin_email
          );
          notificationController.create({
            receiver: email,
            sender: admin_email,
            message: `${taskController.titleCase(
              senderDetail.name,
              " "
            )} has updated your profile.`,
            type: "profile"
          });
          await User.updateOne({ email }, { $set: obj });
        }
      }
    );
    return res.json(responses.updateUserSuccess);
  } catch (err) {
    console.log(`${errorConsole.try_catch}organization_user_modify`, err);
    return res.json(responses.wrongDetailError);
  }
};

/**
 * @method invite: A method to send the Organization invitation to employee
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const invite = async (req, res) => {
  try {
    const {
      logo,
      senderName,
      senderEmail,
      email,
      license,
      name,
      department,
      departments,
      designation,
      projects,
      locations,
      id,
      admin_email
    } = req.body;
    
    req.checkBody("name", "User name is empty").notEmpty();
    req.checkBody("email", "User email id is empty").notEmpty();
    req.checkBody("email", "User email id is invalid").isEmail();
    req.checkBody("department", "User department is empty").notEmpty();
    req.checkBody("designation", "User designation is empty").notEmpty();
    req.checkBody("license", "License key is empty").notEmpty();
    req.checkBody("senderName", "Sender name is empty").notEmpty();
    req.checkBody("senderEmail", "Sender email is empty").notEmpty();
    req.checkBody("admin_email", "Admin email is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    const user = await OrganisationUser.findOne({ email });
    if (user) return res.json(responses.alreadyInvited);

    const licenseDetail = await licenseController.check_license(
      { license, email, admin_email },
      false
    );
    let licenseId;
    if (licenseDetail.status === 200) licenseId = licenseDetail.id;
    else return res.json(licenseDetail);

    let inviteLink;

    let token = await crypto.randomBytes(16).toString("hex");

    const params = {
      email,
      name,
      is_accepted: false,
      org_email: senderEmail,
      department,
      designation,
      locations: locations || [],
      projects: projects || [],
      departments: departments || [],
      license_key: licenseId,
      token
    };

    const OrgUsers = await new OrganisationUser(params).save().catch(err => {
      console.log(
        `${errorConsole.api_catch}invite-organization-user-add-new-user`,
        err
      );
    });

    let attendee;
    await Attendee.findOne({
      email,
      admin_email,
      is_deleted: false
    }).then(async result => {
      const resultObj = JSON.parse(JSON.stringify(result));
      if (result) attendee = { ...resultObj };
      if (!result) {
        await new Attendee({
          name,
          email,
          admin_email
        })
          .save()
          .then(res => {
            const resObj = JSON.parse(JSON.stringify(res));
            attendee = { ...resObj };
          });
      }
    });

    if (OrgUsers) {
      inviteLink = `${url.frontendUrl}/register?token=${token}&oid=${id}`;
    }

    const htmlBody = await htmlFormat.htmlBodyVerify(
      logo,
      senderName,
      senderEmail,
      "Please visit the following invitation",
      inviteLink
    );

    mailer.sendMail(
      senderEmail,
      email,
      htmlBody,
      "Organization Invitation",
      null
    );
    notificationController.create({
      receiver: email,
      sender: admin_email,
      message: `${taskController.titleCase(
        senderName,
        " "
      )} sent you a invitation.`,
      type: "profile"
    });
    return res.json({ ...responses.sendEmailSuccess, attendee });
  } catch (err) {
    console.log(`${errorConsole.try_catch}invite-organization-user`, err);
    return res.json(responses.sendEmailUnSuccess);
  }
};

/**
 * @method resendInvite: A method to resend the Organization invitation to employee
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const resendInvite = async (req, res) => {
  try {
    const { logo, senderName, senderEmail, email, id } = req.body;
    req.checkBody("name", "User name is empty").notEmpty();
    req.checkBody("email", "User email id is empty").notEmpty();
    req.checkBody("email", "User email id is invalid").isEmail();
    req.checkBody("senderName", "Sender name is empty").notEmpty();
    req.checkBody("senderEmail", "Sender email is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    const user = await OrganisationUser.findOne({ email });
    let token;
    if (user) token = user.token;

    let inviteLink = `${url.frontendUrl}/register?token=${token}&oid=${id}`;

    const htmlBody = await htmlFormat.htmlBodyVerify(
      logo,
      senderName,
      senderEmail,
      "Please visit the following invitation",
      inviteLink
    );

    mailer.sendMail(
      senderEmail,
      email,
      htmlBody,
      "Resend Organization Invitation",
      null
    );
    const admin = await orgUserController.orgAdmin(senderEmail);
    notificationController.create({
      receiver: email,
      sender: admin.email,
      message: `${taskController.titleCase(
        senderName,
        " "
      )} again sent you a invitation.`,
      type: "profile"
    });
    return res.json(responses.sendEmailSuccess);
  } catch (err) {
    console.log(`${errorConsole.try_catch}resendInvite-organization-user`, err);
    return res.json(responses.sendEmailUnSuccess);
  }
};

/**
 * @method changePassword: A method to change the user account password
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const changePassword = async (req, res) => {
  try {
    const { id, old_password, new_password, socialLogin } = req.body;
    req.checkBody("id", "User id is empty").notEmpty();
    if (!socialLogin) {
      req.checkBody("old_password", "User old password is empty").notEmpty();
    }
    req.checkBody("new_password", "User new password is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    let user = await User.findOne({ _id: ObjectID(id) });
    if (!user) return res.json(responses.InvalidID);
    if (!socialLogin) {
      if (!user.password) return res.json(responses.notSetPassword);

      let compareOldPassword = await helper.comparePasswords(
        old_password,
        user.password
      );
      if (!compareOldPassword) return res.json(responses.wrongOldPassword);

      let flag1 = await helper.comparePasswords(new_password, user.password);
      if (flag1) return res.json(responses.sameAsOldPassword);
    }
    const { pwd } = await helper.encryptPassword(new_password);
    await User.updateOne({ _id: ObjectID(id) }, { $set: { password: pwd } });
    return res.json(responses.changePasswordSuccess);
  } catch (err) {
    console.log(`${errorConsole.try_catch}changePassword`, err);
    return res.json(responses.wrongDetailError);
  }
};

/**
 * @method resend_verification_email: A method to resend new user verification mail
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const resend_verification_email = async (req, res) => {
  try {
    const { email } = req.body;
    req.checkBody("email", "User email id is empty").notEmpty();
    req.checkBody("email", "User email id is invalid").isEmail();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    let userFound = await User.findOne({ email });
    if (!userFound) return res.json(responses.notExist);

    if (userFound.is_verified) return res.json(responses.alreadyVerified);

    let token = await crypto.randomBytes(16).toString("hex");
    await User.updateOne({ email }, { $set: { email_token: token } });

    const parsedUserData = JSON.parse(JSON.stringify(userFound));
    let verifyEmailLink = `${url.frontendUrl}/verify-email?token=${token}&id=${parsedUserData._id}`;

    const htmlBody = await htmlFormat.htmlBodyVerify(
      url.logo,
      url.name,
      url.email,
      "Please visit the following to verify your email",
      verifyEmailLink
    );

    mailer
      .sendMail(
        url.email,
        email,
        htmlBody,
        "Resend Email for verification",
        null
      )
      .then(() => res.json(responses.sendEmailSuccess))
      .catch(err => {
        console.log(`${errorConsole.api_catch}resend_verification_email`, err);
        return res.json(responses.sendEmailUnSuccess);
      });
  } catch (err) {
    console.log(`${errorConsole.try_catch}resend_verification_email`, err);
    return res.json(responses.wrongDetailError);
  }
};

/**
 * @method meeting_attendance: A method to store the real meeting attendance
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const meeting_attendance = async (req, res) => {
  try {
    const { meeting_id, admin_email, attendee_email } = req.body;
    req.checkBody("meeting_id", "Meeting id is empty").notEmpty();
    req.checkBody("admin_email", "Meeting admin email id is empty").notEmpty();
    req.checkBody("attendee_email", "Attendee email is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    const attendee = await Attendee.findOne(
      {
        $and: [
          { admin_email },
          { email: attendee_email },
          { is_deleted: false }
        ]
      },
      { email: 1, name: 1 }
    );

    if (!attendee) return res.json(responses.catchError);

    const meeting = await Meeting.findOne(
      {
        _id: ObjectID(meeting_id)
      },
      { title: 1 }
    );

    await Meeting.updateOne(
      {
        _id: ObjectID(meeting_id),
        attendees: { $elemMatch: { _id: ObjectID(attendee._id) } }
      },
      {
        $set: {
          "attendees.$.is_present": true,
          "attendees.$.can_attend": "yes"
        }
      }
    ).then(result => {
      if (result && result.nModified) {
        notificationController.create({
          receiver: admin_email,
          sender: attendee_email,
          message: `${
            attendee.name
              ? taskController.titleCase(attendee.name, " ")
              : attendee_email
          } is ready to attend <strong>${htmlFormat.upperCaseFirst(
            meeting.title
          )}</strong> meeting.`,
          type: "meeting",
          meeting: meeting._id
        });
      }
    });
    return res.json(responses.attendMeeting);
  } catch (err) {
    console.log(`${errorConsole.try_catch}meeting_attendance`, err);
    return res.json(responses.wrongDetailError);
  }
};

/**
 * @method get_user: A method to fetch user detail
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const get_user = async (req, res) => {
  try {
    const { id, type } = req.body;
    req.checkBody("id", "Id is empty").notEmpty();
    req.checkBody("type", "Login user type is empty").notEmpty();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    let data;
    if (type === "user") {
      await User.findOne({
        _id: ObjectID(id)
      })
        .populate("license_key")
        .then(result => {
          data = JSON.parse(JSON.stringify(result));
        });
    } else {
      await Organization.findOne({
        _id: ObjectID(id)
      })
        .populate("license_key")
        .then(result => {
          data = JSON.parse(JSON.stringify(result));
        });
    }

    return res.json({
      details: {
        ...data,
        ...authController.expiryDate(data),
        userType: type
      },
      ...responses.getAdminSuccess
    });
  } catch (err) {
    console.log(`${errorConsole.try_catch}get_user`, err);
    return res.json(responses.wrongDetailError);
  }
};

module.exports.create = create;
module.exports.user_modify = user_modify;
module.exports.forgotPassword = forgotPassword;
module.exports.verifyForgotPassword = verifyForgotPassword;
module.exports.verifyEmailToken = verifyEmailToken;
module.exports.resetPassword = resetPassword;
module.exports.userType = userType;
module.exports.organization_user_modify = organization_user_modify;
module.exports.invite = invite;
module.exports.resendInvite = resendInvite;
module.exports.changePassword = changePassword;
module.exports.resend_verification_email = resend_verification_email;
module.exports.meeting_attendance = meeting_attendance;
module.exports.get_user = get_user;
