"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let userSchema = new mongoose.Schema({
  logo: { type: String, default: null },
  name: { type: String, default: null },
  email: { type: String, required: true, unique: true, default: null },
  password: { type: String, default: null },
  department: { type: String, default: null, trim: true },
  designation: { type: String, default: null, trim: true },
  projects: [
    {
      label: { type: String, trim: true },
      value: { type: String, trim: true }
    }
  ],
  locations: [
    {
      label: { type: String, trim: true },
      value: { type: String, trim: true }
    }
  ],
  departments: [
    {
      label: { type: String, trim: true },
      value: { type: String, trim: true }
    }
  ],
  license_key: { type: Schema.Types.ObjectId, ref: "license" },
  type: { type: String, default: null },
  organization_id: { type: String, default: null },
  token: { type: String, default: null },
  tokenDate: { type: Date, default: null },
  email_token: { type: String, default: null },
  is_verified: { type: Boolean, default: false },
  time_zone: { type: String, default: null },
  preferred_calender: { type: String, default: null }
});

module.exports = mongoose.model("user", userSchema);
