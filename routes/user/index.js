"use strict";
const express = require("express");
const controller = require("./user.controller");
const router = express.Router();

router.post("/create", controller.create);
router.post("/modify-user", controller.user_modify);
router.post("/forgot-password", controller.forgotPassword);
router.post("/verify-forgot-password", controller.verifyForgotPassword);
router.post("/verify-email", controller.verifyEmailToken);
router.post("/reset-password", controller.resetPassword);
router.post("/user-type", controller.userType);
router.post("/modify", controller.organization_user_modify);
router.post("/invite", controller.invite);
router.post("/resend-invite", controller.resendInvite);
router.post("/change-password", controller.changePassword);
router.post("/resend-verification-email", controller.resend_verification_email);
router.post("/attendance", controller.meeting_attendance);
router.post("/get-user", controller.get_user);

module.exports = router;
