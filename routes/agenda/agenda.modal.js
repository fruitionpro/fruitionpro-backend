"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let agendaSchema = new mongoose.Schema({
  title: { type: String, default: null },
  documents: [Object],
  duration: {
    value: Number,
    label: { type: String, trim: true }
  },
  notes: { type: String, default: null },
  agendaId: { type: Number, default: null },
  tasks: [{ type: Schema.Types.ObjectId, ref: "task" }],
  decisions: [String]
});

module.exports = mongoose.model("agenda", agendaSchema);
