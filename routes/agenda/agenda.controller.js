"use strict";
const Agenda = require("./agenda.modal");
const mongoose = require("mongoose");
const errorConsole = require("../../common/catch-consoles");
const ObjectID = mongoose.Types.ObjectId;
const taskController = require("../task/task.controller");

/**
 * @method create: A method to create agendas
 * @param {Array}  param List of meeting agendas from previous/parent
 */
const create = async param => {
  try {
    const agendas = [];

    for (const data of param) {
      let agendaData = {
        ...data
      };
      await new Agenda(agendaData)
        .save()
        .then(result => {
          if (result) {
            const obj = JSON.parse(JSON.stringify(result));
            agendas.push(obj);
          }
        })
        .catch(err => {
          console.log(`${errorConsole.api_catch}create-agenda`, err);
        });
    }

    return agendas;
  } catch (err) {
    console.log(`${errorConsole.try_catch}create-agenda`, err);
    return [];
  }
};

/**
 * @method upsert: A method to create/update agendas
 * @param {Array}  param List of meeting agendas from previous/parent
 */
const upsert = async param => {
  try {
    const agendaIds = [];

    for (const data of param) {
      let { tasks, decisions } = data;
      let tasksIds = [];
      if (decisions && decisions.length > 0) {
        decisions = decisions.filter(decision => decision.trim());
      }
      if (tasks && tasks.length > 0) {
        tasksIds = await taskController.upsert(tasks);
      }
      const agendaObj = {
        ...data,
        decisions,
        tasks: tasksIds
      };

      if (agendaObj._id) {
        await Agenda.updateOne(
          { _id: ObjectID(agendaObj._id) },
          { $set: agendaObj }
        )
          .then(result => {
            if (result) {
              agendaIds.push(agendaObj._id);
            }
          })
          .catch(err => {
            console.log(
              `${errorConsole.api_catch}upsert-agenda-updateOne`,
              err
            );
          });
      } else {
        await new Agenda(agendaObj)
          .save()
          .then(result => {
            if (result) {
              agendaIds.push(result._id);
            }
          })
          .catch(err => {
            console.log(
              `${errorConsole.api_catch}upsert-agenda-new-agenda`,
              err
            );
          });
      }
    }

    return agendaIds;
  } catch (err) {
    console.log(`${errorConsole.try_catch}upsert-agenda`, err);
    return [];
  }
};

module.exports.upsert = upsert;
module.exports.create = create;
