"use strict";
const user = require("./user");
const auth = require("./auth");
const organization = require("./organization");
const meeting = require("./meeting");
const organizationUser = require("./organisation_users");
const attendee = require("./attendee");
const task = require("./task");
const license = require("./license");
const notification = require("./notification");

module.exports = app => {
  app.use("/api/auth", auth);
  app.use("/api/user", user);
  app.use("/api/organization", organization);
  app.use("/api/meeting", meeting);
  app.use("/api/org-user", organizationUser);
  app.use("/api/attendee", attendee);
  app.use("/api/task", task);
  app.use("/api/license", license);
  app.use("/api/notification", notification);
};
