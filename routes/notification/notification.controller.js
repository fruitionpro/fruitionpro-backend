"use strict";
const Notification = require("./notification.model");
const User = require("../user/user.model");
const dateFunction = require("../../common/date-functions");
const moment = require("moment");
const errorConsole = require("../../common/catch-consoles");
const responses = require("../../common/responses");

/**
 * @method create: A method to create notification
 * @param {Object}  data notification detail(reveiver, sender objectId and message string) from previous/parent
 */
const create = async data => {
  try {
    if (
      !data ||
      !data.receiver ||
      !data.sender ||
      !data.message ||
      data.receiver === data.sender
    ) {
      return;
    }
    const user = await User.findOne({ email: data.receiver });
    if (user) await new Notification(data).save();
  } catch (err) {
    console.log(`${errorConsole.try_catch}create-notification`, err);
  }
};

/**
 * @method findAll: A method to get last month notifications of particular user
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const findAll = async (req, res) => {
  try {
    const { email } = req.body;
    req.checkBody("email", "Email id is empty").notEmpty();
    req.checkBody("email", "Email id is invalid").isEmail();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }
    let date = new Date(moment().format("LL"));
    date = new Date(date.setMonth(date.getMonth() - 1));
    return await Notification.find(
      {
        $and: [
          { receiver: email },
          {
            createdAt: {
              $gte: date
            }
          }
        ]
      },
      { _id: 0, updatedAt: 0, __v: 0 }
    )
      .populate({
        path: "meeting",
        populate: [
          {
            path: "agendas",
            populate: {
              path: "tasks",
              match: { is_deleted: false }
            }
          },
          { path: "attendees._id" }
        ]
      })
      .populate({ path: "task", populate: { path: "comments" } })
      .sort({ createdAt: -1 })
      .then(async result => {
        if (result && result.length > 0) {
          const arr = JSON.parse(JSON.stringify(result));
          for (let i = 0; i < arr.length; i++) {
            await User.findOne(
              { email: arr[i].sender },
              { logo: 1, name: 1, email: 1, _id: 0 }
            ).then(data => {
              if (data) {
                let user = JSON.parse(JSON.stringify(data));
                arr[i].sender = user;
              } else {
                arr[i].sender = { email: arr[i].sender };
              }
            });
            const time = dateFunction.dateDifference(
              new Date(arr[i].createdAt),
              new Date()
            );
            let str = "";
            const keys = Object.keys(time);
            if (keys.every(i => time[i] === 0)) str = "just now";
            else {
              for (let i of keys) {
                if (time[i] === 1) {
                  str = str.concat(
                    time[i] + " " + i.slice(0, i.length - 1) + " "
                  );
                } else if (time[i] > 0) {
                  str = str.concat(time[i] + " " + i + " ");
                }
              }
              str = str.concat("ago");
            }
            arr[i].createdAt = str;
          }
          return res.json(arr);
        } else res.json([]);
      });
  } catch (err) {
    console.log(`${errorConsole.try_catch}fetch-notification`, err);
    return res.json([]);
  }
};

/**
 * @method deleteAll: A method to delete all notifications of particular user
 * @param {Object}  req api request params from front-end
 * @param {Object}  res api response send to front-end
 */
const deleteAll = async (req, res) => {
  try {
    const { email } = req.body;
    req.checkBody("email", "Email id is empty").notEmpty();
    req.checkBody("email", "Email id is invalid").isEmail();

    const errors = req.validationErrors();
    if (errors && errors.length > 0) {
      return res.json({ status: 400, message: errors[0].msg });
    }

    await Notification.deleteMany({ receiver: email }).then(() =>
      res.json(responses.deleteNotificationsSuccess)
    );
  } catch (err) {
    console.log(`${errorConsole.try_catch}fetch-notification`, err);
    return res.json(responses.catchError);
  }
};

module.exports.create = create;
module.exports.findAll = findAll;
module.exports.deleteAll = deleteAll;
