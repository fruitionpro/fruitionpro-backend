"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let notificationSchema = new mongoose.Schema(
  {
    receiver: { type: String, required: true, default: null, trim: true },
    sender: { type: String, required: true, default: null, trim: true },
    message: { type: String, required: true, default: null, trim: true },
    type: { type: String, default: null, trim: true },
    meeting: { type: Schema.Types.ObjectId, ref: "meeting", default: null },
    task: { type: Schema.Types.ObjectId, ref: "task", default: null }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("notification", notificationSchema);
