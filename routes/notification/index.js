"use strict";
const express = require("express");
const controller = require("./notification.controller");
const router = express.Router();

router.post("/fetch", controller.findAll);
router.post("/delete", controller.deleteAll);

module.exports = router;
